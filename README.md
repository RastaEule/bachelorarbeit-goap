# About
This is the repository of my bachelor thesis titled "Hierarchische Strategieplanung für Agenten in einem Aufbauspiel mithilfe von GOAP". It was completed in summer 2021 at the [University of Applied Sciences Kempten](https://www.hs-kempten.de/) under supervision of Prof. Dr. Christoph Bichlmeier.

This contains a fully realized GOAP-system based on the research by [Jeff Orkin](http://alumni.media.mit.edu/~jorkin/goap.html). The project was made in Unity, but all parts regarding GOAP are independent and can theoretically be used for any project using C#.

[More information and screenshots](https://games.hs-kempten.de/hierarchische-strategieplanung-fuer-agenten-in-einem-aufbauspiel-mithilfe-von-goap/)

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x6_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("atPos", transform.position);

            base.WorldState.SetSymbol("holz_stored", false);
            base.WorldState.SetSymbol("has_Holz", false);
            base.WorldState.SetSymbol("fish_stored", false);
            base.WorldState.SetSymbol("has_Fish", false);
        }
    }
}
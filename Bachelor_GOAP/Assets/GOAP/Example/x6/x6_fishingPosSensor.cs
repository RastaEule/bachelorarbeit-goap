﻿using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x6_fishingPosSensor : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Vector3 targetPos = FishingPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol("FishingPos", targetPos);
        }
    }
}
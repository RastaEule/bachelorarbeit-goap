﻿using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x6_storagePosSensor : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Vector3 targetPos = StoragePositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol("StoragePos", targetPos);
        }
    }
}
﻿using System;
using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x6_action_storeHolz : GOAP_Action
    {
        private Vector3 _targetPos = Vector3.zero;

        private agent_motor _motor = null;

        private Action _actionFinishedCallback = null;
        private Action _actionFailedCallback = null;

        protected override void Awake()
        {
            base.Awake();
            _motor = GetComponent<agent_motor>();

            base._preconditions.SetSymbol("has_Holz", true);
            base._effects.SetSymbol("holz_stored", true);
            base._effects.SetSymbol("has_Holz", false);
            base._effects.SetSymbol("atPos", _targetPos);
        }

        #region IGOAP_Action Members

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            this.SetTargetPos(plannedState);
            plannedState.SetSymbol("targetPos", _targetPos);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            object atposobj;
            object targetposobj;
            if (plannedState.State.TryGetValue("atPos", out atposobj) && plannedState.State.TryGetValue("targetPos", out targetposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                Vector3 targetPos = (Vector3)targetposobj;
                if (atPos != null && targetposobj != null)
                    return base.GetCost(plannedState) + Vector3.Distance(atPos, targetPos);
            }
            return base.GetCost(plannedState);
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);

            effects.SetSymbol("atPos", _targetPos);
            return effects;
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            _actionFinishedCallback = onFinished;
            _actionFailedCallback = onFailed;

            this.SetTargetPos(worldState);
            _motor.Goto(_targetPos, MovementFinished, MovementFailed);
        }

        #endregion

        private void MovementFinished()
        {
            _actionFinishedCallback();
        }

        private void MovementFailed()
        {
            _actionFailedCallback();
        }

        private void SetTargetPos(GOAP_State worldState)
        {
            object targetobj;
            worldState.State.TryGetValue("StoragePos", out targetobj);
            if (targetobj != null)
                _targetPos = (Vector3)targetobj;
        }
    }
}
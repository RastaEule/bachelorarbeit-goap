﻿using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x6_forestPosSensor : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Vector3 targetPos = ForestPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol("ForestPos", targetPos);
        }
    }
}
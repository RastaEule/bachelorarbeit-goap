﻿using System.Collections.Generic;
using UnityEngine;

namespace GOAP.Example
{
    public class StoragePositions : MonoBehaviour
    {
        public static StoragePositions I = null;

        [SerializeField]
        private List<Transform> _targetPositions = null;

        private void Awake()
        {
            if (I == null)
                I = this;
        }

        public Vector3 GetNearestPos(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return Vector3.zero;

            Vector3 closest = _targetPositions[0].position;
            for (int i = 1; i < _targetPositions.Count; i++)
            {
                if (Vector3.Distance(fromPos, _targetPositions[i].position) < Vector3.Distance(fromPos, closest))
                    closest = _targetPositions[i].position;
            }
            return closest;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x4_action_fred : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();

            base._effects.SetSymbol("fredDone", true);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}
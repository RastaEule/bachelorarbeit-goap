﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x4_goal_fred : GOAP_Goal
    {
        protected override void Awake()
        {
            base.Awake();
            base._goalState.SetSymbol("fredDone", true);
        }
    }
}
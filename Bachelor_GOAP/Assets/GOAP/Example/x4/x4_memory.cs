﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x4_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("hugoDone", false);
            base.WorldState.SetSymbol("fredDone", false);
            base.WorldState.SetSymbol("hugo1_done", false);
            base.WorldState.SetSymbol("hugo_special_precondition", true);
        }
    }
}
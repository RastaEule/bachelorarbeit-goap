﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x4_action_hugo1 : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();

            base._effects.SetSymbol("hugo1_done", true);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            // Set precondition of x4_action_hugo2 to false at runtime
            // -> x4_action_hugo2 should not be able to perform and the plan should fail
            worldState.SetSymbol("hugo_special_precondition", false);
            onFinished();
        }
    }
}
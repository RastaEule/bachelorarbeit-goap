﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x2_action_stage0_R : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._effects.SetSymbol("stage0_R_fin", true);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}
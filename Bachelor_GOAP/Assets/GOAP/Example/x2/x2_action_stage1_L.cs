﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x2_action_stage1_L : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._preconditions.SetSymbol("stage0_L_fin", true);
            base._effects.SetSymbol("stage1_L_fin", true);
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }
    }
}
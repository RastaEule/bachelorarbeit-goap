﻿using System.Collections;
using System.Collections.Generic;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x2_goal_stage2 : GOAP_Goal
    {
        protected override void Awake()
        {
            base.Awake();
            base._goalState.SetSymbol("stage2_fin", true);
        }
    }
}

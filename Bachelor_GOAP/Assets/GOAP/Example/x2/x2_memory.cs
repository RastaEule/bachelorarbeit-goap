﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x2_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("stage2_fin", false);
            base.WorldState.SetSymbol("stage1_L_fin", false);
            base.WorldState.SetSymbol("stage1_R_fin", false);
            base.WorldState.SetSymbol("stage1_X_fin", false);
            base.WorldState.SetSymbol("stage0_R_fin", false);
        }
    }
}
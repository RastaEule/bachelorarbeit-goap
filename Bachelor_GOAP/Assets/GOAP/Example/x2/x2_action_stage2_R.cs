﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x2_action_stage2_R : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._preconditions.SetSymbol("stage1_R_fin", true);
            base._preconditions.SetSymbol("stage1_X_fin", true);
            base._effects.SetSymbol("stage2_fin", true);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}
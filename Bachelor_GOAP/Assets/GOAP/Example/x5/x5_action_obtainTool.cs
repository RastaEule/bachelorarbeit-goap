﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x5_action_obtainTool : GOAP_Action
    {
        [SerializeField]
        private Transform _toolStorage = null;
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Vector3 _targetPos = Vector3.zero;
        private agent_motor _motor = null;

        private Action _actionFinishedCallback = null;
        private Action _actionFailedCallback = null;

        protected override void Awake()
        {
            base.Awake();
            _motor = GetComponent<agent_motor>();
            _targetPos = _toolStorage.position;

            base._effects.SetSymbol("atPos", _targetPos);
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);

            GOAP_State possibleTools = new GOAP_State();
            possibleTools.SetSymbol("hasTool_Axe", false);
            possibleTools.SetSymbol("hasToolFishingRod", false);
            effects -= possibleTools;

            object targetobj;
            if (plannedState.State.TryGetValue("targetTool", out targetobj))
            {
                string targetString = (string)targetobj;

                if (targetString.Equals("Axe") ||
                    targetString.Equals("FishingRod"))
                    effects.SetSymbol("hasTool_" + targetString, true);
            }
            return effects;
        }

        public override float GetCost(GOAP_State plannedState)
        {
            object atposobj;
            object targetposobj;
            if (plannedState.State.TryGetValue("atPos", out atposobj) && plannedState.State.TryGetValue("targetPos", out targetposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                Vector3 targetPos = (Vector3)targetposobj;
                if (atPos != null && targetposobj != null)
                    return base.GetCost(plannedState) + Vector3.Distance(atPos, targetPos);
            }
            return base.GetCost(plannedState);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            _actionFinishedCallback = onFinished;
            _actionFailedCallback = onFailed;

            _motor.Goto(_targetPos, MovementFinished, MovementFailed);
        }

        private IEnumerator PerformAction()
        {
            yield return new WaitForSeconds(_actionDuration);
            _actionFinishedCallback();
        }

        private void MovementFinished()
        {
            StartCoroutine(PerformAction());
        }

        private void MovementFailed()
        {
            _actionFailedCallback();
        }
    }
}
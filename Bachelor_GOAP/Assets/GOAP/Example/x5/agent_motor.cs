﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agent_motor : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed = 10f;
    [SerializeField]
    private float _moveTimeout = 20f;

    private bool _move = false;
    private Vector3 _targetPos = Vector3.zero;
    private Action _movementFinished = null;
    private Action _movementFailed = null;
    private float _startMoveTime = 0;

    private void FixedUpdate()
    {
        if (_move)
        {
            float step = _moveSpeed * Time.smoothDeltaTime;

            transform.position = Vector3.Lerp(transform.position, _targetPos, step);

            Vector3 newDir = Vector3.RotateTowards(transform.forward, _targetPos - transform.position, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);

            if (Vector3.Distance(transform.position, _targetPos) < 0.2f)
            {
                _move = false;
                _movementFinished();
            }
            if (Time.time - _startMoveTime > _moveTimeout)
            {
                _move = false;
                _movementFailed();
            }
        }
    }

    public void Goto(Vector3 targetPosition, Action onFinished, Action onFailed)
    {
        _movementFinished = onFinished;
        _movementFailed = onFailed;
        _targetPos = targetPosition;
        _startMoveTime = Time.time;
        _move = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x5_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("atPos", transform.position);

            base.WorldState.SetSymbol("holz_stored", false);
            base.WorldState.SetSymbol("has_Holz", false);
            base.WorldState.SetSymbol("fish_stored", false);
            base.WorldState.SetSymbol("has_Fish", false);
            base.WorldState.SetSymbol("isHungry", true);

            base.WorldState.SetSymbol("hasTool_Axe", false);
            base.WorldState.SetSymbol("hasTool_FishingRod", false);
        }
    }
}
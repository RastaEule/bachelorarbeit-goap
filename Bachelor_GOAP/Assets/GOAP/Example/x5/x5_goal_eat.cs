﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x5_goal_eat : GOAP_Goal
    {
        [SerializeField]
        private float _prioIncreasePerSecond = 0.05f;

        protected override void Awake()
        {
            base.Awake();
            base._goalState.SetSymbol("isHungry", false);
        }

        private void Start()
        {
            StartCoroutine(PriorityIncrease());
        }

        private IEnumerator PriorityIncrease()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                base._priority += _prioIncreasePerSecond;
            }
        }

        public override void OnPlanFinished()
        {
            base._priority = 0f;

            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol("isHungry", true);
        }
    }
}
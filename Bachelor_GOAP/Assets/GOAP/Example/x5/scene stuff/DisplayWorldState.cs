﻿using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GOAP.Core;

namespace GOAP.Example
{
    public class DisplayWorldState : MonoBehaviour
    {
        [SerializeField]
        private Text _textArea = null;

        public void Display(GOAP_State state)
        {
            _textArea.text = string.Empty;

            StringBuilder sb = new StringBuilder();

            List<KeyValuePair<string, object>> symbols = state.State.ToList();
            foreach(var kvp in symbols)
            {
                sb.AppendLine(kvp.Key + ":\t\t" + kvp.Value);
            }

            _textArea.text = sb.ToString();
        }
    }
}
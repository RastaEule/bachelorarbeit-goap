﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP.Example
{
    public class LookAtTransform : MonoBehaviour
    {
        private Transform _target = null;

        private void Awake()
        {
            _target = Camera.main.transform;
        }

        private void FixedUpdate()
        {
            if (_target != null)
            {
                float step = 5f * Time.smoothDeltaTime;

                Vector3 newDir = Vector3.RotateTowards(transform.forward, transform.position - _target.position, step, 0.0f);
                transform.rotation = Quaternion.LookRotation(newDir);
            }
        }
    }
}
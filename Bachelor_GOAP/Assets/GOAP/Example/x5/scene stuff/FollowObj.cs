﻿using UnityEngine;
using UnityEngine.EventSystems;

public class FollowObj : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Transform _objToFollow = null;

    /// <summary>
    /// UI Manager will cast this object to see if it can display stuff for this
    /// </summary>
    [SerializeField]
    [Tooltip("An object that contains info that is evaluated by UIManager")]
    private Transform _infoObj = null;

    private LZ_Camera.CameraController _cameraController = null;

    private void Awake()
    {
        _cameraController = Camera.main.transform.root.GetComponent<LZ_Camera.CameraController>();
    }

    #region IPointerClickHandler Members

    public void OnPointerClick(PointerEventData eventData)
    {
        _cameraController.FollowObject(_objToFollow, this.OnUnfollow);
        Schrubber.Game.GameManager.I.UIManager.DisplayInformation(_infoObj);
    }

    #endregion

    public void StopFollow()
    {
        _cameraController.StopFollowObject();
    }

    private void OnUnfollow()
    {
        Schrubber.Game.GameManager.I.UIManager.StopDisplayInformation();
    }
}
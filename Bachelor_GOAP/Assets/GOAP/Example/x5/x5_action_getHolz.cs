﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x5_action_getHolz : GOAP_Action
    {
        [SerializeField]
        private Transform _woods = null;
        [SerializeField]
        private float _actionDuration = 1.5f;

        private Vector3 _targetPos = Vector3.zero;
        private agent_motor _motor = null;

        private Action _actionFinishedCallback = null;
        private Action _actionFailedCallback = null;

        protected override void Awake()
        {
            base.Awake();
            _motor = GetComponent<agent_motor>();
            _targetPos = _woods.position;

            base._preconditions.SetSymbol("hasTool_Axe", true);
            base._preconditions.SetSymbol("has_Holz", false);
            base._effects.SetSymbol("has_Holz", true);
            base._effects.SetSymbol("hasTool_Axe", false);
            base._effects.SetSymbol("atPos", _targetPos);
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol("targetPos", _targetPos);
            plannedState.SetSymbol("targetTool", "Axe");
        }

        public override float GetCost(GOAP_State plannedState)
        {
            object atposobj;
            object targetposobj;
            if (plannedState.State.TryGetValue("atPos", out atposobj) && plannedState.State.TryGetValue("targetPos", out targetposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                Vector3 targetPos = (Vector3)targetposobj;
                if (atPos != null && targetposobj != null)
                    return base.GetCost(plannedState) + Vector3.Distance(atPos, targetPos);
            }
            return base.GetCost(plannedState);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            _actionFinishedCallback = onFinished;
            _actionFailedCallback = onFailed;

            _motor.Goto(_targetPos, MovementFinished, MovementFailed);
        }

        private IEnumerator PerformAction()
        {
            yield return new WaitForSeconds(_actionDuration);
            _actionFinishedCallback();
        }

        private void MovementFinished()
        {
            StartCoroutine(PerformAction());
        }

        private void MovementFailed()
        {
            _actionFailedCallback();
        }
    }
}
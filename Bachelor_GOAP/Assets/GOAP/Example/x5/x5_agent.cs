﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x5_agent : GOAP_Agent
    {
        [SerializeField]
        private DisplayWorldState _worldstateDisplay = null;

        protected override void Update()
        {
            base.Update();
            _worldstateDisplay?.Display(base.CurrentState);
        }
    }
}
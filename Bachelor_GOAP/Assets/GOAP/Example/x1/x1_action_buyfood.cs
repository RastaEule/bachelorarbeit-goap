﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x1_action_buyfood : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._preconditions.SetSymbol("hasMoney", true);
            base._effects.SetSymbol("hasFood", true);
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }
    }
}

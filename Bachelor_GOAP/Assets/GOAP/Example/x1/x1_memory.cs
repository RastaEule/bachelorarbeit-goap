﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x1_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("isHungry", true);
            base.WorldState.SetSymbol("hasFood", false);
            base.WorldState.SetSymbol("hasMoney", false);
        }
    }
}

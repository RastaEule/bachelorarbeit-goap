﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x1_goal_eat : GOAP_Goal
    {
        protected override void Awake()
        {
            base.Awake();
            base._goalState.SetSymbol("isHungry", false);
        }
    }
}
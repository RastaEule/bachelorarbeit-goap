﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x1_action_eatfood : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._preconditions.SetSymbol("hasFood", true);
            base._effects.SetSymbol("isHungry", false);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}


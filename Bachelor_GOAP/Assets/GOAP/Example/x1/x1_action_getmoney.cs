﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x1_action_getmoney : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._effects.SetSymbol("hasMoney", true);
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }
    }
}
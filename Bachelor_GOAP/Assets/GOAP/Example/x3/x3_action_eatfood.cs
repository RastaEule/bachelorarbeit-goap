﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x3_action_eatfood : GOAP_Action
    {
        protected override void Awake()
        {
            base.Awake();
            base._preconditions.SetSymbol("hasfood", true);
            base._effects.SetSymbol("hungry", false);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}


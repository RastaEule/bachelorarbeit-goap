﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;

namespace GOAP.Example
{
    public class x3_memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol("atPos", transform.position);
            base.WorldState.SetSymbol("hungry", true);
            base.WorldState.SetSymbol("hasfood", false);
            base.WorldState.SetSymbol("food_available", false);
        }
    }

}
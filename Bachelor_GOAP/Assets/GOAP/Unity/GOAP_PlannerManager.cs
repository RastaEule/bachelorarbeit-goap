﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using GOAP.Core;
using GOAP.Logging;

namespace GOAP.Unity
{
    /// <summary>
    /// Singleton class that handles threading
    /// </summary>
    public class GOAP_PlannerManager : MonoBehaviour
    {
        public static GOAP_PlannerManager Instance;

        /// <summary>
        /// If this is enabled, you cannot use unity functions in actions or goals.
        /// </summary>
        [SerializeField][Tooltip("If this is enabled, you cannot use unity functions in actions or goals.")]
        private bool _enableMultithreading = true;
        [SerializeField][Range(1, 8)][Tooltip("Has an impact only when enableMultithreading is true.")]
        private int _threadCount = 4;

        /// <summary>
        /// This event is called when no plan for any action could be found
        /// </summary>
        public event Action<IGOAP_Agent> OnFailed;

        /// <summary>
        /// All work to be done is in this FIFO list. When a thread is available, the next work will be completed
        /// </summary>
        private Queue<WorkInfo> _infos;

        /// <summary>
        /// All resulting plans from thread are saved in this FIFO list. They are given to their agent in Update()
        /// </summary>
        private Queue<GOAP_PlanResults> _planResults;

        /// <summary>
        /// All threads that can perform work
        /// </summary>
        private List<PlannerThread> _threads;

        private void Awake()
        {
            // Add a logger if there isn't one attached to this go
            if (!GetComponent<GOAP_Logger>())
                gameObject.AddComponent<GOAP_Logger>();

            _infos = new Queue<WorkInfo>();
            _planResults = new Queue<GOAP_PlanResults>();
            _threads = new List<PlannerThread>(_threadCount);
            for (int i = 0; i < _threadCount; i++)
                _threads.Add(new PlannerThread());

            if (Instance == null)
                Instance = this;
            else
                GOAP_Logger.I.Log("Only one GOAP_PlannerManager is allowed", EMessageType.Info, EMessageSeverity.Error);
        }

        private void Update()
        {
            // See if there is work to be done and a worker is available
            PlannerThread thread = null;
            if (_infos.Count > 0 && this.ThreadAvailable(out thread))
            {
                // Start working in  another thread
                WorkInfo i = _infos.Dequeue();
                thread.StartPlanning(i);
            }

            GOAP_PlanResults pr = null;
            lock (_planResults)
            {
                if (_planResults.Count > 0)
                    pr = _planResults.Dequeue();
            }
            this.HandlePlanResult(pr);
        }

        private void HandlePlanResult(GOAP_PlanResults pr)
        {
            if (pr != null && pr.Plan != null)
            {
                if (pr.FailedGoals != null)
                {
                    // Blacklist failed goals
                    foreach (IGOAP_Goal g in pr.FailedGoals)
                        g.Blacklist(true);
                }
                if (pr.Agent.ReplanAfterGoalsBlacklisted)
                {
                    pr.Agent.ReplanAfterGoalsBlacklisted = false;
                    pr.Agent.SortGoals();
                    this.PlanRequest(pr.Agent);
                }
                else
                    pr.Agent.StartPlanExecution(pr.Plan);
            }
        }

        /// <summary>
        /// Request some work to be done.
        /// </summary>
        public void PlanRequest(IGOAP_Agent agent)
        {
            agent.ReplanAfterGoalsBlacklisted = false;

            if (_enableMultithreading) // Multithreaded approach: work is done when there is a worker available
                _infos.Enqueue(new WorkInfo(agent));
            else // No multithreading: work is done directly in mainthread
            {
                GOAP_Planner planner = new GOAP_Planner(GOAP_Logger.I);
                planner.Plan(agent, OnPlanningDone, OnPlanningFailed);
            }
        }

        /// <summary>
        /// A plan was generated. This method will inform the agent of the plan
        /// </summary>
        public void OnPlanningDone(GOAP_PlanResults planResult)
        {
            if (_enableMultithreading)
            {
                lock (_planResults)
                {
                    _planResults.Enqueue(planResult);
                }
            }
            else
                this.HandlePlanResult(planResult);
        }

        public void OnPlanningFailed(GOAP_PlanResults planResult)
        {
            GOAP_Logger.I.Log(string.Format("{0} | ---- Planning failed: {1} ----", planResult.Agent.ToString(), planResult.Message), planResult.Agent, EMessageType.Planning, EMessageSeverity.Warning);
            OnFailed(planResult.Agent);
        }

        /// <summary>
        /// Is there any thread available, that isn't busy
        /// </summary>
        private bool ThreadAvailable(out PlannerThread thread)
        {
            thread = null;
            foreach(PlannerThread t in _threads)
            {
                thread = t;
                if (!t.IsAlive)
                    return true;
            }
            return false;
        }

        private void OnApplicationQuit()
        {
            if(_threads != null)
            {
                foreach(PlannerThread thread in _threads)
                {
                    thread.StopPlanning();
                }
            }
        }
    }

    /// <summary>
    /// Class for formulating plan in another thread
    /// </summary>
    internal class PlannerThread
    {
        private Thread _thread;

        public PlannerThread()
        {
        }

        public void StartPlanning(WorkInfo i)
        {
            _thread = new Thread(Plan);
            _thread.Start(i);
        }

        public void StopPlanning()
        {
            _thread?.Abort();
        }

        /// <summary>
        /// Is this thread running right now
        /// </summary>
        public bool IsAlive { get { return _thread == null ? false : _thread.IsAlive; } }

        private static void Plan(object info)
        {
            WorkInfo i = (WorkInfo)info;
            GOAP_Planner planner = new GOAP_Planner(GOAP_Logger.I);
            planner.Plan(i.Agent, GOAP_PlannerManager.Instance.OnPlanningDone, GOAP_PlannerManager.Instance.OnPlanningFailed);
        }
    }

    /// <summary>
    /// Class that contains all informations needed for the GOAP_Planner to formulate a plan. Currently it only needs to know the agent
    /// </summary>
    internal class WorkInfo
    {
        internal WorkInfo(IGOAP_Agent agent)
        {
            Agent = agent;
        }

        internal IGOAP_Agent Agent { private set; get; }
    }
}
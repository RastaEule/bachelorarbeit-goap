﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using GOAP.Logging;

namespace GOAP.Unity
{
    public class GOAP_Agent : MonoBehaviour, IGOAP_Agent
    {
        /// <summary>
        /// Start planning in Start()
        /// </summary>
        [SerializeField] [Tooltip("Start planning in Start()")]
        private bool _planOnStart = true;
        /// <summary>
        /// How often the agent checks, if there is another goal with a higher priority. To disable any checks, use the value -1
        /// </summary>
        [SerializeField] [Tooltip("How often the agent checks, if there is another goal with a higher priority. To disable any checks, use the value -1")]
        private float _goalPriorityUpdateInterval = 2f;
        /// <summary>
        /// When was the last GoalPriorityUpdate performed
        /// </summary>
        private float _lastGoalPriorityUpdate = 0f;

        protected List<IGOAP_Action> _actions;
        protected List<IGOAP_Goal> _goals;
        private GOAP_Memory _memory;
        private string _name = "DefaultAgent";

        private bool _goalPrioUpdateNeeded = true;

        private GOAP_PlanRunner _planRunner;

        private bool _planningFailed = false;

        #region Unity

        protected virtual void Awake()
        {
            this.InitGoalsActions();
            _memory = GetComponent<GOAP_Memory>();
            _memory.Init(this);
            _name = this.gameObject.name;
            _planRunner = new GOAP_PlanRunner(this);

            // Check for errors
            bool error = false;
            if (_memory == null)
            {
                Debug.LogError("No valid memory found!");
                error = true;
            }
            if (_goals.Count == 0)
            {
                Debug.LogError("No valid goals found!");
                error = true;
            }
            if (_actions.Count == 0)
            {
                Debug.LogError("No valid actions found!");
                error = true;
            }
            if (error)
            {
                _planOnStart = false; // Disable any planning
                _goalPriorityUpdateInterval = -1; // Disable goal priority checks
            }
        }

        protected virtual void Start()
        {
            GOAP_PlannerManager.Instance.OnFailed += this.AnyPlanningFailed;
            if (_planOnStart)
                this.GeneratePlan();
        }

        protected virtual void Update()
        {
            if (!IsActive)
                return;

            if (_goalPrioUpdateNeeded && _goalPriorityUpdateInterval > 0 && // If this check is enabled
                Time.time - _lastGoalPriorityUpdate > _goalPriorityUpdateInterval)
            {
                _lastGoalPriorityUpdate = Time.time;
                this.UpdateGoalPriorities();
            }

            if (_planningFailed)
            {
                _planningFailed = false;
                this.OnPlanningFailed();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get all goals and actions that are attached to this GO
        /// </summary>
        private void InitGoalsActions()
        {
            _goals = new List<IGOAP_Goal>(GetComponents<IGOAP_Goal>());
            _actions = new List<IGOAP_Action>(GetComponents<IGOAP_Action>());
        }


        private void UpdateGoalPriorities()
        {
            if (!IsActive)
                return;

            this.SortGoals();
            IGOAP_Goal currentGoal = _planRunner.Plan?.Goal;

            if (currentGoal != null &&
                _goals[0] != currentGoal &&
                _goals[0].IsPossible &&
                !_goals[0].IsBlackListed)
            {
                // Some other goal is now more relevant than the current goal
                GOAP_Logger.I.Log(string.Format("{0} | Found a goal with higher priority. Abort plan execution. Old: {1}, New: {2}.", this.ToString(), currentGoal?.ToString(), _goals[0].ToString()), this, EMessageType.PlanExecution, EMessageSeverity.Info);
                _planRunner.StopPlanExecution(); // Cancel current plan execution and search for a new plan
                _goalPrioUpdateNeeded = false;
            }
        }

        #endregion

        #region IGOAP_Agent Members

        public void GeneratePlan()
        {
            if (!IsActive)
                return;
            // Generate a new plan
            this.SortGoals();
            GOAP_PlannerManager.Instance.PlanRequest(this);
        }

        public void StartPlanExecution(GOAP_Plan plan)
        {
            if (!IsActive)
                return;

            _lastGoalPriorityUpdate = Time.time;
            _goalPrioUpdateNeeded = true;

            this.GoalChanged(plan.Goal);
            _planRunner.StartPlanExecution(plan);
        }

        /// <summary>
        /// Sorts all goals by their priority. Highest Priority first
        /// </summary>
        public void SortGoals()
        {
            _goals = _goals.OrderByDescending(x => x.Priority).ToList();
        }

        public List<IGOAP_Action> GetActions()
        {
            return _actions;
        }

        public List<IGOAP_Goal> GetGoals()
        {
            return _goals;
        }

        public GOAP_PlanRunner Planrunner => _planRunner;

        public GOAP_State CurrentState
        {
            get { return _memory.WorldState; }
            set { _memory.WorldState = value; }
        }

        public bool ReplanAfterGoalsBlacklisted { set; get; } = false;

        #endregion

        /// <summary>
        /// The goal with the highest priority changed
        /// </summary>
        protected virtual void GoalChanged(IGOAP_Goal g)
        {
        }

        private void AnyPlanningFailed(IGOAP_Agent agent)
        {
            if (agent == this)
                _planningFailed = true;
        }

        protected virtual void OnPlanningFailed()
        {
        }

        public bool IsActive { get; set; } = true;

        public override string ToString()
        {
            return _name;
        }
    }
}
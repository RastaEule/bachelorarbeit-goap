﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GOAP.Core;

namespace GOAP.Logging
{
    public class GOAP_Logger : MonoBehaviour, IGOAP_Logger
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        public static GOAP_Logger I;

        [SerializeField]
        private ELoggingStyle _loggingStyle = ELoggingStyle.AllAgents;

        [SerializeField]
        [HideInInspector]
        private EMessageType _messageType;

        private Queue<Goap_LogMessage> _messagesToWrite;

        private void Awake()
        {
            I = this;
            _messagesToWrite = new Queue<Goap_LogMessage>();
        }

        // Update is called once per frame
        void Update()
        {
            Goap_LogMessage msg = null;
            lock (_messagesToWrite)
            {
                if (_messagesToWrite.Count > 0)
                    msg = _messagesToWrite.Dequeue();
            }

            // Check if this message can be displayed
            bool displayMsg = false;
            if (msg != null) // Message has to exist to be displayed
            {
                // Display only if the flag is set
                if (_messageType.HasFlag(msg.Type))
                    displayMsg = true;

#if UNITY_EDITOR
                // Check for selected agent
                if (_loggingStyle == ELoggingStyle.SelectedAgents && msg.Agent != null)
                {
                    IGOAP_Agent agent = Selection.activeGameObject?.GetComponent<IGOAP_Agent>();
                    displayMsg = msg.Agent.Equals(agent);
                }
#endif
            }

            if (displayMsg)
            {
                string pre = "---- ";
                switch (msg.Severity)
                {
                    case EMessageSeverity.Error:
                        Debug.LogError(msg.Message);
                        pre = "GOAP_ERROR: ";
                        break;
                    case EMessageSeverity.Warning:
                        Debug.LogWarning(msg.Message);
                        pre = "GOAP_WARNING: ";
                        break;
                    case EMessageSeverity.Info:
                        Debug.Log(msg.Message);
                        pre = "GOAP_INFO: ";
                        break;
                    default:
                        Debug.Log(msg.Message);
                        break;
                }
                System.Diagnostics.Debug.WriteLine(pre + msg.Message);
            }
        }

        public void Log(string message, IGOAP_Agent agent, EMessageType type, EMessageSeverity severity)
        {
            lock (_messagesToWrite)
            {
                _messagesToWrite.Enqueue(new Goap_LogMessage(message, agent, type, severity));
            }
        }

        public void Log(string message, EMessageType type, EMessageSeverity severity)
        {
            this.Log(message, null, type, severity);
        }

        public void Log(string message)
        {
            this.Log(message, null, EMessageType.Info, EMessageSeverity.Info);
        }

        public EMessageType MessageFlags
        {
            set { _messageType = value; }
            get { return _messageType; }
        }
    }

    public enum EMessageSeverity { Unknown, Info, Warning, Error };

    [Flags]
    public enum EMessageType
    {
        Planning = 1 << 1,
        PlanExecution = 1 << 2,
        Info = 1 << 3
    };

    internal enum ELoggingStyle { AllAgents, SelectedAgents};

    internal class Goap_LogMessage
    {
        public Goap_LogMessage(string message, IGOAP_Agent agent, EMessageType type, EMessageSeverity severity)
        {
            Message = message;
            Agent = agent;
            Type = type;
            Severity = severity;
        }

        public string Message { private set; get; }

        public IGOAP_Agent Agent { private set; get; }

        public EMessageType Type { private set; get; }

        public EMessageSeverity Severity { private set; get; }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;

namespace GOAP.Unity
{
    /// <summary>
    /// This class holds the actual world state of this agent
    /// </summary>
    public class GOAP_Memory : MonoBehaviour
    {
        /// <summary>
        /// Update the sensors every x seconds
        /// </summary>
        [SerializeField][Tooltip("Update the sensors every x seconds")]
        protected float _sensorUpdateInterval = 0.4f;
        private float _lastSensorUpdateTime = 0f;

        private List<GOAP_Sensor> _sensors;

        #region Unity

        protected virtual void Awake()
        {
            WorldState = new GOAP_State();
            this.RefreshSensors();
        }

        private void Start()
        {
            this.UpdateSensors();
        }

        private void Update()
        {
            if (Time.time - _lastSensorUpdateTime > _sensorUpdateInterval)
            {
                // Update sensors
                _lastSensorUpdateTime = Time.time;
                this.UpdateSensors();
            }
        }

        #endregion Unity

        /// <summary>
        /// Get all sensors that are attached to this agent
        /// </summary>
        private void RefreshSensors()
        {
            _sensors = new List<GOAP_Sensor>(GetComponents<GOAP_Sensor>());
        }

        /// <summary>
        /// Update all sensors
        /// </summary>
        private void UpdateSensors()
        {
            foreach (GOAP_Sensor s in _sensors)
            {
                s.UpdateSensor(this);
            }
        }

        public virtual void Init(GOAP_Agent agent)
        {
        }

        public GOAP_State WorldState { get; set; }
    }
}

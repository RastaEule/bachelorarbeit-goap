﻿using UnityEngine;

namespace GOAP.Unity
{
    public abstract class GOAP_Sensor : MonoBehaviour
    {
        /// <summary>
        /// Update this sensor and write the result in worlState
        /// </summary>
        public abstract void UpdateSensor(GOAP_Memory memory);
    }
}

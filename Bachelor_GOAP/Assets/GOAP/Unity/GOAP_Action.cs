﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;

namespace GOAP.Unity
{
    public abstract class GOAP_Action : MonoBehaviour, IGOAP_Action
    {
        [SerializeField][Tooltip("The cost of this action")]
        protected float _cost = 1f;
        [SerializeField][Tooltip("The name of this action")]
        protected string _name = "DefaultAction";

        protected GOAP_State _preconditions;
        protected GOAP_State _effects;

        #region Unity

        protected virtual void Awake()
        {
            _preconditions = new GOAP_State();
            _effects = new GOAP_State();
        }

        #endregion

        #region IGOAP_Action Members

        public virtual GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public virtual GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public GOAP_State GetStaticPreconditions()
        {
            return _preconditions;
        }

        public GOAP_State GetStaticEffects()
        {
            return _effects;
        }

        public virtual void GetPrecalculations(GOAP_State plannedState)
        {

        }

        public virtual float GetCost(GOAP_State plannedState)
        {
            return _cost;
        }

        public virtual bool IsPossible(GOAP_State plannedState)
        {
            return true;
        }

        public abstract void Run(Action onFinished, Action onFailed, GOAP_State worldState);

        #endregion

        public override string ToString()
        {
            return "Goap_Action " + _name;
        }
    }
}

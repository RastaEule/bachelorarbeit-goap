﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using GOAP.Core;
using System.Text;
using Schrubber.GOAP;

namespace GOAP.Unity
{
    public class AgentState_Window : EditorWindow
    {
        [MenuItem("Window/GOAP/AgentState")]
        public static void Init()
        {
            AgentState_Window window = GetWindow<AgentState_Window>("Agent State");
            window.Show();
        }

        private Vector2 _scrollPos = Vector2.zero;
        

        private List<KeyValuePair<string, object>> _symbols;
        private GameObject _selectedGO;
        private IGOAP_Agent _agent;

        private void OnGUI()
        {
            if (!EditorApplication.isPlaying)
                return;

            _scrollPos = GUILayout.BeginScrollView(_scrollPos);
            // Infomation about an agent
            if (_symbols != null && _symbols.Count > 0 && _agent != null)
            {
                GUILayout.Label("World State of " + _agent.ToString(), EditorStyles.boldLabel);

                if (_agent is Schrubber.GOAP.Schrubber_Agent)
                    GUILayout.Label("PersonalGoalUtility:\t\t"+ (_agent as Schrubber.GOAP.Schrubber_Agent).GetPersonalUtility());

                foreach (var kvp in _symbols)
                    GUILayout.Label(kvp.Key + ":\t\t" + kvp.Value);

                GUILayout.Space(10);
                GUILayout.Label("Goal Priorities", EditorStyles.boldLabel);

                foreach(IGOAP_Goal g in _agent.GetGoals())
                {
                    StringBuilder sb = new StringBuilder();
                    if (g.IsPossible)
                        sb.Append("+ ");
                    else
                        sb.Append("- ");

                    sb.Append(g.ToString());
                    if (g.IsBlackListed)
                        sb.Append(" (Blacklisted)");

                    SchrubberGoal sg = (SchrubberGoal)g;
                    if (sg != null &&
                        sg.SquadTarget)
                        sb.Append(" (ST)");
                    sb.AppendFormat(g.Priority.ToString().PadLeft(20));

                    GUILayout.Label(sb.ToString());
                }

                GOAP_PlanRunner planRunner = _agent.Planrunner;
                GOAP_Plan plan = planRunner?.Plan;
                if (plan != null)
                {
                    GUILayout.Space(10);
                    GUILayout.Label("Current Plan (" + plan.Goal.ToString() + ")", EditorStyles.boldLabel);

                    List<IGOAP_Action> actionPlan = plan.Plan.ToList();
                    GUILayout.Label(planRunner.CurrentActionName);
                    foreach (IGOAP_Action a in actionPlan)
                        GUILayout.Label(a.ToString());
                }

                if (_agent is Schrubber.GOAP.Schrubber_Agent)
                {
                    GUILayout.Space(10);
                    GUILayout.Label("Inventory", EditorStyles.boldLabel);
                    Schrubber.GOAP.Schrubber_Agent schrubberAgent = _agent as Schrubber.GOAP.Schrubber_Agent;

                    foreach (KeyValuePair<Schrubber.Item.EItem, List<Schrubber.Item.Item>> kvp in schrubberAgent.Blackboard.Inventory.InternalInventory)
                    {
                        GUILayout.Label(kvp.Key.ToString() + "\t\t" + kvp.Value.Count);
                    }
                }
            }

            // Information about Storage Inventory
            if (_selectedGO != null)
            {
                Dictionary<Schrubber.Item.EItem, List<Schrubber.Item.Item>> inv = null;
                string invName = "";

                Schrubber.Item.Storage storage = _selectedGO.GetComponent<Schrubber.Item.Storage>();
                Schrubber.Game.Dungeon.DungeonStorage dungeonStorage = _selectedGO.GetComponent<Schrubber.Game.Dungeon.DungeonStorage>();

                if (storage != null)
                {
                    inv = storage.Inventory.InternalInventory;
                    invName = storage.ToString();
                }
                if (dungeonStorage != null)
                {
                    inv = dungeonStorage.Inventory.InternalInventory;
                    invName = dungeonStorage.ToString();
                }

                if (inv != null)
                {
                    GUILayout.Space(20);
                    GUILayout.Label("Inventory of " + invName, EditorStyles.boldLabel);
                    foreach (KeyValuePair<Schrubber.Item.EItem, List<Schrubber.Item.Item>> kvp in inv)
                    {
                        GUILayout.Label(kvp.Key.ToString() + "\t\t" + kvp.Value.Count);
                    }
                }
            }
            GUILayout.EndScrollView();
        }

        private void Update()
        {
            if (EditorApplication.isPlaying)
            {
                _selectedGO = Selection.activeGameObject;
                IGOAP_Agent agent = _selectedGO?.GetComponentInChildren<IGOAP_Agent>();

                if (_agent == null && agent != null) // Es gibt keinen alten agent, aber dafür einen neuen
                {
                    _agent = agent;
                    _symbols = agent.CurrentState.State.ToList();
                    Repaint();
                }
                else if (_agent != null) // Es gibt einen alten agent
                {
                    if (agent == null || agent.Equals(_agent)) // Es gibt keinen neuen agent oder der gleiche wie vorher ist ausgewählt
                        _symbols = _agent.CurrentState.State.ToList();
                    else // Ein neuer Agent wurde ausgewählt
                    {
                        _agent = agent;
                        _symbols = agent.CurrentState.State.ToList();
                    }
                    Repaint();
                }
                Repaint();
            }
        }
    }
}
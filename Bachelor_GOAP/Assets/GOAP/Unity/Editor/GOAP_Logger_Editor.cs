﻿using UnityEditor;
using UnityEngine;
using GOAP.Logging;

namespace GOAP.Unity
{
    [CustomEditor(typeof(GOAP_Logger))]
    public class GOAP_Logger_Editor : Editor
    {
        private EMessageType _messages;
        private float _gameSpeed = 1;

        public override void OnInspectorGUI()
        {
            base.DrawDefaultInspector();

            GOAP_Logger logger = (GOAP_Logger)target;
            logger.MessageFlags = (EMessageType)EditorGUILayout.EnumFlagsField("Messages Amount", logger.MessageFlags);
            serializedObject.Update();

            GUILayout.Space(10);
            _gameSpeed = EditorGUILayout.Slider("GameSpeed", _gameSpeed, 0.2f, 100);
            Time.timeScale = _gameSpeed;
        }
    }
}
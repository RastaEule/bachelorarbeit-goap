﻿using System.Collections.Generic;

namespace GOAP.Core
{
    public class GOAP_State
    {
        private Dictionary<string, object> _state;

        public GOAP_State()
        {
            _state = new Dictionary<string, object>();
        }

        public void SetSymbol(KeyValuePair<string, object> symbol)
        {
            this.SetSymbol(symbol.Key, symbol.Value);
        }

        public void SetSymbol(string key, object value)
        {
            if (_state.ContainsKey(key))
                _state[key] = value;
            else
                _state.Add(key, value);
        }

        public Dictionary<string, object> State => _state;

        #region Helper functions

        /// <summary>
        /// Are two states exactly the same. Same symbols with same values
        /// </summary>
        public override bool Equals(object obj)
        {
            GOAP_State s = (GOAP_State)obj;
            if (s == null)
                return false;
            else
            {
                // Check all symbols in this state
                foreach (KeyValuePair<string, object> kvp in _state)
                    if (!s.State.ContainsKey(kvp.Key) || !s.State[kvp.Key].Equals(kvp.Value))
                        return false; // Not equal, if any key doesnt exist in other state or value is different

                // Check all symbols in state s
                foreach (KeyValuePair<string, object> kvp in s.State)
                    if (!_state.ContainsKey(kvp.Key))
                        return false; // Not equal, if any key doesn't exist in other state

                return true;
            }
        }

        public override int GetHashCode() // This was only implemented to remove the compiler warning
        {
            Logging.GOAP_Logger.I.Log("HashCode not implemented", Logging.EMessageType.Info, Logging.EMessageSeverity.Error);
            return -1;
        }

        /// <summary>
        /// All symbols from s exist and are equal. Sequence matters: s1.EqualsPart(s2) != s2.EqualsPart(s1)
        /// </summary>
        public bool IsGoal(GOAP_State goalState)
        {
            // Check all symbols in state s
            foreach(KeyValuePair<string, object> kvp in goalState.State)
            {
                bool containsKey = false;
                bool equals = false;
                if (_state.ContainsKey(kvp.Key))
                {
                    containsKey = true;
                    equals = object.Equals(_state[kvp.Key], kvp.Value);
                }
                if (!containsKey || !equals)
                    return false;
                
                //if (!_state.ContainsKey(kvp.Key) || !object.Equals(_state[kvp.Key], kvp.Value)) // This does not work
                //    return false; // Not equal, if a key doesnt' exist in this state or value is different
            }
            return true;
        }

        /// <summary>
        /// Are there any symbols that have the same value
        /// </summary>
        public bool HasMatchingSymbols(GOAP_State s)
        {
            return this.HasMatchingSymbols(s, out GOAP_State matchingSymbols);
        }

        /// <summary>
        /// Are there any symbols that have the same value
        /// </summary>
        /// <param name="matchingSymbols">All equal symbols</param>
        /// <returns></returns>
        public bool HasMatchingSymbols(GOAP_State s, out GOAP_State matchingSymbols)
        {
            matchingSymbols = new GOAP_State();
            foreach (KeyValuePair<string, object> kvp in _state)
                if (s.State.ContainsKey(kvp.Key) && s.State[kvp.Key].Equals(kvp.Value)) // Key exists and value is the same
                    matchingSymbols.SetSymbol(kvp);

            return matchingSymbols.State.Count > 0;
        }

        /// <summary>
        /// Any symbols in s, that either don't exist or have a different value. Result values originate from s.
        /// </summary>
        /// <param name="mismatchingSymbols">All symbols with the value of s. Their value in this state is different.</param>
        public bool HasMismatchingSymbols(GOAP_State s, out GOAP_State mismatchingSymbols)
        {
            // The symbol is mismatching if it only exists in in s or the value is different
            mismatchingSymbols = new GOAP_State();
            foreach(KeyValuePair<string, object> kvp in s.State)
                if (!_state.ContainsKey(kvp.Key) || !_state[kvp.Key].Equals(kvp.Value))
                    mismatchingSymbols.SetSymbol(kvp);

            return mismatchingSymbols.State.Count > 0;
        }

        /// <summary>
        /// Performs a deep copy of this instance
        /// </summary>
        public GOAP_State Copy()
        {
            GOAP_State result = new GOAP_State();
            Dictionary<string, object> obj = new Dictionary<string, object>(_state);

            foreach(KeyValuePair<string, object> kvp in obj)
                result.SetSymbol(kvp.Key, kvp.Value);
            return result;
        }

        /// <summary>
        /// Add two states
        /// </summary>
        public static GOAP_State operator+ (GOAP_State a, GOAP_State b)
        {
            GOAP_State result = new GOAP_State();

            // Add all symbols that exists in a. If they also exist in b, they get the value of b
            foreach (KeyValuePair<string, object> kvp in a.State)
            {
                KeyValuePair<string, object> symbol = kvp;
                if (b.State.ContainsKey(symbol.Key)) // If this symbol exists in a and b
                    symbol = new KeyValuePair<string, object>(symbol.Key, b.State[kvp.Key]); // Use value of b

                result.SetSymbol(symbol);
            }
            // Add all symbols that are only in b
            foreach (KeyValuePair<string, object> kvp in b.State)
            {
                // Add this symbol, if it doesn't exist yet
                if (!result.State.ContainsKey(kvp.Key))
                    result.SetSymbol(kvp);
            }
            return result;
        }

        /// <summary>
        /// Remove symbols. Value doesn't matter
        /// </summary>
        /// <param name="a">State to remove symbols from</param>
        /// <param name="b">Which symbols to remove</param>
        /// <returns></returns>
        public static GOAP_State operator- (GOAP_State a, GOAP_State b)
        {
            GOAP_State result = new GOAP_State();

            foreach (KeyValuePair<string, object> kvp in a.State)
                if (!b.State.ContainsKey(kvp.Key)) // If this symbol only exists in a -> didn't get removed
                    result.SetSymbol(kvp);

            return result;
        }

        #endregion
    }
}
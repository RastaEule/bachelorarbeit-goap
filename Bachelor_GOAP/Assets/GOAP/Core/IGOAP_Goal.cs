﻿namespace GOAP.Core
{
    public interface IGOAP_Goal
    {
        float Priority { get; set; }

        /// <summary>
        /// Is this goal possible at all
        /// </summary>
        bool IsPossible { get; set; }

        /// <summary>
        /// Is this goal blacklisted
        /// </summary>
        bool IsBlackListed { get; }

        /// <summary>
        /// Planner couldn't find a valid plan for this goal -> blacklist it
        /// </summary>
        /// <param name="byPlanner">Was this goal blacklisted by the planner or someone else</param>
        void Blacklist(bool byPlanner);

        GOAP_State GetGoalState();

        /// <summary>
        /// A plan for this goal did successfully finish.
        /// </summary>
        void OnPlanFinished();

        /// <summary>
        /// A plan for this goal failed to execute
        /// </summary>
        void OnPlanFailed();
    }
}
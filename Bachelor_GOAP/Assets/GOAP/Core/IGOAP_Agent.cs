﻿using System.Collections.Generic;

namespace GOAP.Core
{
    public interface IGOAP_Agent
    {
        /// <summary>
        /// Get all actions of this agent
        /// </summary>
        List<IGOAP_Action> GetActions();

        /// <summary>
        /// Get all goals of this agent
        /// </summary>
        List<IGOAP_Goal> GetGoals();

        /// <summary>
        /// Tell agent to generate a new plan
        /// </summary>
        void GeneratePlan();

        /// <summary>
        /// Sort all goals by priority. Highest Priority comes first
        /// </summary>
        void SortGoals();

        /// <summary>
        /// Start executing a given plan
        /// </summary>
        void StartPlanExecution(GOAP_Plan plan);

        /// <summary>
        /// Use this to toggle if the Planner should generate a new plan after blacklisting goals.
        /// It gets reset to false by the PlannerManager after Handling it once. So you have to set it to true OnGoalBlacklisted()
        /// 
        /// Usage: TargetGoal was blacklisted -> change Priorities of other goals and set this property to true
        /// Then the plannerManager will generate a new plan using the changed values. This will be done only once though
        /// </summary>
        bool ReplanAfterGoalsBlacklisted { set;  get; }

        /// <summary>
        /// Returns the actual current World State
        /// </summary>
        GOAP_State CurrentState { get; set; }

        /// <summary>
        /// Returns the current plan. Is only used for debug display
        /// </summary>
        GOAP_PlanRunner Planrunner { get; }
    }
}

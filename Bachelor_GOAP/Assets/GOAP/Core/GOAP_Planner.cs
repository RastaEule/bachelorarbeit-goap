﻿using System;
using System.Collections.Generic;
using GOAP.Pathfinding;
using GOAP.Logging;

namespace GOAP.Core
{
    public class GOAP_Planner
    {
        private IGOAP_Logger _logger;

        public GOAP_Planner(IGOAP_Logger logger)
        {
            _logger = logger;
        }

        public void Plan(IGOAP_Agent agent, Action<GOAP_PlanResults> onDone, Action<GOAP_PlanResults> onFailed)
        {
            GOAP_PlanResults result = new GOAP_PlanResults { Agent = agent };
            try
            {
                AStar astar = new AStar(agent, _logger);

                List<IGOAP_Goal> goals = agent.GetGoals();
                List<IGOAP_Goal> failedGoals = new List<IGOAP_Goal>();

                GOAP_Plan plan = null;
                bool pathFound = false;
                // Try each goal if they fail
                _logger.Log(string.Format("{0} |---- Planning started ----", agent.ToString()), agent, EMessageType.Planning, EMessageSeverity.Info);
                foreach (IGOAP_Goal g in goals)
                {
                    GOAP_State goalState = g.GetGoalState();
                    GOAP_State currentState = agent.CurrentState.Copy();

                    if (g.IsPossible &&
                        !g.IsBlackListed &&
                        !currentState.IsGoal(goalState)) // goal isn't already fulfilled
                    {
                        if (astar.FindPath(goalState, currentState, out Queue<IGOAP_Action> path))// Search for path
                        {
                            pathFound = true;
                            plan = new GOAP_Plan(path, agent, g);
                            break;
                        }
                        else
                            failedGoals.Add(g);
                    }
                }
                result.FailedGoals = failedGoals;
                if (pathFound)
                {
                    _logger.Log(agent.ToString() + " | " + plan.ToString(), agent, EMessageType.Planning, EMessageSeverity.Info);
                    _logger.Log(string.Format("{0} | ---- Planning ended successfully ----", agent.ToString()), agent, EMessageType.Planning, EMessageSeverity.Info);
                    result.Plan = plan;
                    onDone(result);
                }
                else
                {
                    result.Message = agent.ToString() + " | No valid path for any goal found";
                    onFailed(result);
                }
            }
            catch(Exception e)
            {
                result.Message = agent.ToString() + " | " + e.ToString();
                onFailed(result);
            }
        }
    }
}
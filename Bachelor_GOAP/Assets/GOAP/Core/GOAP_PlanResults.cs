﻿using System.Collections.Generic;

namespace GOAP.Core
{
    /// <summary>
    /// This is a context class that contains informations about a generated plan.
    /// It is a separate class, so the GOAP_Planner has to use less parameters in the callbacks
    /// </summary>
    public class GOAP_PlanResults
    {
        public GOAP_PlanResults()
        {
        }

        public GOAP_Plan Plan { set; get; }

        public IGOAP_Agent Agent { set; get; }

        public string Message { set; get; } = "null";

        /// <summary>
        /// Goals the planner couldn't find a valid plan for
        /// </summary>
        public List<IGOAP_Goal> FailedGoals { set; get; }
    }
}
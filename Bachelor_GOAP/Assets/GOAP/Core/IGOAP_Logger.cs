﻿using GOAP.Core;

namespace GOAP.Logging
{
    public interface IGOAP_Logger
    {
        void Log(string message, IGOAP_Agent agent, EMessageType type, EMessageSeverity severity);

        void Log(string message, EMessageType type, EMessageSeverity severity);

        void Log(string message);

        EMessageType MessageFlags{ set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GOAP.Core
{
    public class GOAP_Plan
    {
        public GOAP_Plan(Queue<IGOAP_Action> plan, IGOAP_Agent agent, IGOAP_Goal goal)
        {
            Plan = plan;
            Agent = agent;
            Goal = goal;
        }

        /// <summary>
        /// All actions in this plan
        /// </summary>
        public Queue<IGOAP_Action> Plan { private set; get; }

        /// <summary>
        /// The goal this plan has a solution for
        /// </summary>
        public IGOAP_Goal Goal { private set; get; }

        /// <summary>
        /// For which agent is this plan
        /// </summary>
        public IGOAP_Agent Agent { private set; get; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            string goalString = Goal == null ? "??" : Goal.ToString();
            sb.Append("Plan for " + goalString + ":\t\t");

            if (Plan == null)
                sb.Append("Invalid plan");
            else
            {
                List<IGOAP_Action> planList = Plan.ToList();
                for (int i = 0; i < planList.Count; i++)
                {
                    sb.Append(planList[i].ToString());
                    if (i < planList.Count - 1)
                        sb.Append(" -> ");
                }
            }
            return sb.ToString();
        }
    }
}

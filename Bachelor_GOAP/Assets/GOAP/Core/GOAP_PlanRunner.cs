﻿using System;
using GOAP.Logging;

namespace GOAP.Core
{
    /// <summary>
    /// This class manages the execution of a plan
    /// </summary>
    public class GOAP_PlanRunner
    {
        private IGOAP_Agent _agent;
        private GOAP_Plan _currentPlan = null;
        private IGOAP_Action _currentAction = null;

        /// <summary>
        /// Plan execution should be cancelled
        /// </summary>
        private bool _planCancelRequest = false;


        /// <summary>
        /// The last plan of this PlanRunner.
        /// Multithreading: Planner is already generating a new plan, so the currentPlan is null.
        /// Old actions although can still finish/fail. To handle those, save the old plan in this variable.
        /// </summary>
        private GOAP_Plan _lastPlan = null;
        /// <summary>
        /// The last action of this PlanRunner.
        /// Multithreading: Planner is already generating a new plan, so the currentAction is null.
        /// Old actions although can still finish/fail. To handle those, save the old action in this variable.
        /// </summary>
        private IGOAP_Action _lastAction = null;
        /// <summary>
        /// Multithreading: Is the planner currently planning a new plan
        /// </summary>
        private bool _currentlyPlanning = false;

        public GOAP_PlanRunner(IGOAP_Agent agent)
        {
            _agent = agent;
        }

        public void StartPlanExecution(GOAP_Plan plan)
        {
            if (plan == null || plan.Plan.Count <= 0)
            {
                this.PlanExecutionFinished(); // No plan to execute
            }
            else
            {
                _currentlyPlanning = false;
                _currentPlan = plan;
                // Execute the first action
                this.OnActionFinished();
            }
        }

        /// <summary>
        /// Stop the execution of the current plan and request a new plan.
        /// </summary>
        public void StopPlanExecution()
        {
            if (_currentPlan == null) // No plan available, just generate a new plan
                this.PlanExecutionFinished();
            else
                _planCancelRequest = true; // Generate a new plan after the next action is completed
        }

        /// <summary>
        /// This plan is finished. Tell Agent to formulate a new plan.
        /// </summary>
        private void PlanExecutionFinished()
        {
            if (!_currentlyPlanning)
            {
                _planCancelRequest = false; // Reset cancel request
                _currentAction = null; // Reset current action

                _currentlyPlanning = true;
                _agent.GeneratePlan();
            }
        }

        /// <summary>
        /// Current method finished. Execute the next method in the plan.
        /// </summary>
        private void OnActionFinished()
        {
            IGOAP_Action action = _currentAction;
            GOAP_Plan plan = _currentPlan;
            if (_currentlyPlanning)
            {
                action = _lastAction;
                plan = _lastPlan;
            }

            if (action != null)
            {
                // This action was finished
                // Apply effects to players WorldState
                _agent.CurrentState = _agent.CurrentState.Copy() + action.GetStaticEffects() + action.GetDynamicEffects(_agent.CurrentState);
            }

            if (plan == null)
            {
                GOAP_Logger.I.Log(_agent.ToString() + " | Unexpected error: currentPlan in PlanRunner is null. CurrentlyPlanning = " + _currentlyPlanning + "; LastPlan = " + _lastPlan.ToString(), EMessageType.PlanExecution, EMessageSeverity.Error);
                return;
            }

            if (plan.Plan.Count == 0 || _planCancelRequest)
            {
                if (plan.Plan.Count == 0) // Plan was executed correctly
                {
                    plan.Goal.OnPlanFinished(); // Only call this, if the plan was executed correctly
                }
                else
                {
                    plan.Goal.OnPlanFailed();
                }

                // Stop execution if there is no action to execute left or there was a request to stop the current plan execution
                this.PlanExecutionFinished();
            }
            else if (plan.Plan.Count > 0)
            {
                // Execute the next action
                _currentAction = plan.Plan.Dequeue();
                _currentAction.GetPrecalculations(_agent.CurrentState);
                GOAP_Logger.I.Log(_agent.ToString() + " | Performing " + _currentAction.ToString(), _agent, EMessageType.PlanExecution, EMessageSeverity.Info);

                // Check if this actions preconditions are still met
                if (_currentAction.IsPossible(_agent.CurrentState) &&
                    _agent.CurrentState.IsGoal(_currentAction.GetStaticPreconditions() + _currentAction.GetDynamicPreconditions(_agent.CurrentState)))
                {
                    _currentAction.Run(OnActionFinished, OnActionFailed, _agent.CurrentState);
                }
                else
                {
                    GOAP_Logger.I.Log(string.Format(_agent.ToString() + " | Plan execution cannot continue: Preconditions of {0} aren't fulfilled anymore", _currentAction.ToString()), _agent, EMessageType.PlanExecution, EMessageSeverity.Warning);

                    plan.Goal.Blacklist(false);
                    this.PlanExecutionFinished();
                }
            }
            else
            {
                GOAP_Logger.I.Log(_agent.ToString() + " | Unexpected error", EMessageType.PlanExecution, EMessageSeverity.Warning);
            }
        }

        /// <summary>
        /// Current method failed to execute. The whole plan can't be executed anymore. So tell agent to formulate a new plan.
        /// </summary>
        private void OnActionFailed()
        {
            IGOAP_Action action = _currentAction;
            GOAP_Plan plan = _currentPlan;
            if (_currentlyPlanning)
            {
                action = _lastAction;
                plan = _lastPlan;
            }

            if (action == null)
            {
                GOAP_Logger.I.Log(_agent.ToString() + " | Unexpected error: PlanRunner currentAction is null; Currently Planning = " + _currentlyPlanning + "; LastAction = " + _lastAction.ToString(), EMessageType.PlanExecution, EMessageSeverity.Error);
                return;
            }
            GOAP_Logger.I.Log(string.Format("{0} | Plan execution cannot continue: {1} failed", _agent.ToString(), action.ToString()), _agent, EMessageType.PlanExecution, EMessageSeverity.Warning);

            plan.Goal.OnPlanFailed();
            plan.Goal.Blacklist(false);

            _lastAction = action;
            _lastPlan = plan;
            _currentPlan = null;
            this.PlanExecutionFinished();
        }

        public GOAP_Plan Plan => _currentPlan == null ? _lastPlan : _currentPlan;

        public string CurrentActionName
        {
            get
            {
                string actionName = "null";
                if (_lastAction != null)
                    actionName = _lastAction.ToString();
                if (_currentAction != null)
                    actionName = _currentAction.ToString();
                return actionName;
            }
        }
    }
}
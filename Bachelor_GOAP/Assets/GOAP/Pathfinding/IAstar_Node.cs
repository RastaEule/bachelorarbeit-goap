﻿using System.Collections.Generic;

namespace GOAP.Pathfinding
{
    public interface IAstar_Node
    {
        IAstar_Node Parent { get; set; }

        List<IAstar_Node> GetNeighbours();

        /// <summary>
        /// Cost from starting node
        /// </summary>
        float gCost { get; set; }

        /// <summary>
        /// Cost from target node (heuristic)
        /// </summary>
        float hCost { get; set; }

        /// <summary>
        /// Cost overall
        /// </summary>
        float fCost { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using GOAP.Core;

namespace GOAP.Pathfinding
{
    public class GOAP_Node
    {
        /// <summary>
        /// The state that this node wants to be fulfilled
        /// </summary>
        private GOAP_State _goalState;

        /// <summary>
        /// The current worldstate after the actions effects are applied
        /// </summary>
        private GOAP_State _currentState;

        /// <summary>
        /// Symbols in <see cref="_currentState"/> that were changed by the actions precalculations
        /// </summary>
        private GOAP_State _precalculatedState;

        private IGOAP_Agent _agent;
        private IGOAP_Action _action;
        private float _actionCost = 0f;

        public GOAP_Node(IGOAP_Agent agent, IGOAP_Action action, GOAP_State goalState, GOAP_State currentState, GOAP_State precalculatedState, float actionCost)
        {
            _goalState = goalState;
            _currentState = currentState;
            _precalculatedState = precalculatedState;
            _agent = agent;
            _action = action;
            _actionCost = actionCost;
        }

        public GOAP_Node Parent { get; set; }


        public List<GOAP_Node> GetNeighbours()
        {
            List<GOAP_Node> neighbours = new List<GOAP_Node>();
            List<IGOAP_Action> actions = _agent.GetActions();

            // Check each action
            foreach(IGOAP_Action a in actions)
            {
                GOAP_State newCurrentState = _currentState.Copy();

                // Precalculations. Any changes made to the worldState are saved and will be applied to the player when planning is finished
                a.GetPrecalculations(newCurrentState);
                GOAP_State newPrecalculatedSymbols;
                _currentState.HasMismatchingSymbols(newCurrentState, out GOAP_State changedSymbols);
                newPrecalculatedSymbols = _precalculatedState + changedSymbols;

                // Effects & Preconditions
                GOAP_State actionEffects = a.GetStaticEffects() + a.GetDynamicEffects(newCurrentState.Copy());
                GOAP_State actionPreconditions = a.GetStaticPreconditions() + a.GetDynamicPreconditions(newCurrentState.Copy());

                // Check only relevant goals -> ones that aren't fulfilled already
                GOAP_State relevantGoalSymbols = _goalState;
                if (newCurrentState.HasMatchingSymbols(_goalState, out GOAP_State matchingWorldStateSymbols))
                    relevantGoalSymbols = _goalState - matchingWorldStateSymbols;


                // If this action satisfies any symbol in goalstate AND the proceduralPrecondition is fulfilled,
                // it is considered to be a neighbour
                if (a != _action &&
                    a.IsPossible(newCurrentState.Copy()) && // Is it possible at all
                    relevantGoalSymbols.HasMatchingSymbols(actionEffects)) // Does any goal of this node get fulfilled by action a
                {
                    float actionCost = a.GetCost(newCurrentState);
                    // New goalState for this neighbour: Add all preconditions -> they have yet to be satisfied
                    GOAP_State newGoalState = _goalState + actionPreconditions;
                    // New currentState for this nb: Apply all effects of this action -> this action is performed now
                    newCurrentState += actionEffects;

                    neighbours.Add(new GOAP_Node(_agent, a, newGoalState, newCurrentState, newPrecalculatedSymbols, actionCost));
                }
            }
            return neighbours;
        }

        public GOAP_State GoalState => _goalState;

        public GOAP_State CurrentState
        {
            get { return _currentState; }
            set { _currentState = value; }
        }

        public GOAP_State PrecalculatedSymbols => _precalculatedState;

        public float ActionCost => _actionCost;

        public IGOAP_Action Action => _action;

        /// <summary>
        /// Cost from starting node
        /// </summary>
        public float gCost { get; set; } = 0;

        /// <summary>
        /// Cost from target node (heuristic)
        /// </summary>
        public float hCost { get; set; } = 0;

        /// <summary>
        /// Cost overall
        /// </summary>
        public float fCost { get { return gCost + hCost; } }

        #region Helper functions

        public override string ToString()
        {
            string actionName = _action == null ? "GoalState" : _action.ToString();
            return "GOAP_Node " + actionName;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using GOAP.Core;
using GOAP.Logging;

namespace GOAP.Pathfinding
{
    public class AStar
    {
        private IGOAP_Agent _agent;
        private IGOAP_Logger _logger;

        public AStar(IGOAP_Agent agent, IGOAP_Logger logger)
        {
            _agent = agent;
            _logger = logger;
        }

        /// <summary>
        /// Searches a path through the state space
        /// </summary>
        /// <param name="start">Worldstate that is searched from</param>
        /// <param name="target">Worldstate that this method tries to reach</param>
        /// <returns>True, if a path was successfully found</returns>
        public bool FindPath(GOAP_State start, GOAP_State target, out Queue<IGOAP_Action> path)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            int iterationCount = 0;
            path = null;

            // Setup lists and initial node
            List<GOAP_Node> open = new List<GOAP_Node>();
            HashSet<GOAP_Node> closed = new HashSet<GOAP_Node>();
            GOAP_Node startNode = new GOAP_Node(_agent, null, start, target, new GOAP_State(), 0);
            startNode.hCost = Heuristic.CalculateHeuristicCost(startNode.CurrentState, start);
            open.Add(startNode);

            sw.Start();
            while (open.Count > 0)
            {
                if (iterationCount >= MaxIterations)
                    break;

                iterationCount++;

                // Get best currentNode
                GOAP_Node currentNode = open[0];
                for (int i = 1; i < open.Count; i++)
                    if (open[i].fCost < currentNode.fCost || open[i].fCost == currentNode.fCost && open[i].hCost < currentNode.hCost)
                        currentNode = open[i];

                open.Remove(currentNode);
                closed.Add(currentNode);

                this.ApplyPlayerStateToNode(currentNode, target);
                if (this.IsGoal(currentNode, target))
                {
                    // Success
                    // Apply precalculated symbols
                    _agent.CurrentState = _agent.CurrentState.Copy() + currentNode.PrecalculatedSymbols;

                    // Retrace path
                    sw.Stop();
                    _logger.Log(String.Format("{0} | A* finished. Iterations: {1}; Elapsed time: {2}", _agent.ToString(), iterationCount, sw.Elapsed), _agent, EMessageType.Planning, EMessageSeverity.Info);
                    path = this.RetracePath(currentNode);
                    return true;
                }

                foreach (GOAP_Node con in currentNode.GetNeighbours())
                {
                    if (!closed.Contains(con))
                    {
                        float gCostNew = con.gCost + con.ActionCost;
                        if (gCostNew < con.gCost || !open.Contains(con))
                        {
                            con.gCost = gCostNew;
                            con.hCost = Heuristic.CalculateHeuristicCost(con.CurrentState, con.GoalState);
                            con.Parent = currentNode;

                            if (!open.Contains(con))
                                open.Add(con);
                        }
                    }
                }
            }
            _logger.Log(String.Format("{0} | A* search failed. Iterations: {1}", _agent.ToString(), iterationCount), _agent, EMessageType.Planning, EMessageSeverity.Warning);
            return false;
        }

        /// <summary>
        /// Check if a GOAP_State is the goal
        /// </summary>
        /// <param name="stateToCheck">The state that is checked if it is the goal</param>
        /// <param name="goal">The goal to check for</param>
        /// <param name="playerState">The players world state. Maybe the a part of the goal is already fulfilled.</param>
        /// <returns></returns>
        private bool IsGoal(GOAP_Node node, GOAP_State playerState)
        {
            if (node.CurrentState.IsGoal(node.GoalState))
                return true;
            else
            {
                // Get all states in goal with a different value: states that arent fulfilled
                node.CurrentState.HasMismatchingSymbols(node.GoalState, out GOAP_State unfulfilledGoalSymbols);

                GOAP_State newplayerstate = playerState.Copy() - node.PrecalculatedSymbols;
                // Check if this value is already fulfilled by the players world state
                return newplayerstate.IsGoal(unfulfilledGoalSymbols);
            }
        }

        /// <summary>
        /// Checks if the playerstate fulfills any Symbols for the goalstate of this node and applies these
        /// </summary>
        private void ApplyPlayerStateToNode(GOAP_Node node, GOAP_State playerState)
        {
            GOAP_State playerStateCopy = playerState.Copy();
            playerStateCopy -= node.PrecalculatedSymbols;

            // Get all states in goal with a different value: states that arent fulfilled
            node.CurrentState.HasMismatchingSymbols(node.GoalState, out GOAP_State unfulfilledGoalSymbols);

            if (playerStateCopy.HasMatchingSymbols(unfulfilledGoalSymbols, out GOAP_State matchingSymbols))
                node.CurrentState += matchingSymbols;
        }

        private Queue<IGOAP_Action> RetracePath(GOAP_Node resultNode)
        {
            Queue<IGOAP_Action> path = new Queue<IGOAP_Action>();
            GOAP_Node cn = resultNode;

            while (cn != null)
            {
                IGOAP_Action a = cn.Action;
                if (a == null) // only goal state doesn't have an action
                    break;
                else
                {
                    path.Enqueue(cn.Action);
                    cn = cn.Parent;
                }
            }
            return path;
        }

        public int MaxIterations { set; get; } = 100;
    }
}
﻿using GOAP.Core;

namespace GOAP.Pathfinding
{
    public static class Heuristic
    {
        /// <summary>
        /// Amount of symbols, that arent fulfilled
        /// </summary>
        public static int CalculateHeuristicCost(GOAP_State currentState, GOAP_State goalState)
        {
            int hCost = goalState.State.Count;
            if (currentState.HasMatchingSymbols(goalState, out GOAP_State matchingSymbols))
                hCost -= matchingSymbols.State.Count;

            return hCost;
        }
    }
}
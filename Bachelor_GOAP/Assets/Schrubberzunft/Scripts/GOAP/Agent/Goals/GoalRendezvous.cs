﻿namespace Schrubber.GOAP
{
    public class GoalRendezvous : SchrubberGoal
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "Rendezvous";
            base._goalType = EGoalType.Clean;

            base._goalState.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, true);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DungeonStorageFilled, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, false);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DungeonStorageFilled, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, false);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DungeonStorageFilled, false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, false);
        }
    }
}
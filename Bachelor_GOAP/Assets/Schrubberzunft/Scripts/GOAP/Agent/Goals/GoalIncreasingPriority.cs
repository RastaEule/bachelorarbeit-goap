﻿using UnityEngine;
using GOAP.Logging;

namespace Schrubber.GOAP
{
    public class GoalIncreasingPriority : SchrubberGoal
    {
        [Header("Constraints")]
        [SerializeField]
        protected Vector2 _prioMinMaxValues = Vector2.up;

        [Header ("Updates")]
        [SerializeField]
        protected float _prioIncreasePerInterval = 0.05f;
        [SerializeField]
        private float _updateInterval = 1;
        [SerializeField]
        private bool _randomPrioAtStart = false;

        private bool _start = false;

        private float _lastUpdateTime = 0;

        private void Start()
        {
            if (_prioMinMaxValues.x > _prioMinMaxValues.y)
                GOAP_Logger.I.Log("Goal priority min is larger than max value.", EMessageType.Info, EMessageSeverity.Warning);
            else
            {
                if (_randomPrioAtStart)
                {
                    base._priority = LZRandom.I.Rand.Next((int)_prioMinMaxValues.x * 100, (int)((_prioMinMaxValues.y * 0.75f) * 100)) / 100.0f;
                    _prioIncreasePerInterval = (float)LZRandom.I.Rand.Next(1, 10) * 0.0001f;
                }

                _start = true;
            }
        }

        private void Update()
        {
            if (_start &&
                base._isPossible &&
                Time.time - _lastUpdateTime >= _updateInterval)
            {
                _lastUpdateTime = Time.time;

                if (base.IsBlackListed) // The goal loses priority if it is blacklisted
                    base._priority -= _prioIncreasePerInterval;
                else // otherwise the priority rises normally
                    base._priority += _prioIncreasePerInterval;

                base._priority = Mathf.Clamp(base._priority, _prioMinMaxValues.x, _prioMinMaxValues.y); // Stay within range
            }
        }

        public bool RandomPrioAtStart
        {
            get { return _randomPrioAtStart; }
            set { _randomPrioAtStart = value; }
        }

        public Vector2 PriorityMinMax => _prioMinMaxValues;
    }
}
﻿namespace Schrubber.GOAP
{
    public class GoalOpenTreasureChest : SchrubberGoal
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "Open Treasure Chest";
            base._goalType = EGoalType.Clean;

            base._goalState.SetSymbol(SchrubberSymbols.TreasureChestOpened, true);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.TreasureChestOpened, false);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.TreasureChestOpened, false);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.TreasureChestOpened, false);
        }
    }
}
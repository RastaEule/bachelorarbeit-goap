﻿using System.Collections;
using UnityEngine;
using GOAP.Unity;

namespace Schrubber.GOAP
{
    public class GoalHygiene : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "PersonalHygiene";
            base._goalType = EGoalType.Personal;

            base._goalState.SetSymbol(SchrubberSymbols.IsDirty, false);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            base._priority = 0f;
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsDirty, true);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsDirty, true);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsDirty, true);
        }
    }
}
﻿namespace Schrubber.GOAP
{
    public class GoalGetToolFishingRod : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "GetToolFishingRod";
            base._goalType = EGoalType.Base;

            base._goalState.SetSymbol(SchrubberSymbols.ItemStored(TargetTool), true);
            base._goalState.SetSymbol(SchrubberSymbols.ItemCrafted(TargetTool), true);
        }

        public override void OnPlanFinished()
        {
            this.ResetCurrentState();
            base._priority = 0f;
            base.OnPlanFinished();
        }

        public override void OnPlanFailed()
        {
            this.ResetCurrentState();
            base.OnPlanFailed();
        }

        public override void Blacklist(bool byPlanner)
        {
            this.ResetCurrentState();
            base.Blacklist(byPlanner);
        }

        /// <summary>
        /// Reset the worldState so the goal can be repeated
        /// </summary>
        private void ResetCurrentState()
        {
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemStored(TargetTool), false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemCrafted(TargetTool), false);
        }

        private string TargetTool => SchrubberSymbols.Tool_FishingRod;
    }
}
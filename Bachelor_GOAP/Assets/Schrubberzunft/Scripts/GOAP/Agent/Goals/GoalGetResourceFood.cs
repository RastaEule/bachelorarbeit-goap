﻿namespace Schrubber.GOAP
{
    public class GoalGetResourceFood : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "GetResourceFood";
            base._goalType = EGoalType.Base;

            base._goalState.SetSymbol(SchrubberSymbols.ItemStored(TargetItem), true);
            base._goalState.SetSymbol(SchrubberSymbols.ItemCrafted(TargetItem), true);
        }

        public override void OnPlanFinished()
        {
            this.ResetCurrentState();
            base._priority = 0f;
            base.OnPlanFinished();
        }

        public override void OnPlanFailed()
        {
            this.ResetCurrentState();
            base.OnPlanFailed();
        }

        public override void Blacklist(bool byPlanner)
        {
            this.ResetCurrentState();
            base.Blacklist(byPlanner);
        }

        /// <summary>
        /// Reset the worldState so the goal can be repeated
        /// </summary>
        private void ResetCurrentState()
        {
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemStored(TargetItem), false);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemCrafted(TargetItem), false);
        }

        private string TargetItem => SchrubberSymbols.Resource_Food;
    }
}
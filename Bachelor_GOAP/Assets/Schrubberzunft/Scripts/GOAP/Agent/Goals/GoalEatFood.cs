﻿namespace Schrubber.GOAP
{
    public class GoalEatFood : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "EatFood";
            base._goalType = EGoalType.Personal;

            base._goalState.SetSymbol(SchrubberSymbols.IsHungry, false);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            base._priority = 0f;
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsHungry, true);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsHungry, true);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsHungry, true);
        }
    }
}
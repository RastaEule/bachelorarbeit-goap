﻿namespace Schrubber.GOAP
{
    public class GoalAttackDirtMonster : SchrubberGoal
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "Attack Dirt Monster";
            base._goalType = EGoalType.Clean;

            base._goalState.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, true);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, false);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, false);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, false);
        }
    }
}
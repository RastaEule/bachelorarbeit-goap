﻿namespace Schrubber.GOAP
{
    public class GoalDrink : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "Drink";
            base._goalType = EGoalType.Personal;

            base._goalState.SetSymbol(SchrubberSymbols.IsThirsty, false);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            base._priority = 0f;
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsThirsty, true);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsThirsty, true);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.IsThirsty, true);
        }
    }
}
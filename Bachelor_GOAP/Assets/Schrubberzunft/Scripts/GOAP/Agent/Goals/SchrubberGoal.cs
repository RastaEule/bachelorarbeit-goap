﻿using System;
using UnityEngine;
using GOAP.Unity;
using Schrubber.Hierarchy;

namespace Schrubber.GOAP
{
    public class SchrubberGoal : GOAP_Goal
    {
        [SerializeField]
        protected EGoalType _goalType = EGoalType.Personal;

        private ITargetGoalListener _targetGoalListener;

        public void Init(Blackboard blackboard, ITargetGoalListener targetGoalListener)
        {
            Blackboard = blackboard;
            _targetGoalListener = targetGoalListener;
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            if (SquadTarget)
                _targetGoalListener.OnTargetGoalFinished(this);
        }

        public override void OnPlanFailed()
        {
            if (SquadTarget)
                _targetGoalListener.OnTargetGoalFailed(this);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);

            if (SquadTarget)
                _targetGoalListener.OnTargetGoalBlacklisted(this, byPlanner);
        }

        public bool SquadTarget { get; set; } = false;

        public Blackboard Blackboard { private set; get; }

        public EGoalType GoalType => _goalType;
    }
}
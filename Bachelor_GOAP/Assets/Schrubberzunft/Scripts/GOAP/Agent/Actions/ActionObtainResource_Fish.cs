﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionObtainResource_Fish : ActionObtainResource
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "ObtainResourceFish";
        }

        #endregion

        #region ActionObtainResource Members

        public override EItem TargetTool => EItem.Tool_FishingRod;

        public override EItem TargetResource => EItem.Resource_Fish;

        public override string TargetPosition_Name => SchrubberSymbols.PositionLake;

        #endregion
    }
}
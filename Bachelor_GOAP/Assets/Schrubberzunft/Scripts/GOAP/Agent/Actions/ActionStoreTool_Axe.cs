﻿using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreTool_Axe : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreToolAxe";
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, EItem.Tool_Axe);
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Tool_Axe;

        #endregion
    }
}
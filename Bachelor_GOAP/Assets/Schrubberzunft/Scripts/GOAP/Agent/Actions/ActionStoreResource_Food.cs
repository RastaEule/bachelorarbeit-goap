﻿using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreResource_Food : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreResourceFood";
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, EItem.Resource_Food);
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Resource_Food;

        #endregion
    }
}
﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public abstract class ActionObtainResource : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.2f;

        #region IGOAP_Action Members

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            base.GetPrecalculations(plannedState);
            if (TargetTool != EItem.Nothing)
            {
                plannedState.SetSymbol(SchrubberSymbols.TargetStorageItem, TargetTool);
                plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, TargetTool);
            }
        }

        public override GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            GOAP_State preconditions = base.GetDynamicPreconditions(plannedState);

            if (TargetTool != EItem.Nothing)
                preconditions.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(TargetTool)), true);

            return preconditions;
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);
            effects.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(TargetResource)), true);
            return effects;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            object targetobj;
            if (worldState.State.TryGetValue(TargetPosition_Name, out targetobj))
                targetPos = (Vector3)targetobj;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            if (TargetTool == EItem.Nothing)
            {
                // Add 1 resource when not using a tool
                base._blackboard.Inventory.AddItem(Utils.GetItemInstance(TargetResource));
                yield return new WaitForSeconds(_actionDuration);
                onFinished();
            }
            else
            {
                Item.Item item;
                if (base._blackboard.Inventory.PeekItem(TargetTool, out item))
                {
                    Tool tool = item as Tool;
                    if (tool.Use()) // If the tool is destroyed, remove it from inventory
                        base._blackboard.Inventory.TakeItem(TargetTool);

                    for (int i= 0; i < tool.ResourceObtainAmount; i++) // Add x resources when using a tool
                        base._blackboard.Inventory.AddItem(Utils.GetItemInstance(TargetResource));
                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
                else
                    onFailed();
            }
        }

        #region Abstract

        public abstract EItem TargetTool { get; }

        public abstract EItem TargetResource { get; }

        public abstract string TargetPosition_Name { get; }

        #endregion
    }
}
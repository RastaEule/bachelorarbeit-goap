﻿using System;
using System.Collections;
using UnityEngine;
using Schrubber.Game;
using GOAP.Core;

namespace Schrubber.GOAP
{
    public class ActionOpenTreasureChest : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 1.0f;

        private TreasureChest _targetChest = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Open Treasure Chest";

            base._preconditions.SetSymbol(SchrubberSymbols.TreasureChestOpened, false);
            base._effects.SetSymbol(SchrubberSymbols.TreasureChestOpened, true);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override bool IsPossible(GOAP_State plannedState)
        {
            _targetChest = _blackboard.TargetTreasureChest;
            return base.IsPossible(plannedState) && _targetChest != null && !_targetChest.IsOpen;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            _targetChest = _blackboard.TargetTreasureChest;
            Vector3 targetPos = Vector3.zero;
            if (_targetChest != null)
                targetPos = _targetChest.Pos;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            _targetChest = _blackboard.TargetTreasureChest;
            if (_targetChest == null || _targetChest.IsOpen)
            {
                onFailed();
                yield break;
            }
            yield return new WaitForSeconds(_actionDuration);
            _targetChest.Open(_blackboard);
            onFinished();
        }
    }
}
﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;
using Schrubber.Game.Dungeon;

namespace Schrubber.GOAP
{
    public class ActionRendezvous : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.75f;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Rendezvous";

            // Goto rendezvous position with all equipment needed for ActionCleanDirtSpot
            base._preconditions.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(EItem.Tool_Broom)), true);

            base._preconditions.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, false);
            base._effects.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, true);
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetStorageItem, EItem.Tool_Broom);
            plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, EItem.Tool_Broom);
        }

        public override GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            GOAP_State preconditions = base.GetDynamicPreconditions(plannedState);

            if (base._blackboard.IsDungeonStorageRefillAgent &&
                !DungeonStorage.I.IsFull())
                preconditions.SetSymbol(SchrubberSymbols.DungeonStorageFilled, true);

            return preconditions;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            Vector3 targetPos = Vector3.zero;
            object atposobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.AtPos, out atposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                if (atPos != null)
                    targetPos = base._blackboard.GetRendezvousPosition(atPos);
            }

            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(this.Wait(onFinished));
        }

        #endregion

        private IEnumerator Wait(Action onFinished)
        {
            yield return new WaitForSeconds(_actionDuration);
            onFinished();
        }
    }
}
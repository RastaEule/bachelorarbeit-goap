﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;

namespace Schrubber.GOAP
{
    public class ActionDrink : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 1.5f;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Drink";

            base._preconditions.SetSymbol(SchrubberSymbols.IsThirsty, true);
            base._effects.SetSymbol(SchrubberSymbols.IsThirsty, false);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.PositionFountain, out targetobj))
                targetPos = (Vector3)targetobj;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            yield return new WaitForSeconds(_actionDuration);
            onFinished();
        }
    }
}
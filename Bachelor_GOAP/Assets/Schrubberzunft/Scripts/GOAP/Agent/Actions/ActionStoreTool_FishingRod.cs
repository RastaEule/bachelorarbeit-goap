﻿using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreTool_FishingRod : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreToolFishingRod";
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, EItem.Tool_FishingRod);
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Tool_FishingRod;

        #endregion
    }
}
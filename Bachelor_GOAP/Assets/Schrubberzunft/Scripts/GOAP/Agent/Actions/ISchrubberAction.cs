﻿using GOAP.Core;

namespace Schrubber.GOAP
{
    public interface ISchrubberAction : IGOAP_Action
    {
        void Init(Blackboard blackboard);
    }
}
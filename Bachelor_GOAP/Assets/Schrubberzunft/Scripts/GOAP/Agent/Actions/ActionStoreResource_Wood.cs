﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreResource_Wood : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreResourceWood";
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Resource_Wood;

        #endregion
    }
}
﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreResource_Fish : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreResourceFish";
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Resource_Fish;

        #endregion
    }
}
﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Game;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public  class ActionAttackDirtMonster : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 1.0f;

        private DirtMonster _targetMonster = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Attack DirtMonster";

            base._preconditions.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(EItem.Tool_Broom)), true);
            base._preconditions.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, false);
            base._effects.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, true);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetStorageItem, EItem.Tool_Broom);
        }

        public override bool IsPossible(GOAP_State plannedState)
        {
            _targetMonster = _blackboard.TargetDirtMonster;
            return base.IsPossible(plannedState) && _targetMonster != null && !_targetMonster.IsDead;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            _targetMonster = _blackboard.TargetDirtMonster;
            Vector3 targetPos = Vector3.zero;
            if (_targetMonster != null)
                targetPos = _targetMonster.Pos;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            _targetMonster = _blackboard.TargetDirtMonster;
            if (_targetMonster == null)
            {
                onFailed();
                yield break;
            }
            if (_targetMonster.IsDead)
            {
                onFinished();
                yield break;
            }

            if (_targetMonster != null &&
                base._blackboard.Inventory.PeekItem(EItem.Tool_Broom, out Item.Item item))
            {
                Tool attackTool = item as Tool;
                _targetMonster = _blackboard.TargetDirtMonster;
                if (_targetMonster.IsDead)
                {
                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
                else
                {
                    _targetMonster.Attack(1);
                    base._blackboard.PlayAttackAnimation();
                    if (attackTool.Use())
                    {
                        base._blackboard.Inventory.TakeItem(Item.EItem.Tool_Broom);
                        worldState.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(EItem.Tool_Broom)), false);
                    }
                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
            }
            else
            {
                onFailed();
            }
        }
    }
}
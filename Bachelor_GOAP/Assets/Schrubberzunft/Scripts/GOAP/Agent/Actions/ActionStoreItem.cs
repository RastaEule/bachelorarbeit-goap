﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    /// <summary>
    /// This is an abstract action that stores any item to the nearest storage
    /// You need to implement ItemType, so this action knows which item to store and set preconditions
    /// The agent needs a StoragePositionSensor
    /// </summary>
    public abstract class ActionStoreItem : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Storage _targetStorage = null;

        #region IGOAP_Action Members

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            GOAP_State preconditions = base.GetDynamicPreconditions(plannedState);

            preconditions.SetSymbol(SchrubberSymbols.ItemStored(Utils.GetItemName(ItemType)), false);
            preconditions.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(ItemType)), true);

            return preconditions;
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);

            effects.SetSymbol(SchrubberSymbols.ItemStored(Utils.GetItemName(ItemType)), true);
            effects.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(ItemType)), false);

            return effects;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.NearestStorage, out targetobj))
                _targetStorage = (Storage)targetobj;
            return _targetStorage.Position;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            if (!base._blackboard.Inventory.ItemExists(ItemType))
                onFailed();

            Storage s = StoragePositions.I.GetNearestStorage(transform.position);
            if (s == null)
                onFailed();
            else
            {
                int count;
                if (base._blackboard.Inventory.TakeAllItems(ItemType, out count))
                {
                    for (int i = 0; i < count; i++) // Add as many items, as in inventory
                        s.Inventory.AddItem(Utils.GetItemInstance(ItemType));
                }
                yield return new WaitForSeconds(_actionDuration);
                onFinished();
            }
        }

        public abstract EItem ItemType { get; }
    }
}
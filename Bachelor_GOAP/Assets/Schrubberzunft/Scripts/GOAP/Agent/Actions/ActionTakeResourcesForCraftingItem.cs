﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionTakeResourcesForCraftingItem : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Storage _targetStorage = null;
        private EItem _targetItem = Item.EItem.Nothing;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "TakeResourcesForCraftingItem";

            base._effects.SetSymbol(SchrubberSymbols.EnoughResourcesForRecipeInInventory, true);
        }

        public override bool IsPossible(GOAP_State plannedState)
        {
            this.GetTargetPosition(plannedState);
            this.UpdateTargetItem(plannedState);

            bool hasenoughresources = false;
            if (_targetItem != EItem.Nothing)
            {
                List<KeyValuePair<EItem, int>> itemsToTake;
                hasenoughresources = _targetStorage.Inventory.HasEnoughResourcesCombined(base._blackboard.Inventory, RecipeDatabase.GetRecipe(_targetItem), out itemsToTake);
            }
            return hasenoughresources;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.NearestStorage, out targetobj))
                _targetStorage = ((Storage)targetobj);
            return _targetStorage.Position;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            this.UpdateTargetItem(worldState);
            List<KeyValuePair<EItem, int>> itemsToTake;
            if (_targetStorage.Inventory.HasEnoughResourcesCombined(base._blackboard.Inventory, RecipeDatabase.GetRecipe(_targetItem), out itemsToTake))
            {
                if (_targetItem == EItem.Resource_Food) // Take as many items as possible
                {
                    _targetStorage.Inventory.TakeAllItems(EItem.Resource_Fish, out int foodCount);
                    List<KeyValuePair<EItem, int>> itemsToAdd = new List<KeyValuePair<EItem, int>>();
                    itemsToAdd.Add(new KeyValuePair<EItem, int>(EItem.Resource_Fish, foodCount));
                    base._blackboard.Inventory.AddItems(itemsToAdd);

                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
                else // Take items for only one crafting
                {
                    _targetStorage.Inventory.TakeItems(itemsToTake);
                    base._blackboard.Inventory.AddItems(itemsToTake);

                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
            }
            else
                onFailed();
        }

        private void UpdateTargetItem(GOAP_State worldState)
        {
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.TargetCraftItem, out targetobj))
                _targetItem = (EItem)targetobj;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;
using Schrubber.Game.Dungeon;

namespace Schrubber.GOAP
{
    public class ActionTakeResourcesForDungeonStorage : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Storage _targetStorage = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Take Resources For Dungeon Storage";

            base._effects.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, true);
        }

        public override bool IsPossible(GOAP_State plannedState)
        {
            this.GetTargetPosition(plannedState);

            Recipe r = NeededItems;
            r.AddItem(new KeyValuePair<EItem, int>(EItem.Tool_Broom, 1));

            return _targetStorage.Inventory.HasEnoughResourcesCombined(base._blackboard.Inventory, r, out List<KeyValuePair<EItem, int>> itemsToTake);
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.NearestStorage, out targetobj))
                _targetStorage = ((Storage)targetobj);
            return _targetStorage.Position;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            List<KeyValuePair<EItem, int>> itemsToTake;
            if (_targetStorage.Inventory.HasEnoughResourcesCombined(base._blackboard.Inventory, NeededItems, out itemsToTake))
            {
                _targetStorage.Inventory.TakeItems(itemsToTake);
                base._blackboard.Inventory.AddItems(itemsToTake);

                yield return new WaitForSeconds(_actionDuration);
                onFinished();
            }
            else
                onFailed();
        }

        private Recipe NeededItems => DungeonStorage.I.GetCurrentlyNeededItems();
    }
}
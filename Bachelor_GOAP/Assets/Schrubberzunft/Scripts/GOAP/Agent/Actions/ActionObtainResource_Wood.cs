﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionObtainResource_Wood : ActionObtainResource
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "ObtainResourceWood";
        }

        #endregion

        #region ActionObtainResource Members

        public override EItem TargetTool => EItem.Tool_Axe;

        public override EItem TargetResource => EItem.Resource_Wood;

        public override string TargetPosition_Name => SchrubberSymbols.PositionForest;

        #endregion
    }
}
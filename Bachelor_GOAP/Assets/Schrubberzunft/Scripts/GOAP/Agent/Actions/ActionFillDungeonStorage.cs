﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;
using Schrubber.Game.Dungeon;
using System.Collections.Generic;

namespace Schrubber.GOAP
{
    public class ActionFillDungeonStorage : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.75f;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "Fill Dungeon Storage";

            base._effects.SetSymbol(SchrubberSymbols.DungeonStorageFilled, true);
        }

        public override GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            // Check if agent needs to take additional items from storage
            GOAP_State preconditions = base.GetDynamicPreconditions(plannedState);
            Recipe neededItems = DungeonStorage.I.GetCurrentlyNeededItems(); // Get items needed to fill the store
            neededItems.AddItem(new KeyValuePair<EItem, int>(EItem.Tool_Broom, 1)); // Agent also needs a broom for himself

            if (!_blackboard.Inventory.HasEnoughResources(neededItems)) // Check if agent already has these items in inventory
                preconditions.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, true); // If not, he has to take additional items from storage

            return preconditions;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            return DungeonStorage.I.Pos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            // Agent now has all needed items in his inventory and is at DungeonStoragePosition
            StartCoroutine(this.Wait(onFinished, onFailed));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            Recipe itemsToStore = DungeonStorage.I.GetCurrentlyNeededItems(); // Items needed to fill up the DungeonStorage

            // Take needed items from inventory and store them in DungeonStorage
            base._blackboard.Inventory.TakeItemsFromRecipe(itemsToStore);
            DungeonStorage.I.Inventory.AddItems(itemsToStore);

            yield return new WaitForSeconds(_actionDuration);
            onFinished();
        }
    }
}
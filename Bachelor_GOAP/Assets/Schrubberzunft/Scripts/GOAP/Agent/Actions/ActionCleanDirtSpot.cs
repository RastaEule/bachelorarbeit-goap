﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Game;
using Schrubber.Game.Dungeon;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionCleanDirtSpot : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 1.0f;

        private DungeonRoom _targetRoom = null;
        private DirtSpot _targetSpot = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "CleanDirtSpot";

            base._preconditions.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(EItem.Tool_Broom)), true);
            base._preconditions.SetSymbol(SchrubberSymbols.DirtSpotCleaned, false);
            base._effects.SetSymbol(SchrubberSymbols.DirtSpotCleaned, true);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetStorageItem, EItem.Tool_Broom);
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            this.UpdateTargetRoom();
            _targetSpot = _targetRoom?.GetNextDirtSpot();

            if (_targetRoom == null || _targetSpot == null)
                onFailed();
            else
            {
                _targetSpot.ClaimedBy = this;
                base.Run(onFinished, onFailed, worldState);
            }
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            Vector3 targetPos = Vector3.zero;
            if (_targetSpot != null)
                targetPos = _targetSpot.Pos;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private void UpdateTargetRoom()
        {
            _blackboard.UpdateTargetRoom();
            _targetRoom = _blackboard.TargetRoom;
        }

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            Item.Item item;
            Tool toolToUse = null;
            bool failed = false;
            if (base._blackboard.Inventory.PeekItem(Item.EItem.Tool_Broom, out item))
            {
                toolToUse = item as Tool;
                if (toolToUse == null)
                    failed = true;
            }
            else
                failed = true;
            if (failed)
            {
                if (_targetSpot != null && _targetSpot.ClaimedBy == this)
                    _targetSpot.ClaimedBy = null;

                onFailed();
                yield break;
            }
            _targetSpot.Clean();
            if (toolToUse.Use()) // If the tool is destroyed, remove it from inventory
            {
                base._blackboard.Inventory.TakeItem(Item.EItem.Tool_Broom);
                worldState.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(EItem.Tool_Broom)), false);
            }
            Game.GameManager.I.Player.AddMoney(10);
            yield return new WaitForSeconds(_actionDuration);
            onFinished();
        }
    }
}
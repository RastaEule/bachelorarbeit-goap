﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionEatFood : GOAP_Action, ISchrubberAction
    {
        [SerializeField]
        private float _actionDuration = 0.75f;

        /// <summary>
        /// This blackboard needs to be obtained from the agent
        /// </summary>
        protected Blackboard _blackboard = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "EatFood";

            base._preconditions.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Food), true);
            base._preconditions.SetSymbol(SchrubberSymbols.IsHungry, true);
            base._effects.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Food), false);
            base._effects.SetSymbol(SchrubberSymbols.IsHungry, false);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol(SchrubberSymbols.TargetStorageItem, EItem.Resource_Food);
            plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, EItem.Resource_Food);
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed));
        }

        #endregion

        #region ISchrubberAction Members

        public void Init(Blackboard blackboard)
        {
            _blackboard = blackboard;
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed)
        {
            if (_blackboard.Inventory.ItemExists(EItem.Resource_Food))
            {
                _blackboard.Inventory.TakeItem(EItem.Resource_Food);
                _blackboard.Heal(1);
                yield return new WaitForSeconds(_actionDuration);
                onFinished();
            }
            else
                onFailed();
        }
    }
}
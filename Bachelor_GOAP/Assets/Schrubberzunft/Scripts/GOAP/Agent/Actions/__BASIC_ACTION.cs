﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;

namespace Schrubber.GOAP
{
    public class BASIC_ACTION : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 1.5f;

        private Action _onFinished = null;
        private Action _onFailed = null;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "__BASIC_ACTION";

            base._effects.SetSymbol(SchrubberSymbols.IsHungry, false);
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.PositionForest, out targetobj))
                targetPos = (Vector3)targetobj;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            _onFinished = onFinished;
            _onFailed = onFailed;

            StartCoroutine(Wait());
        }

        #endregion

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(_actionDuration);
            _onFinished();
        }
    }
}
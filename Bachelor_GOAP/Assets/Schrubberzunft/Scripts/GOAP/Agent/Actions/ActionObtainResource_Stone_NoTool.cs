﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionObtainResource_Stone_NoTool : ActionObtainResource
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "ObtainResourceStone_NoTool";
        }

        #endregion

        #region ActionObtainResource Members

        public override EItem TargetTool => EItem.Nothing;

        public override EItem TargetResource => EItem.Resource_Stone;

        public override string TargetPosition_Name => SchrubberSymbols.PositionQuarry;

        #endregion
    }
}
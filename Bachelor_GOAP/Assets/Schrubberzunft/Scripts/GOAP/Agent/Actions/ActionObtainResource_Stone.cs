﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionObtainResource_Stone : ActionObtainResource
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "ObtainResourceStone";
        }

        #endregion

        #region ActionObtainResource Members

        public override EItem TargetTool => EItem.Tool_Pickaxe;

        public override EItem TargetResource => EItem.Resource_Stone;

        public override string TargetPosition_Name => SchrubberSymbols.PositionQuarry;

        #endregion
    }
}
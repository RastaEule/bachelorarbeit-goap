﻿using GOAP.Core;
using GOAP.Unity;
using System;
using UnityEngine;

namespace Schrubber.GOAP
{
    public abstract class BaseAction_Move : GOAP_Action, ISchrubberAction
    {
        /// <summary>
        /// This blackboard needs to be obtained from the agent
        /// </summary>
        protected Blackboard _blackboard = null;

        private Vector3 _targetPos = Vector3.zero;
        GOAP_State _worldState = null;
        private Action _actionFinishedCallback = null;
        private Action _actionFailedCallback = null;

        #region ISchrubberAction Members

        public void Init(Blackboard blackboard)
        {
            _blackboard = blackboard;
        }

        #endregion

        #region IGOAP_Action Members

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            _targetPos = this.GetTargetPosition(plannedState);
            plannedState.SetSymbol(SchrubberSymbols.TargetPos, _targetPos);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            if (plannedState.State.TryGetValue(SchrubberSymbols.AtPos, out object atposobj) && plannedState.State.TryGetValue(SchrubberSymbols.TargetPos, out object targetposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                Vector3 targetPos = (Vector3)targetposobj;
                if (atPos != null && targetposobj != null)
                    return base.GetCost(plannedState) + Vector3.Distance(atPos, targetPos);
            }
            return base.GetCost(plannedState);
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);

            effects.SetSymbol(SchrubberSymbols.AtPos, _targetPos);

            return effects;
        }

        public override void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            _actionFinishedCallback = onFinished;
            _actionFailedCallback = onFailed;
            _worldState = worldState;

            _targetPos = this.GetTargetPosition(worldState);
            _blackboard.GotoPos(_targetPos, MovementFinished, ActionFailed);
        }

        #endregion

        private void MovementFinished()
        {
            this.Execute(ActionFinished, ActionFailed, _worldState);
        }

        private void ActionFailed()
        {
            _actionFailedCallback();
        }

        private void ActionFinished()
        {
            _actionFinishedCallback();
        }

        protected abstract Vector3 GetTargetPosition(GOAP_State worldState);

        protected abstract void Execute(Action onFinished, Action onFailed, GOAP_State worldState);
    }
}
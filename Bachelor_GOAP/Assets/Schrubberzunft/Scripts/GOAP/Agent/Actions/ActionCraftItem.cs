﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionCraftItem : BaseAction_Move
    {
        [SerializeField]
        protected float _actionDuration = 1f;

        private EItem _targetItem = EItem.Nothing;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "CraftItem";

            base._preconditions.SetSymbol(SchrubberSymbols.EnoughResourcesForRecipeInInventory, true);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            base.GetPrecalculations(plannedState);
            this.UpdateTargetItem(plannedState);

            bool hasenoughresources = false;
            if (_targetItem != EItem.Nothing)
            {
                plannedState.SetSymbol(SchrubberSymbols.TargetCraftItem, _targetItem);

                Recipe r = RecipeDatabase.GetRecipe(_targetItem);
                hasenoughresources = base._blackboard.Inventory.HasEnoughResources(r);
            }
            plannedState.SetSymbol(SchrubberSymbols.EnoughResourcesForRecipeInInventory, hasenoughresources);
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);
            this.UpdateTargetItem(plannedState);

            if (_targetItem != EItem.Nothing)
            {
                effects.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(_targetItem)), true);
                effects.SetSymbol(SchrubberSymbols.ItemCrafted(Utils.GetItemName(_targetItem)), true);
            }

            return effects;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            if (worldState.State.TryGetValue(SchrubberSymbols.PositionWorkshop, out object targetobj))
                targetPos = (Vector3)targetobj;

            // Alternative position for food
            if (_targetItem == EItem.Resource_Food &&
                (worldState.State.TryGetValue(SchrubberSymbols.PositionKitchen, out targetobj)))
                    targetPos = (Vector3)targetobj;

            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            this.UpdateTargetItem(worldState);
            if (_targetItem == EItem.Nothing)
                onFailed();

            Recipe r = RecipeDatabase.GetRecipe(_targetItem);
            if (base._blackboard.Inventory.HasEnoughResources(r))
            {
                if (_targetItem == EItem.Resource_Food) // Craft as many items as possible
                {
                    while(base._blackboard.Inventory.HasEnoughResources(r))
                    {
                        base._blackboard.Inventory.TakeItemsFromRecipe(r);
                        base._blackboard.Inventory.AddItem(Utils.GetItemInstance(_targetItem));
                    }
                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
                else // Craft only one item
                {
                    base._blackboard.Inventory.TakeItemsFromRecipe(r);
                    base._blackboard.Inventory.AddItem(Utils.GetItemInstance(_targetItem));
                    yield return new WaitForSeconds(_actionDuration);
                    onFinished();
                }
            }
            else
                onFailed();
        }

        private void UpdateTargetItem(GOAP_State worldState)
        {
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.TargetCraftItem, out targetobj))
                _targetItem = (EItem)targetobj;
        }
    }
}
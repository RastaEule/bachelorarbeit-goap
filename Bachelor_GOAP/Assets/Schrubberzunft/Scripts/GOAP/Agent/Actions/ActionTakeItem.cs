﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;
using Schrubber.Game.Dungeon;

namespace Schrubber.GOAP
{
    public class ActionTakeItem : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Inventory _targetInventory = null;
        private EItem _targetItem = EItem.Nothing;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "TakeItem";
        }

        public override bool IsPossible(GOAP_State plannedState)
        {
            this.UpdateTargetItem(plannedState);
            this.GetTargetPosition(plannedState);
            return _targetInventory.ItemExists(_targetItem);
        }


        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetDynamicEffects(plannedState);
            this.UpdateTargetItem(plannedState);

            if (_targetItem != EItem.Nothing)
                effects.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(_targetItem)), true);
            return effects;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            if (base._blackboard.CanRefillAtDungeonStorage &&
                base._blackboard.CurrentSquadType == EGoalType.Clean)
            {
                _targetInventory = DungeonStorage.I.Inventory;
                targetPos = DungeonStorage.I.Pos;
            }
            else if (worldState.State.TryGetValue(SchrubberSymbols.NearestStorage, out object targetobj))
            {
                Storage storage = (Storage)targetobj;
                _targetInventory = storage.Inventory;
                targetPos = storage.Position;
            }
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            this.UpdateTargetItem(worldState);
            this.GetTargetPosition(worldState);
            Item.Item targetItem;
            if (_targetInventory.TakeItem(_targetItem, out targetItem))
            {
                base._blackboard.Inventory.AddItem(targetItem);
                yield return new WaitForSeconds(_actionDuration);
                onFinished();
            }
            else
                onFailed();
        }

        private void UpdateTargetItem(GOAP_State worldState)
        {
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.TargetStorageItem, out targetobj))
                _targetItem = (EItem)targetobj;
        }
    }
}
﻿using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionStoreResource_Stone : ActionStoreItem
    {
        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "StoreResourceStone";
        }

        #endregion

        #region ActionStoreResource Members

        public override EItem ItemType => EItem.Resource_Stone;

        #endregion
    }
}
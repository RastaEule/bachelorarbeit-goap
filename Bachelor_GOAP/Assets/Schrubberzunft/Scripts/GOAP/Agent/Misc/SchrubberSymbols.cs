﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Schrubber.GOAP
{
    public static class SchrubberSymbols
    {
        public static string AtPos => "atPos";
        public static string TargetPos => "targetPos";
        public static string TargetStorageItem => "targetStorageItem";
        public static string TargetCraftItem => "targetCraftItem";

        public static string PositionBath => "posBath";
        public static string PositionForest => "posForest";
        public static string PositionFountain => "posFountain";
        public static string PositionKitchen => "posKitchen";
        public static string PositionLake => "posLake";
        public static string PositionQuarry => "posQuarry";
        public static string PositionWorkshop => "posWorkshop";
        public static string NearestStorage => "nearestStorage";

        public static string HasItem(string itemName)
        {
            return "hasItem" + itemName;
        }

        public static string ItemStored(string itemName)
        {
            return "itemStored" + itemName;
        }

        public static string ItemCrafted(string itemName)
        {
            return "itemCrafted" + itemName;
        }

        public static string EnoughResourcesForRecipeInInventory => "enoughResourcesForRecipeInInventory";
        public static string EnoughToolsInStorage => "enoughToolsInStorage";

        public static string Tool_Axe => "Axe";
        public static string Tool_Broom => "Broom";
        public static string Tool_FishingRod => "FishingRod";
        public static string Tool_Pickaxe => "Pickaxe";

        public static string Resource_Fish => "Fish";
        public static string Resource_Food => "Food";
        public static string Resource_Stone => "Stone";
        public static string Resource_Wood => "Wood";

        public static string IsDirty => "isDirty";
        public static string IsHungry => "isHungry";
        public static string IsThirsty => "isThirsty";

        // ----- ------- -----
        // ----- Dungeon -----
        // ----- ------- -----

        public static string DirtMonsterAttacked => "DirtMonsterAttacked";

        public static string DirtSpotCleaned => "DirtSpotCleaned";

        public static string DungeonStorageFilled => "DungeonStorageFilled";

        public static string EnoughResourcesForDungeonStorageInInventory => "EnoughResourcesForDungeonStorageInInventory";

        public static string IsAtRendezvousPoint => "IsAtRendezvouzPoint";

        public static string TreasureChestOpened => "TreasureChestOpened";
    }
}
﻿using System;

namespace Schrubber
{
    public sealed class LZRandom
    {
        private static readonly Lazy<LZRandom> _lazy = new Lazy<LZRandom>(() => new LZRandom());

        public static LZRandom I => _lazy.Value;

        private System.Random _random = null;

        private LZRandom()
        {
            _random = new System.Random(System.Guid.NewGuid().GetHashCode());
        }

        public System.Random Rand => _random;
    }
}
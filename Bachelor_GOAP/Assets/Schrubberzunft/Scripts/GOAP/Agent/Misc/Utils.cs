﻿using GOAP.Core;
using GOAP.Logging;
using Schrubber.GOAP;
using Schrubber.Item;

namespace Schrubber
{
    public static class Utils
    {
        public static string GetItemName(EItem itemType)
        {
            switch (itemType)
            {
                case EItem.Resource_Fish:
                    return SchrubberSymbols.Resource_Fish;
                case EItem.Resource_Food:
                    return SchrubberSymbols.Resource_Food;
                case EItem.Resource_Stone:
                    return SchrubberSymbols.Resource_Stone;
                case EItem.Resource_Wood:
                    return SchrubberSymbols.Resource_Wood;
                case EItem.Tool_Axe:
                    return SchrubberSymbols.Tool_Axe;
                case EItem.Tool_Broom:
                    return SchrubberSymbols.Tool_Broom;
                case EItem.Tool_FishingRod:
                    return SchrubberSymbols.Tool_FishingRod;
                case EItem.Tool_Pickaxe:
                    return SchrubberSymbols.Tool_Pickaxe;
                default:
                    {
                        GOAP_Logger.I.Log("Unknown Item: " + itemType.ToString(), EMessageType.Info, EMessageSeverity.Error);
                        return null;
                    }
            }
        }

        public static Item.Item GetItemInstance(EItem itemtype)
        {
            switch (itemtype)
            {
                case EItem.Resource_Fish:
                    return new Resource_Fish();
                case EItem.Resource_Food:
                    return new Resource_Food();
                case EItem.Resource_Stone:
                    return new Resource_Stone();
                case EItem.Resource_Wood:
                    return new Resource_Wood();
                case EItem.Tool_Axe:
                    return new Tool_Axe();
                case EItem.Tool_Broom:
                    return new Tool_Broom();
                case EItem.Tool_FishingRod:
                    return new Tool_FishingRod();
                case EItem.Tool_Pickaxe:
                    return new Tool_Pickaxe();
                default:
                    {
                        GOAP_Logger.I.Log("Unknown Item: " + itemtype.ToString(), EMessageType.Info, EMessageSeverity.Error);
                        return null;
                    }
            }
        }

        /// <summary>
        /// A GOAP_State containing all craftable items as  HasItem_Item false
        /// </summary>
        public static GOAP_State HasItemCraftable
        {
            get
            {
                GOAP_State s = new GOAP_State();
                s.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Food), false);
                s.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Axe), false);
                s.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Broom), false);
                s.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_FishingRod), false);
                s.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Pickaxe), false);
                return s;
            }
        }
    }
}
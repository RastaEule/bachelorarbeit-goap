﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using GOAP.Unity;
using Schrubber.Hierarchy;
using Schrubber.Game.Dungeon;

namespace Schrubber.GOAP
{
    public class Schrubber_Agent : GOAP_Agent, ISquadMember, ITargetGoalListener
    {
        private float _squadCleanPriority = 0.8f;

        private Blackboard _blackboard = null;

        private List<SchrubberGoal> _schrubberGoals;
        private ISquad _squad;
        private Order _currentOrder;

        public void Init(bool allGoalsRandomPriority)
        {
            _blackboard = transform.parent.GetComponentInChildren<Blackboard>();
            _blackboard.Init(this);

            base.Awake();

            foreach (IGOAP_Action a in base.GetActions())
            {
                ISchrubberAction sa = (ISchrubberAction)a;
                if (sa != null)
                    sa.Init(_blackboard);
            }
            foreach (IGOAP_Goal g in base.GetGoals())
            {
                SchrubberGoal sg = (SchrubberGoal)g;
                if (sg != null)
                {
                    sg.Init(_blackboard, this);

                    if (sg is GoalIncreasingPriority)
                    {
                        GoalIncreasingPriority gi = sg as GoalIncreasingPriority;
                        if (gi != null && allGoalsRandomPriority)
                            gi.RandomPrioAtStart = true;
                    }
                }
            }
            // Init schrubber goals
            _schrubberGoals = new List<SchrubberGoal>(GetComponents<SchrubberGoal>());
        }

        #region GOAP_Agent Members

        protected override void GoalChanged(IGOAP_Goal g)
        {
            SchrubberGoal sg = g as SchrubberGoal;
            if (sg != null &&
                _squad != null &&
                _squad.SquadType == EGoalType.Clean &&
                sg.GoalType == EGoalType.Personal &&
                !(g.GetType() == typeof(GoalEatFood) && _blackboard.CanRefillAtDungeonStorage && DungeonStorage.I.Inventory.ItemExists(Item.EItem.Resource_Food)))
            {
                _squad.SquadMemberLeft(this);
            }
        }

        #endregion

        #region Squad Members

        public void SetSquad(ISquad squad)
        {
            _squad = squad;
            this.ResetTargetGoal(); // Reset old target goal
            _currentOrder = null;
            // Move transform
            transform.parent.SetParent(squad.Transform);

            // Set goals
            switch (squad.SquadType)
            {
                case EGoalType.Base:
                    {
                        foreach (SchrubberGoal g in _schrubberGoals)
                        {
                            if (g.GoalType == EGoalType.Base)
                                g.IsPossible = true;
                            if (g.GoalType == EGoalType.Clean)
                                g.IsPossible = false;
                        }
                        break;
                    }
                case EGoalType.Clean:
                    {
                        foreach (SchrubberGoal g in _schrubberGoals)
                        {
                            if (g.GoalType == EGoalType.Base)
                                g.IsPossible = false;
                            if (g.GoalType == EGoalType.Clean)
                                g.IsPossible = true;
                        }
                        break;
                    }
            }
        }

        public void SetOrder(Order order)
        {
            _currentOrder = order;

            // Select a target goal, which priority should be increased
            this.ResetTargetGoal(); // Reset old target goal
            Type targetGoal = GetOrderTargetGoal(order.Type); // Get a new target goal based on the order
            _blackboard.TargetDirtMonster = order.TargetDirtMonster;
            _blackboard.TargetRoom = order.TargetRoom;
            _blackboard.TargetTreasureChest = order.TargetTreasureChest;

            // Set Squad Goal onFinished, onFailed Actions
            this.ResetCleanSquadGoalPriorities();
            foreach (IGOAP_Goal g in base.GetGoals())
            {
                SchrubberGoal sg = (SchrubberGoal)g;
                if (sg != null && sg.GetType() == targetGoal)
                {
                    sg.Priority = _squadCleanPriority;
                    sg.SquadTarget = true;
                }
            }
        }

        public float GetPersonalUtility()
        {
            float prio = 0;
            int goalcount = 0;
            foreach (SchrubberGoal g in _schrubberGoals)
                if (g.GoalType == EGoalType.Personal)
                {
                    prio += g.Priority;
                    goalcount++;
                }
            return prio / goalcount;
        }

        public ISquad CurrentSquad => _squad;

        public Blackboard Blackboard => _blackboard;

        #endregion

        #region ITargetGoalListener Members

        public void OnTargetGoalFinished(IGOAP_Goal goal)
        {
            if (_currentOrder == null ||
                !GoalOrderSiblings(goal.GetType(), _currentOrder.Type))
                return;

            // Check if this is the current targetgoal
            switch (_currentOrder.Type)
            {
                case EOrderType.AttackEnemy:
                    if (_currentOrder.TargetDirtMonster.IsDead)
                    {
                        this.ResetTargetGoal();
                        this.ResetCleanSquadGoalPriorities();
                        _currentOrder.OnFinished(this, _currentOrder.Type);
                    }
                    break;
                case EOrderType.CleanRoom:
                    {
                        if (_currentOrder.TargetRoom == null ||
                            _currentOrder.TargetRoom.IsClean)
                        {
                            this.ResetTargetGoal();
                            this.ResetCleanSquadGoalPriorities();
                            _currentOrder.OnFinished(this, _currentOrder.Type);
                        }
                        break;
                    }
                case EOrderType.OpenTreasureChest:
                case EOrderType.Rendezvous:
                    {
                        _currentOrder.OnFinished(this, _currentOrder.Type);
                        break;
                    }
                default:
                    this.ResetTargetGoal();
                    this.ResetCleanSquadGoalPriorities();
                    _currentOrder.OnFinished(this, _currentOrder.Type);
                    break;
            }
        }

        public void OnTargetGoalFailed(IGOAP_Goal goal)
        {
            this.ResetTargetGoal();
            this.ResetCleanSquadGoalPriorities();
            if (_currentOrder == null || !GoalOrderSiblings(goal.GetType(), _currentOrder.Type)) // Something is wrong
                _squad.SquadMemberLeft(this); // Leave squad
            else if (_currentOrder != null) // Handle failed order correctly
            {
                _currentOrder.OnFailed(this, _currentOrder.Type);
                _currentOrder = null;
            }
        }

        public void OnTargetGoalBlacklisted(IGOAP_Goal goal, bool byPlanner)
        {
            this.ResetTargetGoal();
            this.ResetCleanSquadGoalPriorities();
            if (byPlanner)
            {
                if (goal.GetType() == typeof(GoalAttackDirtMonster))
                    base.ReplanAfterGoalsBlacklisted = true;

                this.OnTargetGoalFailed(goal);
            }
            else if (_currentOrder == null && _squad.SquadType == EGoalType.Clean) // Something went wrong
                _squad.SquadMemberLeft(this); // Leave squad

#warning OnTargetGoalBlacklisted todo
            //if (byPlanner &&
            //    goal.GetType() == typeof(GoalAttackDirtMonster)) // There is no valid path for this goal
            //{
            //    this.OnTargetGoalFailed(goal); // Handle as if the order failed to execute -> This agent will get a new order
            //    base.ReplanAfterGoalsBlacklisted = true; // Plan again for this order
            //}
            //else if (_currentOrder == null && _squad.SquadType == EGoalType.Clean) // Something went wrong
            //    _squad.SquadMemberLeft(this); // Leave squad
        }

        #endregion

        private void ResetTargetGoal()
        {
            foreach (IGOAP_Goal g in base.GetGoals())
            {
                SchrubberGoal sg = (SchrubberGoal)g;
                if (sg != null)
                    sg.SquadTarget = false;
            }
        }

        /// <summary>
        /// Set priorities of each CleanSquadGoal to 0
        /// </summary>
        private void ResetCleanSquadGoalPriorities()
        {
            foreach (IGOAP_Goal g in base.GetGoals())
            {
                SchrubberGoal sg = (SchrubberGoal)g;
                if (sg != null && sg.GoalType == EGoalType.Clean)
                    sg.Priority = 0f;
            }
        }

        /// <summary>
        /// Check which goal correspondents to an order type
        /// </summary>
        private static Type GetOrderTargetGoal(EOrderType type)
        {
            switch (type)
            {
                case EOrderType.AttackEnemy:
                    return typeof(GoalAttackDirtMonster);
                case EOrderType.CleanRoom:
                    return typeof(GoalCleanSpot);
                case EOrderType.OpenTreasureChest:
                    return typeof(GoalOpenTreasureChest);
                case EOrderType.Rendezvous:
                    return typeof(GoalRendezvous);
            }
            return null;
        }

        /// <summary>
        /// Check which order correspondetns to a goal
        /// </summary>
        private static EOrderType GetGoalOrder(Type goal)
        {
            if (goal == typeof(GoalAttackDirtMonster))
                return EOrderType.AttackEnemy;
            if (goal == typeof (GoalCleanSpot))
                return EOrderType.CleanRoom;
            if (goal == typeof(GoalOpenTreasureChest))
                return EOrderType.OpenTreasureChest;
            if (goal == typeof(GoalRendezvous))
                return EOrderType.Rendezvous;
            return EOrderType.None;
        }

        /// <summary>
        /// Does a goal and an orderType fit together
        /// </summary>
        private static bool GoalOrderSiblings(Type goal, EOrderType order)
        {
            EOrderType orderOfGoal = GetGoalOrder(goal);
            return orderOfGoal == order;
        }

        public int ID { get; set; } = -1;

        public override string ToString()
        {
            return base.ToString() + " " + ID;
        }
    }
}
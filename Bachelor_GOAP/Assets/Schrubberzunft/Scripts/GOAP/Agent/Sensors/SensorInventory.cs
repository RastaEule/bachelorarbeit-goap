﻿using GOAP.Unity;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class SensorInventory : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Schrubber_Memory mem = memory as Schrubber_Memory;

            Item.Item oi;
            bool hasItem = false;

            // Tools
            hasItem = mem.Blackboard.Inventory.PeekItem(EItem.Tool_Axe, out oi);
            mem.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Axe), hasItem);

            hasItem = mem.Blackboard.Inventory.PeekItem(EItem.Tool_Broom, out oi);
            mem.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Broom), hasItem);

            hasItem = mem.Blackboard.Inventory.PeekItem(EItem.Tool_FishingRod, out oi);
            mem.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_FishingRod), hasItem);

            hasItem = mem.Blackboard.Inventory.PeekItem(EItem.Tool_Pickaxe, out oi);
            mem.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Pickaxe), hasItem);
        }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;
using Schrubber.Game;

namespace Schrubber.GOAP
{
    public class SensorTreasureChest : GOAP_Sensor
    {
        [SerializeField]
        private float _searchRadius = 5f;

        public override void UpdateSensor(GOAP_Memory memory)
        {
            Schrubber_Memory mem = memory as Schrubber_Memory;

            // Search for treasure chest in radius
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _searchRadius);
            List<TreasureChest> nearbyChests = new List<TreasureChest>();
            foreach (Collider col in hitColliders)
            {
                TreasureChest chest = col.GetComponent<TreasureChest>();
                if (chest != null)
                    nearbyChests.Add(chest);
            }
            TreasureChest nearestChest = nearbyChests.Count > 0 ? nearbyChests[0] : null;
            if (nearbyChests.Count > 1)
            {
                for(int i = 1; i< nearbyChests.Count; i++)
                {
                    if (Vector3.Distance(transform.position, nearbyChests[i].Pos) < Vector3.Distance(transform.position, nearestChest.Pos))
                        nearestChest = nearbyChests[i];
                }
            }

            if (nearestChest != null)
            {
                mem.Blackboard.TreasureChestSpotted(nearestChest);
            }
        }
    }
}
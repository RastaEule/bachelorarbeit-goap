﻿using GOAP.Unity;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class SensorStorage : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Storage targetStorage = StoragePositions.I.GetNearestStorage(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.NearestStorage, targetStorage);
        }
    }
}
﻿using UnityEngine;
using GOAP.Unity;

namespace Schrubber.GOAP
{
    public class SensorPositions : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Vector3 targetPos = Vector3.zero;

            // Bath Position
            targetPos = BathPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionBath, targetPos);

            // Forest Position
            targetPos = ForestPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionForest, targetPos);

            // Fountain Position
            targetPos = FountainPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionFountain, targetPos);

            // Kitchen Position
            targetPos = KitchenPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionKitchen, targetPos);

            // Lake Position
            targetPos = LakePositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionLake, targetPos);

            // Quarry Position
            targetPos = QuarryPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionQuarry, targetPos);

            // Workshop Position
            targetPos = WorkshopPositions.I.GetNearestPos(transform.position);
            memory.WorldState.SetSymbol(SchrubberSymbols.PositionWorkshop, targetPos);
        }
    }
}


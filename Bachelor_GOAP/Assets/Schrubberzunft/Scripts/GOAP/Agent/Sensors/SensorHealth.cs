﻿using GOAP.Unity;

namespace Schrubber.GOAP
{
    public class SensorHealth : GOAP_Sensor
    {
        public override void UpdateSensor(GOAP_Memory memory)
        {
            Schrubber_Memory mem = memory as Schrubber_Memory;

            // If hp is below 50%, change currentSquad to BaseSquad
            if ((float)mem.Blackboard.HealthPoints < ((float)Blackboard.MaxHealthPoints / 2f))
                mem.Blackboard.ChangeSquadBase();
        }
    }
}
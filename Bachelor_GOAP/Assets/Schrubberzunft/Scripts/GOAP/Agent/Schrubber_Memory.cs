﻿using GOAP.Unity;

namespace Schrubber.GOAP
{
    public class Schrubber_Memory : GOAP_Memory
    {
        protected override void Awake()
        {
            base.Awake();
            base.WorldState.SetSymbol(SchrubberSymbols.AtPos, transform.position);

            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Fish), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Food), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Stone), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Resource_Wood), false);

            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Axe), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Broom), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_FishingRod), false);
            base.WorldState.SetSymbol(SchrubberSymbols.HasItem(SchrubberSymbols.Tool_Pickaxe), false);

            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Resource_Fish), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Resource_Food), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Resource_Stone), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Resource_Wood), false);

            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Axe), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Broom), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_FishingRod), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Pickaxe), false);

            base.WorldState.SetSymbol(SchrubberSymbols.ItemCrafted(SchrubberSymbols.Resource_Food), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemCrafted(SchrubberSymbols.Tool_Axe), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemCrafted(SchrubberSymbols.Tool_Broom), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemCrafted(SchrubberSymbols.Tool_FishingRod), false);
            base.WorldState.SetSymbol(SchrubberSymbols.ItemCrafted(SchrubberSymbols.Tool_Pickaxe), false);

            base.WorldState.SetSymbol(SchrubberSymbols.IsDirty, true);
            base.WorldState.SetSymbol(SchrubberSymbols.IsHungry, true);
            base.WorldState.SetSymbol(SchrubberSymbols.IsThirsty, true);

            base.WorldState.SetSymbol(SchrubberSymbols.DirtMonsterAttacked, false);
            base.WorldState.SetSymbol(SchrubberSymbols.DirtSpotCleaned, false);
            base.WorldState.SetSymbol(SchrubberSymbols.DungeonStorageFilled, false);
            base.WorldState.SetSymbol(SchrubberSymbols.EnoughResourcesForDungeonStorageInInventory, false);
            base.WorldState.SetSymbol(SchrubberSymbols.IsAtRendezvousPoint, false);
            base.WorldState.SetSymbol(SchrubberSymbols.TreasureChestOpened, false);
        }

        public override void Init(GOAP_Agent agent)
        {
            base.Init(agent);

            if (agent is Schrubber_Agent)
            {
                Blackboard = (agent as Schrubber_Agent).Blackboard;
            }
        }

        public Blackboard Blackboard {private set; get;}
    }
}
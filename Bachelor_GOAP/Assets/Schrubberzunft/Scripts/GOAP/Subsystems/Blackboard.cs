﻿using System;
using UnityEngine;
using GOAP.Unity;
using Schrubber.Item;
using Schrubber.Game.Dungeon;
using Schrubber.Game;
using Schrubber.Hierarchy;

namespace Schrubber.GOAP
{
    public class Blackboard : MonoBehaviour
    {
        private Schrubber_Agent _agent = null;
        private IMotor _motor = null;
        private Inventory _inv = null;

        private ParticleSystem _particleSystem = null;

        public void Init(Schrubber_Agent agent)
        {
            _agent = agent;
            _motor = GetComponent<IMotor>();
            _inv = new Inventory();
            _particleSystem = GetComponentInChildren<ParticleSystem>();
        }

        public void GotoPos(Vector3 targetPos, Action onFinished, Action onFailed)
        {
            _motor.Goto(targetPos, onFinished, onFailed);
        }

        public Vector3 GetRendezvousPosition(Vector3 fromPos)
        {
            RendezVouzPoint point = GameManager.I.DungeonManager.RendezvousPositionManager.GetRendezvousPoint(fromPos, _agent);
            point.Claim(_agent);
            return point.Pos;
        }

        /// <summary>
        /// This agent is now the target of the dirt monster. Only agents in cleanSquad can be targeted
        /// </summary>
        public bool TargetByDirtMonster(DirtMonster monster)
        {
            if (_agent.CurrentSquad is CleanSquad)
            {
                _agent.CurrentSquad.MemberNotification(_agent, ENotificationType.TargetedByDirtMonster, monster);
                return true;
            }
            return false;
        }

        /// <summary>
        /// An agent has spotted a treasure chest
        /// </summary>
        public void TreasureChestSpotted(TreasureChest chest)
        {
            if (_agent.CurrentSquad is CleanSquad)
                _agent.CurrentSquad.MemberNotification(_agent, ENotificationType.TreasureChestSpotted, chest);
        }

        /// <summary>
        /// Change the current squad to base squad
        /// </summary>
        public void ChangeSquadBase()
        {
            if (!(_agent.CurrentSquad is BaseSquad))
                _agent.CurrentSquad.MemberNotification(_agent, ENotificationType.SquadChangeBase, null);
        }

        public void PlayAttackAnimation()
        {
            _particleSystem.Stop();
            _particleSystem.Play();
        }

        public void AttackByMonster(int damage)
        {
            HealthPoints -= damage;
            if (HealthPoints <= 0)
                _agent.CurrentSquad.MemberNotification(_agent, ENotificationType.AgentDead, null);
        }

        public void Heal(int amount)
        {
            HealthPoints += amount;
            if (HealthPoints > MaxHealthPoints)
                HealthPoints = MaxHealthPoints;
        }

        public void UpdateTargetRoom()
        {
            if (TargetRoom == null ||
                TargetRoom.IsClean ||
                TargetRoom.IsOverfilled)
            {
                GameManager.I.DungeonManager.UnAssignAgent(_agent);
                TargetRoom = GameManager.I.DungeonManager.AssignAgentToRoom(_agent);
            }
        }

        /// <summary>
        /// Is this agent responsible for filling up the dungeon storage
        /// </summary>
        public bool IsDungeonStorageRefillAgent { get; set; } = false;

        public DirtMonster TargetDirtMonster { get; set; }

        public DungeonRoom TargetRoom { get; set; }

        public TreasureChest TargetTreasureChest { get; set; }

        public Inventory Inventory => _inv;

        public EGoalType CurrentSquadType => _agent.CurrentSquad.SquadType;

        /// <summary>
        /// Can this agent take items from dungeonStorage. Is true after rendezvous was finished
        /// </summary>
        public bool CanRefillAtDungeonStorage { get; set; } = false;

        public int HealthPoints { private set; get; } = MaxHealthPoints;

        public static int MaxHealthPoints => 5;

        public Vector3 Pos => transform.position;
    }
}
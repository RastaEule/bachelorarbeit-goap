﻿using UnityEngine;

namespace Schrubber.Item
{
    public class Storage : MonoBehaviour
    {
        /// <summary>
        /// Add some tools at the beginning of the game
        /// </summary>
        [SerializeField][Tooltip("Add some tools at the beginning of the game")]
        private int _initialTools = 3;

        private void Awake()
        {
            Position = transform.position;
            Inventory = new Inventory();

            for (int i= 0; i < _initialTools; i++)
            {
                Inventory.AddItem(new Tool_Axe());
                Inventory.AddItem(new Tool_Broom());
                Inventory.AddItem(new Tool_FishingRod());
                Inventory.AddItem(new Tool_Pickaxe());
            }
            if (_initialTools == -1)
            {
                //Inventory.AddItem(new Resource_Wood());
                Inventory.AddItem(new Resource_Wood());
                Inventory.AddItem(new Resource_Wood());
                Inventory.AddItem(new Resource_Fish());
                Inventory.AddItem(new Resource_Fish());
                Inventory.AddItem(new Resource_Fish());
            }
        }

        public Inventory Inventory {private set; get;}

        public Vector3 Position { private set; get; }
    }
}
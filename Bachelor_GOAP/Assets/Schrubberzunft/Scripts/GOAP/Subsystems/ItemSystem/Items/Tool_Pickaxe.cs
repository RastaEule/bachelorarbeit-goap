﻿namespace Schrubber.Item
{
    public class Tool_Pickaxe : Tool
    {
        private static int DURATION = 3;

        public Tool_Pickaxe() : base(DURATION)
        {
        }

        public override EItem Type => EItem.Tool_Pickaxe;
    }
}
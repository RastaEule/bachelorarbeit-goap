﻿namespace Schrubber.Item
{
    public class Tool_Axe : Tool
    {
        private static int DURATION = 5;

        public Tool_Axe() : base(DURATION)
        {
        }

        public override EItem Type => EItem.Tool_Axe;
    }
}
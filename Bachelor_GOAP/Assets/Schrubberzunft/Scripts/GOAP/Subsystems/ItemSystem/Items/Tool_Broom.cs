﻿namespace Schrubber.Item
{
    public class Tool_Broom : Tool
    {
        public static int DURATION = 3;

        public Tool_Broom() : base(DURATION)
        {
        }

        public override EItem Type => EItem.Tool_Broom;
    }
}
﻿namespace Schrubber.Item
{
    public class Tool_FishingRod : Tool
    {
        private static int DURATION = 3;

        public Tool_FishingRod() : base(DURATION)
        {
        }

        public override EItem Type => EItem.Tool_FishingRod;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Schrubber.Item
{
    public abstract class Tool : Item
    {
        /// <summary>
        /// After how many uses does this tool break
        /// </summary>
        private int _totalDurability;

        /// <summary>
        /// How many uses are left until this tool will break
        /// </summary>
        private int _currentDurability;

        /// <summary>
        /// Creates a new tool
        /// </summary>
        /// <param name="durability">After how many uses does this tool break</param>
        protected Tool(int durability)
        {
            _totalDurability = durability;
            _currentDurability = durability;
        }

        /// <summary>
        /// Use this tool.
        /// </summary>
        /// <returns>True, if the tool broke after using it</returns>
        public bool Use()
        {
            _currentDurability--;
            return _currentDurability <= 0;
        }

        /// <summary>
        /// How many resources you get when using this tool
        /// </summary>
        public int ResourceObtainAmount => LZRandom.I.Rand.Next(1, 5);

        /// <summary>
        /// Has this tool ever been used. True=never used, false=used
        /// </summary>
        public bool IsPristine => _currentDurability >= _totalDurability;
    }
}
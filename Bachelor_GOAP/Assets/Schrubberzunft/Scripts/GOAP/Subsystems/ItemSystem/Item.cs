﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Logging;

namespace Schrubber.Item
{
    public abstract class Item
    {
        protected Item()
        {
        }

        public abstract EItem Type { get; }
    }
}
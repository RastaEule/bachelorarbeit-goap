﻿using System;
using System.Collections.Generic;
using GOAP.Logging;

namespace Schrubber.Item
{
    public class Inventory
    {
        private Dictionary<EItem, List<Item>> _inventory;

        public Inventory()
        {
            _inventory = new Dictionary<EItem, List<Item>>();

            // Add all items
            foreach (EItem item in (EItem[])Enum.GetValues(typeof(EItem))) // The cast to array is not necessary but makes the code 0.5ns faster
                if (item != EItem.Nothing)
                    _inventory.Add(item, new List<Item>());
        }

        public void AddItem(Item itemToAdd)
        {
            List<Item> items;
            if (_inventory.TryGetValue(itemToAdd.Type, out items))
            {
                items.Add(itemToAdd);
                _inventory[itemToAdd.Type] = items;
            }
            else
                GOAP_Logger.I.Log("Itemtype not implemented", EMessageType.Info, EMessageSeverity.Error);
        }

        public void AddItems(List<Item> itemsToAdd)
        {
            foreach (Item i in itemsToAdd)
                this.AddItem(i);
        }

        public void AddItems(Recipe r)
        {
            this.AddItems(r.InternalRecipe);
        }

        public void AddItems(List<KeyValuePair<EItem, int>> itemsToAdd)
        {
            foreach(KeyValuePair<EItem, int> kvp in itemsToAdd)
                for (int i = 0; i < kvp.Value; i++)
                    this.AddItem(Utils.GetItemInstance(kvp.Key));
        }

        /// <summary>
        /// Look at the first item
        /// </summary>
        /// <returns>True, if an item of this type exists</returns>
        public bool PeekItem(EItem itemType, out Item item)
        {
            item = null;
            List<Item> items;
            if (_inventory.TryGetValue(itemType, out items))
            {
                if (items == null || items.Count == 0)
                    return false;

                item = items[0];
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Look at all items of a type
        /// </summary>
        /// <returns>True, if an item of this type exists</returns>
        public bool PeekItems(EItem itemType, out List<Item> items)
        {
            items = new List<Item>();
            if (_inventory.TryGetValue(itemType, out List<Item> invitems))
            {
                if (invitems == null || invitems.Count == 0)
                    return false;

                foreach (Item i in invitems)
                    items.Add(i);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Does an item exist
        /// </summary>
        public bool ItemExists(EItem itemType)
        {
            Item item;
            return this.PeekItem(itemType, out item);
        }

        public int ItemCount(EItem itemType)
        {
            int count = -1;
            if (_inventory.TryGetValue(itemType, out List<Item> items))
                return items.Count;
            return count;
        }

        /// <summary>
        /// Take the first item out of the inventory
        /// </summary>
        /// <returns>True, if an item of this type exists</returns>
        public bool TakeItem(EItem itemType, out Item item)
        {
            item = null;
            List<Item> items;
            if (_inventory.TryGetValue(itemType, out items))
            {
                if (items == null || items.Count == 0)
                    return false;

                item = items[0];
                items.RemoveAt(0);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Remove an item of this type from inventory
        /// </summary>
        public void TakeItem(EItem itemType)
        {
            Item i;
            this.TakeItem(itemType, out i);
        }

        /// <summary>
        /// Remove multiple items from this type from inventory. You can specify how many you want and get them in a list.
        /// </summary>
        public bool TakeItems(EItem itemType, int itemCount, out List<Item> resultItems)
        {
            resultItems = new List<Item>();
            List<Item> items;
            if (_inventory.TryGetValue(itemType, out items))
            {
                if (items == null || items.Count <= 0)
                    return false;

                int itemsToTake = items.Count < itemCount ? items.Count : itemCount;
                for (int i= 0; i < itemsToTake; i++)
                {
                    resultItems.Add(items[0]);
                    items.RemoveAt(0);
                }
                return true;
            }
            return false;
        }

        public void TakeItems(List<KeyValuePair<EItem, int>> itemsToTake)
        {
            foreach (KeyValuePair<EItem, int> kvp in itemsToTake)
            {
                List<Item> items;
                if (_inventory.TryGetValue(kvp.Key, out items) &&
                    items.Count >= kvp.Value)
                    items.RemoveRange(0, kvp.Value);
            }
        }

        /// <summary>
        /// Remove all items of a type from this inventory.
        /// </summary>
        /// <param name="itemCount">How many items were removed</param>
        /// <returns></returns>
        public bool TakeAllItems(EItem itemType, out int itemCount)
        {
            itemCount = 0;
            List<Item> items;
            if (_inventory.TryGetValue(itemType, out items))
            {
                if (items == null || items.Count <= 0)
                    return false;

                itemCount = items.Count;
                _inventory[itemType] = new List<Item>();
                return true;
            }
            return false;
        }

        public void TakeItemsFromRecipe(Recipe r)
        {
            this.TakeItems(r.InternalRecipe);
        }

        #region Helper functions

        /// <summary>
        /// Has enough resources so the recipe can be crafted
        /// </summary>
        public bool HasEnoughResources(Recipe r)
        {
            foreach(KeyValuePair<EItem, int> kvp in r.InternalRecipe) // Go through each needed resource
            {
                List<Item> items;
                if ((_inventory.TryGetValue(kvp.Key, out items) && items.Count < kvp.Value) || // Item exists, but not enough of it
                    !_inventory.TryGetValue(kvp.Key, out items)) // Item doesnt exist
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Has enough resources so the recipe can be crafted
        /// </summary>
        /// <param name="neededItems">If there aren't enough resources, these are the items that are missing</param>
        /// <returns></returns>
        public bool HasEnoughResources(Recipe r, out List<KeyValuePair<EItem, int>> neededItems)
        {
            neededItems = new List<KeyValuePair<EItem, int>>();
            foreach (KeyValuePair<EItem, int> kvp in r.InternalRecipe) // Go through each needed resource
            {
                if (_inventory.TryGetValue(kvp.Key, out List<Item> items)) // Item exists
                {
                    if (items.Count < kvp.Value) // Item exists, but not enough of it
                        neededItems.Add(new KeyValuePair<EItem, int>(kvp.Key, kvp.Value - items.Count));
                }
                else // Item doesn't exist
                    neededItems.Add(new KeyValuePair<EItem, int>(kvp.Key, kvp.Value));
            }
            return neededItems.Count == 0;
        }

        /// <summary>
        /// Has enough resources combined with another inventory so the recipe can be crafted
        /// </summary>
        /// <param name="itemsToTake">The items to take from this inventory so the otherI has enough resources for the recipe</param>
        public bool HasEnoughResourcesCombined(Inventory otherI, Recipe r, out List<KeyValuePair<EItem, int>> itemsToTake)
        {
            bool result = true;
            itemsToTake = new List<KeyValuePair<EItem, int>>();
            foreach(KeyValuePair<EItem, int> recipeSymbol in r.InternalRecipe)
            {
                List<Item> thisItems;
                List<Item> otherItems;
                if (_inventory.TryGetValue(recipeSymbol.Key, out thisItems) &&
                    otherI.InternalInventory.TryGetValue(recipeSymbol.Key, out otherItems))
                {
                    // Check how many items need to be took from this inventory
                    int amountToTakeFromThis = 0;
                    if (otherItems.Count < recipeSymbol.Value) // The other inventory only needs to take items from this, if it doesn't have enough already
                    {
                        int missing = recipeSymbol.Value - otherItems.Count; // How many items need to be took from this inventory
                        if (thisItems.Count < missing) // If this doesn't have enough -> not possible
                            return false;
                        else
                            amountToTakeFromThis = missing;
                    }

                    // Save the items to take from this
                    if (amountToTakeFromThis > 0)
                        itemsToTake.Add(new KeyValuePair<EItem, int>(recipeSymbol.Key, amountToTakeFromThis));
                }
                else
                    return false;
            }
            return result;
        }

        #endregion

        public Dictionary<EItem, List<Item>> InternalInventory => _inventory;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Schrubber.Item
{
    /// <summary>
    /// Describes which resources are needed in order to craft an item
    /// </summary>
    public class Recipe
    {
        private List<KeyValuePair<EItem, int>> _recipe;

        public Recipe(params KeyValuePair<EItem, int>[] list)
        {
            _recipe = new List<KeyValuePair<EItem, int>>();

            foreach(KeyValuePair<EItem, int> kvp in list)
            {
                _recipe.Add(kvp);
            }
        }

        /// <summary>
        /// Add another item to this recipe
        /// </summary>
        public void AddItem(KeyValuePair<EItem, int> item)
        {
            bool sameKeyExists = false;
            KeyValuePair<EItem, int> kvpToRemove;
            foreach (KeyValuePair<EItem, int> kvp in _recipe) // Check if the key already exists
            {
                if (kvp.Key == item.Key)
                {
                    sameKeyExists = true;
                    kvpToRemove = kvp; // Cannot modify kvp value directly, so save this to delete it and add a new kvp with the new value later
                    break;
                }
            }
            if (sameKeyExists)
            {
                int newValue = kvpToRemove.Value + item.Value;
                _recipe.Remove(kvpToRemove); // Remove old kvp with same key
                _recipe.Add(new KeyValuePair<EItem, int>(item.Key, newValue)); // Add new kvp with updated value
            }
            else
            {
                _recipe.Add(item);
            }
        }

        public List<KeyValuePair<EItem, int>> InternalRecipe => _recipe;

        #region Helper Functions

        public static Recipe operator+ (Recipe a, Recipe b)
        {
            Recipe result = new Recipe();
            foreach(KeyValuePair<EItem, int> kvp in b.InternalRecipe)
                a.AddItem(kvp);

            foreach (KeyValuePair<EItem, int> kvp in a.InternalRecipe)
                result.AddItem(kvp);

            return result;
        }

        #endregion
    }
}
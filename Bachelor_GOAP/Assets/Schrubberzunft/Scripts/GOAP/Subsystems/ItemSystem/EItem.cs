﻿namespace Schrubber.Item
{
    /// <summary>
    /// This enum contains all possible items
    /// </summary>
    public enum EItem
    {
        Nothing,
        Tool_Axe,
        Tool_Broom,
        Tool_FishingRod,
        Tool_Pickaxe,
        Resource_Fish,
        Resource_Food,
        Resource_Stone,
        Resource_Wood,
    }
}
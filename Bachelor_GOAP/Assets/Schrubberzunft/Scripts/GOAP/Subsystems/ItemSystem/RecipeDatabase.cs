﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Logging;

namespace Schrubber.Item
{
    public static class RecipeDatabase
    {
        public static Recipe GetRecipe(EItem itemtype)
        {
            Recipe r = new Recipe();

            switch(itemtype)
            {
                case EItem.Tool_Axe:
                    {
                        r = new Recipe(
                            new KeyValuePair<EItem, int>(EItem.Resource_Wood, 2),
                            new KeyValuePair<EItem, int>(EItem.Resource_Stone, 1));
                        break;
                    }
                case EItem.Tool_Broom:
                    {
                        r = new Recipe(
                            new KeyValuePair<EItem, int>(EItem.Resource_Wood, 2));
                        break;
                    }
                case EItem.Tool_FishingRod:
                    {
                        r = new Recipe(
                            new KeyValuePair<EItem, int>(EItem.Resource_Wood, 3));
                        break;
                    }
                case EItem.Tool_Pickaxe:
                    {
                        r = new Recipe(
                            new KeyValuePair<EItem, int>(EItem.Resource_Wood, 2),
                            new KeyValuePair<EItem, int>(EItem.Resource_Stone, 2));
                        break;
                    }
                case EItem.Resource_Food:
                    {
                        r = new Recipe(new KeyValuePair<EItem, int>(EItem.Resource_Fish, 1));
                        break;
                    }
                default:
                    {
                        GOAP_Logger.I.Log("No recipe for this item found: " + itemtype.ToString(), EMessageType.Info, EMessageSeverity.Error);
                        break;
                    }
            }
            return r;
        }
    }
}
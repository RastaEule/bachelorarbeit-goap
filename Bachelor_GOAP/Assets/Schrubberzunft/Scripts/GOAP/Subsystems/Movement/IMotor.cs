﻿using System;
using UnityEngine;

namespace Schrubber.GOAP
{
    public interface IMotor
    {
        void Goto(Vector3 targetPosition, Action onFinished, Action onFailed);
    }
}
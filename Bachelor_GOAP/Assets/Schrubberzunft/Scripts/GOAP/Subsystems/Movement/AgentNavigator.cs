﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Schrubber.GOAP
{
    public class AgentNavigator : MonoBehaviour, IMotor
    {
        /// <summary>
        /// Distance between currentpos and target pos that is considered to have reached the targetpos
        /// </summary>
        [SerializeField]
        [Tooltip("Distance between currentpos and target pos that is considered to have reached the targetpos")]
        private float _destReachedThreshold = 0.1f;
        [SerializeField]
        private float _moveTimeout = 2f;

        private bool _move = false;
        private Vector3 _targetPos = Vector3.zero;
        private Action _movementFinished = null;
        private Action _movementFailed = null;
        private float _startMoveTime = 0;

        private NavMeshAgent _navMeshAgent = null;

        private void Awake()
        {
            _navMeshAgent = GetComponentInParent<NavMeshAgent>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_move)
            {
                Vector3 currentPos = Vector3.Scale(new Vector3(1, 0, 1), transform.position);
                Vector3 targetPos = Vector3.Scale(new Vector3(1, 0, 1), _targetPos);

                if (Vector3.Distance(currentPos, targetPos) < _destReachedThreshold)
                {
                    // Reached targetPos
                    _move = false;
                    _movementFinished();
                }
                if (Time.time - _startMoveTime > _moveTimeout)
                {
                    _move = false;
                    _movementFailed();
                }
            }
        }

        public void Goto(Vector3 targetPosition, Action onFinished, Action onFailed)
        {
            _movementFinished = onFinished;
            _movementFailed = onFailed;
            _targetPos = targetPosition;
            _startMoveTime = Time.time;
            _move = true;

            _navMeshAgent.SetDestination(targetPosition);
        }
    }
}
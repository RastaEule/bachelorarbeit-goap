﻿using System.Collections.Generic;

namespace Schrubber.Hierarchy
{
    public class OrderStatus
    {
        /// <summary>
        /// SquadMembers that have to finish an order
        /// </summary>
        private Dictionary<EOrderType, List<ISquadMember>> _membersToFinish;
        /// <summary>
        /// SquadMembers that have already finished their order
        /// </summary>
        private Dictionary<EOrderType, List<ISquadMember>> _membersFinished;

        public OrderStatus()
        {
            _membersToFinish = new Dictionary<EOrderType, List<ISquadMember>>();
            _membersFinished = new Dictionary<EOrderType, List<ISquadMember>>();
        }

        public void StartNewOrder(EOrderType orderType, List<ISquadMember> members)
        {
            if (_membersToFinish.TryGetValue(orderType, out List<ISquadMember> toFinish) &&
                _membersFinished.TryGetValue(orderType, out List<ISquadMember> finished))
            {
                // This order already exists
                // Create new clean lists
                toFinish = new List<ISquadMember>();
                finished = new List<ISquadMember>();

                foreach (ISquadMember member in members) // Initialize list
                    if (!toFinish.Contains(member))
                        toFinish.Add(member);
            }
            else // This order doesn't exist yet
            {
                // Create new order
                _membersToFinish.Add(orderType, new List<ISquadMember>());
                _membersFinished.Add(orderType, new List<ISquadMember>());

                foreach (ISquadMember member in members) // Initialize list
                    _membersToFinish[orderType].Add(member);
            }
        }

        /// <summary>
        /// Remove all references to agents for a given order
        /// </summary>
        /// <param name="orderType"></param>
        public void StopOrder(EOrderType orderType)
        {
            _membersToFinish.Remove(orderType);
            _membersFinished.Remove(orderType);
        }

        /// <summary>
        /// Add a single squad member to a specific order
        /// </summary>
        public void AddSquadMember(ISquadMember member, EOrderType orderType)
        {
            if (_membersToFinish.TryGetValue(orderType, out List<ISquadMember> toFinish) &&
                _membersFinished.TryGetValue(orderType, out List<ISquadMember> finished))
            {
                // This order already exists, add the agent
                if (!toFinish.Contains(member))
                    toFinish.Add(member);
                if (finished.Contains(member))
                    finished.Remove(member);
            }
            else // this order doesn't exist yet: start a new order
            {
                List<ISquadMember> newmembers = new List<ISquadMember>();
                newmembers.Add(member);
                this.StartNewOrder(orderType, newmembers);
            }
        }

        /// <summary>
        /// Remove all references to this squad member
        /// </summary>
        public void RemoveSquadMember(ISquadMember member)
        {
            // Check for each ordertype if this member exists and remove him
            foreach (KeyValuePair<EOrderType, List<ISquadMember>> kvp in _membersFinished)
                if (kvp.Value.Contains(member))
                    kvp.Value.Remove(member);

            foreach (KeyValuePair<EOrderType, List<ISquadMember>> kvp in _membersToFinish)
                if (kvp.Value.Contains(member))
                    kvp.Value.Remove(member);
        }

        public void AgentOrderFinished(EOrderType orderType, ISquadMember member)
        {
            if (_membersFinished.TryGetValue(orderType, out List<ISquadMember> membersFinished) &&
                !membersFinished.Contains(member))
                membersFinished.Add(member);
        }

        /// <summary>
        /// Get all agents that have finished a specific order
        /// </summary>
        public List<ISquadMember> GetMembersFinished(EOrderType orderType)
        {
            if (_membersFinished.TryGetValue(orderType, out List<ISquadMember> membersFinished))
                return membersFinished;
            else
                return new List<ISquadMember>();
        }

        public bool Exists(EOrderType orderType)
        {
            return _membersFinished.ContainsKey(orderType) && _membersToFinish.ContainsKey(orderType);
        }

        public bool IsFinished(EOrderType orderType)
        {
            if (_membersToFinish.TryGetValue(orderType, out List<ISquadMember> yetToFinish) &&
                _membersFinished.TryGetValue(orderType, out List<ISquadMember> finished))
            {
                if (finished.Count == yetToFinish.Count)
                    return true;
                return false;
            }
            return false;
        }
    }
}
﻿using System.Collections.Generic;
using Schrubber.GOAP;

namespace Schrubber.Hierarchy
{
    public interface ISquad
    {
        /// <summary>
        /// Initialize
        /// </summary>
        void Init(Commander commander);

        /// <summary>
        /// An agent left this squad.
        /// </summary>
        void SquadMemberLeft(ISquadMember member);

        /// <summary>
        /// Add a new agent to an already active squad
        /// </summary>
        void AddAgent(ISquadMember member);

        /// <summary>
        /// Start this squad
        /// </summary>
        void StartSquad(List<ISquadMember> members);

        /// <summary>
        /// Stop this squad
        /// </summary>
        void StopSquad();

        /// <summary>
        /// A squad members sends this squad a notification
        /// </summary>
        /// <param name="type">The type of the notification</param>
        void MemberNotification(ISquadMember member, ENotificationType type, object target);

        /// <summary>
        /// Agents in this squad
        /// </summary>
        List<ISquadMember> SquadMembers { get; }

        /// <summary>
        /// The transform of this squad for parenting
        /// </summary>
        UnityEngine.Transform Transform { get; }

        /// <summary>
        /// Kind of this squad
        /// </summary>
        EGoalType SquadType { get; }

        /// <summary>
        /// Does this squad operate at the moment?
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// How many agents should be in this squad. -1 for infinitely large squad
        /// </summary>
        int TargetAgentCount { get; set; }
    }
}
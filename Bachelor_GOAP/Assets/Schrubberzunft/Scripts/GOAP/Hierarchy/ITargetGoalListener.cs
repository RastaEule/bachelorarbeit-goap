﻿using GOAP.Core;

namespace Schrubber.Hierarchy
{
    /// <summary>
    /// Listener to changes on a target goal
    /// </summary>
    public interface ITargetGoalListener
    {
        /// <summary>
        /// A plan for the target goal finished executing
        /// </summary>
        void OnTargetGoalFinished(IGOAP_Goal goal);

        /// <summary>
        /// A plan for the target goal failed to execute
        /// </summary>
        void OnTargetGoalFailed(IGOAP_Goal goal);

        /// <summary>
        /// This goal is blacklisted
        /// </summary>
        /// <param name="byPlanner">Was this goal blacklisted by the planner or by someone else</param>
        void OnTargetGoalBlacklisted(IGOAP_Goal goal, bool byPlanner);
    }
}
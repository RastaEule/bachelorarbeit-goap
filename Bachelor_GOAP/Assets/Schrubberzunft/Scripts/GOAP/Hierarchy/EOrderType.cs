﻿namespace Schrubber.Hierarchy
{
    public enum EOrderType
    {
        None,
        AttackEnemy,
        CleanRoom,
        OpenTreasureChest,
        Rendezvous,
        Wait
    }
}
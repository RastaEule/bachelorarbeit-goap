﻿namespace Schrubber.Hierarchy
{
    public enum ENotificationType
    {
        None,
        AgentDead,
        SquadChangeBase,
        TargetedByDirtMonster,
        TreasureChestSpotted,
    }
}
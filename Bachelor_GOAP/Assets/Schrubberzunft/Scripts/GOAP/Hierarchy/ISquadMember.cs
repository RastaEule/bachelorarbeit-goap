﻿using Schrubber.GOAP;

namespace Schrubber.Hierarchy
{
    public interface ISquadMember
    {
        void SetSquad(ISquad squad);

        void SetOrder(Order order);

        /// <summary>
        /// Check how well the personal goals of this squadMember are fulfilled. Lower = better
        /// </summary>
        float GetPersonalUtility();

        ISquad CurrentSquad { get; }

        Blackboard Blackboard { get; }
    }
}


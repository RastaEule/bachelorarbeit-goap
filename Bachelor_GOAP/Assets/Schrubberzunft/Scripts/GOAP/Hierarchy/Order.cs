﻿using System;
using Schrubber.GOAP;
using Schrubber.Game.Dungeon;
using Schrubber.Game;

namespace Schrubber.Hierarchy
{
    public class Order
    {
        public Order(EOrderType type, Action<Schrubber_Agent, EOrderType> onFinished, Action<Schrubber_Agent, EOrderType> onFailed)
        {
            Type = type;
            OnFinished = onFinished;
            OnFailed = onFailed;
        }

        public Order(EOrderType type, DirtMonster targetDirtMonster, Action<Schrubber_Agent, EOrderType> onFinished, Action<Schrubber_Agent, EOrderType> onFailed)
        {
            Type = type;
            TargetDirtMonster = targetDirtMonster;
            OnFinished = onFinished;
            OnFailed = onFailed;
        }

        public Order(EOrderType type, DungeonRoom targetRoom, Action<Schrubber_Agent, EOrderType> onFinished, Action<Schrubber_Agent, EOrderType> onFailed)
        {
            Type = type;
            TargetRoom = targetRoom;
            OnFinished = onFinished;
            OnFailed = onFailed;
        }

        public Order(EOrderType type, TreasureChest targetTreasureChest, Action<Schrubber_Agent, EOrderType> onFinished, Action<Schrubber_Agent, EOrderType> onFailed)
        {
            Type = type;
            TargetTreasureChest = targetTreasureChest;
            OnFinished = onFinished;
            OnFailed = onFailed;
        }

        public EOrderType Type { private set; get; } = EOrderType.None;

        public DirtMonster TargetDirtMonster { private set; get; }

        public DungeonRoom TargetRoom { private set; get; }

        public TreasureChest TargetTreasureChest { private set; get; }

        public Action<Schrubber_Agent, EOrderType> OnFinished { private set; get; }

        public Action<Schrubber_Agent, EOrderType> OnFailed { private set; get; }
    }
}
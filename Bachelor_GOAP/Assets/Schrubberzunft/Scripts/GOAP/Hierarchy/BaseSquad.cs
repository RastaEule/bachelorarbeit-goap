﻿using System.Collections.Generic;
using UnityEngine;
using Schrubber.GOAP;
using GOAP.Logging;

namespace Schrubber.Hierarchy
{
    public class BaseSquad : MonoBehaviour, ISquad
    {
        private Commander _commander;
        private bool _isActive = true;

        private List<ISquadMember> _members;

        private int _targetAgentCount = -1;

        #region ISquad Members

        public void Init(Commander commander)
        {
            _commander = commander;
        }

        public void StartSquad(List<ISquadMember> members)
        {
            _members = members;
            foreach (ISquadMember m in _members)
                m.SetSquad(this);
        }

        public void StopSquad()
        {
            GOAP_Logger.I.Log("BaseSquad should never be stopped!", EMessageType.Info, EMessageSeverity.Error);
            _commander.StopSquad(SquadType);
            _isActive = false;
            _members = null;
        }

        public void SquadMemberLeft(ISquadMember member)
        {
            _members.Remove(member);
        }

        public void AddAgent(ISquadMember member)
        {
            member.SetSquad(this);
            if (!_members.Contains(member))
                _members.Add(member);
        }


        public void MemberNotification(ISquadMember member, ENotificationType type, object target)
        {
            switch(type)
            {
                case ENotificationType.AgentDead:
                    _commander.RemoveAgent((Schrubber_Agent)member);
                    break;
            }
        }

        public List<ISquadMember> SquadMembers => _members;

        public EGoalType SquadType => EGoalType.Base;

        public Transform Transform => transform;

        public bool IsActive => _isActive;

        public int TargetAgentCount { get; set; } = -1;

        #endregion
    }
}
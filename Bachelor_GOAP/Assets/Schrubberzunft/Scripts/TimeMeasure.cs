﻿using System;
using UnityEngine;
using GOAP.Logging;

namespace Schrubber
{
    public class TimeMeasure : MonoBehaviour
    {
        private double _startTime;
        private float _elapsedTime = 0.0f;

        // Start is called before the first frame update
        void Start()
        {
            _startTime = Time.time;
            Game.GameManager.I.DungeonManager.OnDungeonCleaned += OnDungeonClean;
        }

        private void FixedUpdate()
        {
            _elapsedTime += Time.fixedDeltaTime;
        }

        private void OnDungeonClean()
        {
            double endTime = Time.time;
            TimeSpan ts = new TimeSpan(0, 0, 0, (int)_elapsedTime, 0);
            // Log as error so i can see it better
            Debug.LogError(string.Format("Time to clean Dungeon: {0}h {1}m {2}s {3}ms", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds));
            Debug.LogError(string.Format("Time to clean Dungeon: {0}s", _elapsedTime));

            Time.timeScale = 1.0f;
        }
    }
}
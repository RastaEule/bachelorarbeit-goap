﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Schrubber
{
    public class PositionManager : MonoBehaviour
    {
        protected List<Transform> _targetPositions = null;

        public void AddTargetPos(Transform t)
        {
            if (_targetPositions == null)
                _targetPositions = new List<Transform>();

            if (!_targetPositions.Contains(t))
                _targetPositions.Add(t);
        }

        public void RemoveTargetPos(Transform t)
        {
            if (_targetPositions.Contains(t))
            {
                _targetPositions.Remove(t);
            }
        }

        #region Getters

        public virtual Vector3 GetNearestPos(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return Vector3.zero;

            Vector3 closest = _targetPositions[0].position;
            for (int i = 1; i < _targetPositions.Count; i++)
            {
                if (Vector3.Distance(fromPos, _targetPositions[i].position) < Vector3.Distance(fromPos, closest))
                    closest = _targetPositions[i].position;
            }
            return closest;
        }

        public Transform GetNearestTransform(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return null;

            Transform closest = _targetPositions[0];
            for (int i = 1; i < _targetPositions.Count; i++)
            {
                if (Vector3.Distance(fromPos, _targetPositions[i].position) < Vector3.Distance(fromPos, closest.position))
                    closest = _targetPositions[i];
            }
            return closest;
        }

        public List<Transform> GetNearestTransforms(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return null;

            List<Transform> closestTransforms = _targetPositions;
            closestTransforms = closestTransforms.OrderBy(x => Vector3.Distance(x.position, fromPos)).ToList();
            return closestTransforms;
        }

        #endregion
    }
}
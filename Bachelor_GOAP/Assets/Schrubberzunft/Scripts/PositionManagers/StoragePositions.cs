﻿using System.Collections.Generic;
using UnityEngine;
using Schrubber.Item;

namespace Schrubber
{
    public class StoragePositions : PositionManager
    {
        public static StoragePositions I = null;

        private void Awake()
        {
            if (I == null)
                I = this;
        }

        public Storage GetNearestStorage(Vector3 fromPos)
        {
            Transform t = base.GetNearestTransform(fromPos);
            return t.GetComponent<Storage>();
        }

        public List<Storage> GetNearestStorages(Vector3 fromPos)
        {
            List<Transform> transforms = base.GetNearestTransforms(fromPos);
            List<Storage> storages = new List<Storage>();
            foreach (Transform t in transforms)
                storages.Add(t.GetComponent<Storage>());
            return storages;
        }
    }
}
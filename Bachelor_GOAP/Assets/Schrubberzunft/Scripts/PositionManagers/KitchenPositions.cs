﻿using System.Collections.Generic;
using UnityEngine;
using Schrubber.Item;

namespace Schrubber
{
    public class KitchenPositions : PositionManager
    {
        public static KitchenPositions I = null;

        private void Awake()
        {
            if (I == null)
                I = this;
        }
    }
}
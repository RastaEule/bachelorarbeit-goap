﻿using UnityEngine;

namespace Schrubber.Game
{
    /// <summary>
    /// This is a house that can be bought
    /// </summary>
    public class BuyHouse : MonoBehaviour
    {
        private FollowObj _followobj = null;

        private void Awake()
        {
            _followobj = GetComponentInChildren<FollowObj>();
        }

        public void Buy(EBuildingTypes type)
        {
            // Actually build it
            GameManager.I.MapManager.InstantiateBuilding(type, transform.position);

            _followobj.StopFollow();
            Destroy(this.gameObject);
        }
    }
}
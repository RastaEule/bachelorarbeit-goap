﻿using UnityEngine;
using GOAP.Core;

namespace Schrubber.Game
{
    public class RendezVouzPoint : MonoBehaviour
    {
        private void Awake()
        {
            Pos = transform.position;
        }

        public void Claim(IGOAP_Agent agent)
        {
            Claimed = true;
            ClaimedAgent = agent;
        }

        public void Unclaim()
        {
            Claimed = false;
            ClaimedAgent = null;
        }

        public Vector3 Pos { private set; get; }

        public bool Claimed { private set; get; } = false;

        public IGOAP_Agent ClaimedAgent { private set; get; }
    }
}
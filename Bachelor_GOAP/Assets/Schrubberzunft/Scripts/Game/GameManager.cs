﻿using UnityEngine;
using Schrubber.Map;
using Schrubber.Game.Dungeon;

namespace Schrubber.Game
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager I = null;

        [SerializeField]
        private Player _player = null;
        [SerializeField]
        private DungeonManager _dungeonManager = null;
        [SerializeField]
        private MapManager _mapManager = null;
        [SerializeField]
        private UIManager _uiManager = null;


        private void Awake()
        {
            I = this;
        }

        public Player Player => _player;
        public DungeonManager DungeonManager => _dungeonManager;
        public MapManager MapManager => _mapManager;
        public UIManager UIManager => _uiManager;
    }
}
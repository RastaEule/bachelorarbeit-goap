﻿
namespace Schrubber.Game
{
    /// <summary>
    /// Buildings, that can be built by the player
    /// </summary>
    public enum EBuildingTypes
    {
        None,
        Bath,
        Forest,
        Fountain,
        Kitchen,
        Lake,
        Quarry,
        Workshop
    }
}
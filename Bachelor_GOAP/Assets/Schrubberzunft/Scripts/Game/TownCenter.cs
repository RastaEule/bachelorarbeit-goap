﻿using UnityEngine;
using Schrubber.Hierarchy;
using Schrubber.GOAP;

namespace Schrubber.Game
{
    public class TownCenter : MonoBehaviour
    {
        [SerializeField]
        private Commander _commander = null;

        [SerializeField]
        private GameObject _agentPrefab = null;

        public void SpawnAgent()
        {
            GameObject go = Instantiate(_agentPrefab, _commander.transform);
            go.transform.position = transform.position;
            Schrubber_Agent a = go.transform.GetComponentInChildren<Schrubber_Agent>();
            if (a == null)
                Destroy(go);
            else
                _commander.AddAgent(a);
        }
    }
}
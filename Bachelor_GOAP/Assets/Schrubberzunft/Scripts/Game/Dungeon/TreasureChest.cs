﻿using System.Collections.Generic;
using UnityEngine;
using Schrubber.GOAP;

namespace Schrubber.Game
{
    public class TreasureChest : MonoBehaviour
    {
        /// <summary>
        /// Agents that have already opened this chest
        /// </summary>
        private List<Blackboard> _agentsOpened;

        private void Awake()
        {
            Pos = transform.parent.position;
            _agentsOpened = new List<Blackboard>();
        }

        public void Open(Blackboard blackboard)
        {
            if (!_agentsOpened.Contains(blackboard))
                _agentsOpened.Add(blackboard);

            if (_agentsOpened.Count >= NeededAgents)
            {
                IsOpen = true;
                GameManager.I.Player.AddMoney(25);
                this.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// How many agents are needed to open this chest
        /// </summary>
        public int NeededAgents => 2;

        public Vector3 Pos { private set; get; } = Vector3.zero;

        public bool IsOpen { private set; get; } = false;
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Schrubber.Hierarchy;

namespace Schrubber.Game.Dungeon
{
    public class DungeonManager : MonoBehaviour
    {
        private List<DungeonRoom> _roomsToClean = null;

        #region Unity

        private void Awake()
        {
            _roomsToClean = new List<DungeonRoom>(GetComponentsInChildren<DungeonRoom>());
            foreach (DungeonRoom dr in _roomsToClean)
                dr.Init(this);

            RendezvousPositionManager = GetComponentInChildren<RendezvousPositionManager>();
        }

        #endregion

        public DungeonRoom AssignAgentToRoom(ISquadMember member)
        {
            foreach(DungeonRoom room in _roomsToClean)
            {
                if (!room.IsClean &&
                    !room.AllDirtSpotsClaimed &&
                    room.AssignedAgents.Count < room.GetTargetSchrubberCount())
                {
                    room.AssignAgent(member);
                    return room;
                }
            }
            return null;
        }

        public void UnAssignAgent(ISquadMember member)
        {
            foreach(DungeonRoom room in _roomsToClean)
                room.UnAssignAgent(member);
        }

        public void ClearAssignments()
        {
            foreach (DungeonRoom room in _roomsToClean)
                room.ClearAssignments();
        }

        /// <summary>
        /// A room is now clean, remove from the list of rooms that need to be cleaned
        /// </summary>
        public void RemoveRoom(DungeonRoom room)
        {
            _roomsToClean.Remove(room);

            if (this.IsClean)
                OnDungeonCleaned?.Invoke();
        }

        public int TotalNeededAgents
        {
            get
            {
                int neededAgents = 0;
                foreach (DungeonRoom room in _roomsToClean)
                    neededAgents += room.GetTargetSchrubberCount();
                return neededAgents;
            }
        }

        public bool IsClean
        {
            get
            {
                foreach (DungeonRoom room in _roomsToClean)
                    if (!room.IsClean)
                        return false;
                return true;
            }
        }

        public List<DungeonRoom> Rooms => _roomsToClean;

        public RendezvousPositionManager RendezvousPositionManager { private set; get; }

        public Action OnDungeonCleaned { set; get; }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;
using Schrubber.Hierarchy;

namespace Schrubber.Game.Dungeon
{
    public class DungeonRoom : MonoBehaviour
    {
        private DungeonManager _dungeon = null;

        /// <summary>
        /// How many dirtspots an agent cleans
        /// </summary>
        private static int CLEANCOUNT_PER_AGENT = 2;

        /// <summary>
        /// Spots that need to be cleaned
        /// </summary>
        private List<DirtSpot> _targetCleanPositions = null;

        private List<ISquadMember> _assignedAgents = null;

        private void Awake()
        {
            _targetCleanPositions = new List<DirtSpot>(GetComponentsInChildren<DirtSpot>());
            foreach (DirtSpot d in _targetCleanPositions)
                d.Init(this);
        }

        public void Init(DungeonManager dungeon)
        {
            _dungeon = dungeon;
            _assignedAgents = new List<ISquadMember>();
        }

        public void AssignAgent(ISquadMember member)
        {
            _assignedAgents.Add(member);
        }

        public void UnAssignAgent(ISquadMember member)
        {
            if (_assignedAgents.Contains (member))
                _assignedAgents.Remove(member);
        }
        
        public void ClearAssignments()
        {
            _assignedAgents = new List<ISquadMember>();
        }

        /// <summary>
        /// How many Schrubbers are required to clean this room
        /// </summary>
        /// <param name="cleanCountPerSchrubber">How many dirtspots are cleaned by one schrubber</param>
        /// <returns></returns>
        public int GetTargetSchrubberCount()
        {
            return Mathf.CeilToInt((float)_targetCleanPositions.Count / CLEANCOUNT_PER_AGENT);
        }

        public DirtSpot GetNextDirtSpot()
        {
            foreach(DirtSpot spot in _targetCleanPositions)
                if (!spot.Claimed)
                    return spot;
            return null;
        }

        /// <summary>
        /// A dirt spot was cleaned, remove from the list of spots that need to be cleaned
        /// </summary>
        public void RemoveSpot(DirtSpot spot)
        {
            _targetCleanPositions.Remove(spot);
            if (IsClean)
            {
                _dungeon.RemoveRoom(this);
            }
        }

        public bool AllDirtSpotsClaimed
        {
            get
            {
                foreach (DirtSpot spot in _targetCleanPositions)
                    if (!spot.Claimed)
                        return false;
                return true;
            }
        }

        public bool IsOverfilled => _assignedAgents.Count > _targetCleanPositions.Count;

        public List<ISquadMember> AssignedAgents => _assignedAgents;

        public bool IsClean => _targetCleanPositions.Count <= 0;
    }
}
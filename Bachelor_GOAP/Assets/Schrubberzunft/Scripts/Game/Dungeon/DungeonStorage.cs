﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Schrubber.Item;

namespace Schrubber.Game.Dungeon
{
    public class DungeonStorage : MonoBehaviour
    {
        public static DungeonStorage I;

        private void Awake()
        {
            I = this;
            Inventory = new Inventory();
            Pos = transform.position;
        }

        /// <summary>
        /// Check if this storage is missing some items and a refill is needed
        /// </summary>
        /// <returns></returns>
        public bool IsFull()
        {
            return Inventory.HasEnoughResources(TargetItems, out var a);
        }

        /// <summary>
        /// Get a recipe for all items, that need to be stored in this, so <see cref="TargetItems"/> is reached
        /// </summary>
        /// <returns></returns>
        public Recipe GetCurrentlyNeededItems()
        {
            List<KeyValuePair<EItem, int>> neededItems;
            if (Inventory.HasEnoughResources(TargetItems, out neededItems))
                return new Recipe();
            else
            {
                Recipe result = new Recipe();
                foreach(KeyValuePair<EItem, int> kvp in neededItems)
                    result.AddItem(kvp);
                return result;
            }
        }

        public Inventory Inventory { private set; get; }

        /// <summary>
        /// All items that need to be in this storage in order to start a cleanSquad
        /// </summary>
        public static Recipe TargetItems => new Recipe(new KeyValuePair<EItem, int>(EItem.Tool_Broom, 2)); // NO need for food
                                                                                                           //new KeyValuePair<EItem, int>(EItem.Tool_Broom, 2),
                                                                                                           //new KeyValuePair<EItem, int>(EItem.Resource_Food, 3));
#warning dungeonstorage TargetItems No need for food

        public Vector3 Pos { private set; get; }
    }
}
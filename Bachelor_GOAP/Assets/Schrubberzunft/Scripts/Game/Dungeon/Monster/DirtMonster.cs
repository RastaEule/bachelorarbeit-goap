﻿using Schrubber.GOAP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Schrubber.Game
{
    public class DirtMonster : MonoBehaviour
    {
        [SerializeField]
        private float _searchRadius = 5f;

        /// <summary>
        /// Distance between currentpos and target pos that is considered to have reached the targetpos
        /// </summary>
        [SerializeField]
        [Tooltip("Distance between currentpos and target pos that is considered to have reached the targetpos")]
        private float _destReachedThreshold = 1.5f;

        private NavMeshAgent _navAgent = null;
        private ParticleSystem _particleSystem = null;
        private UnityEngine.UI.Slider _hpSlider = null;

        [SerializeField]
        private float _updateInterval = 0.4f;
        private float _lastUpdateTime = 0f;

        [SerializeField]
        private float _attackInterval = 0.75f;
        private float _lastAttackTime = 0f;

        private bool _hasTarget = false;
        private bool _isAttacking = false;

        private Blackboard _targetAgent = null;

        private void Awake()
        {
            _navAgent = GetComponent<NavMeshAgent>();
            _particleSystem = GetComponentInChildren<ParticleSystem>();
            _hpSlider = GetComponentInChildren<UnityEngine.UI.Slider>();
            _hpSlider.value = 1;
        }

        private void Update()
        {
            if (!IsDead)
            {
                Pos = transform.position;
                if (Time.time - _lastUpdateTime > _updateInterval) // Select target and move towards it
                {
                    if (this.GetNearestAgent(out Blackboard targetAgent)) // Search target
                    {
                        // Search for new target
                        if (targetAgent != _targetAgent && // Only notify agent on change
                            targetAgent.TargetByDirtMonster(this)) // Notify agent, that he has been targeted by dirt monster
                        {
                            _targetAgent = targetAgent;
                            _hasTarget = true;
                        }
                        if (_targetAgent != null)
                        {
                            // Move to and attack target
                            _navAgent.SetDestination(_targetAgent.transform.position);
                            _isAttacking = Vector3.Distance(transform.position, _targetAgent.transform.position) < _destReachedThreshold;
                        }
                    }
                    else // No target found - just stop everything
                    {
                        _hasTarget = false;
                        _isAttacking = false;
                        _particleSystem.Stop();
                        _targetAgent = null;
                        _navAgent.SetDestination(transform.position);
                    }
                    _lastUpdateTime = Time.time;
                }
                if (_isAttacking &&
                    Time.time - _lastAttackTime > _attackInterval) // Attack
                {
                    _targetAgent.AttackByMonster(1);
                    _particleSystem.Play();
                    _lastAttackTime = Time.time;
                }
            }
        }

        private bool GetNearestAgent(out Blackboard nearestAgent)
        {
            // Search for agents in radius
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _searchRadius);
            List<Blackboard> nearbyAgents = new List<Blackboard>();
            foreach (Collider c in hitColliders)
            {
                Blackboard b = c.transform.parent.GetComponentInChildren<Blackboard>();
                if (b != null)
                    nearbyAgents.Add(b);
            }
            // Get nearest agent
            nearestAgent = nearbyAgents.Count > 0 ? nearbyAgents[0] : null;
            if (nearbyAgents.Count > 1)
            {
                for (int i = 1; i < nearbyAgents.Count; i++)
                {
                    if (Vector3.Distance(transform.position, nearbyAgents[i].transform.position) < Vector3.Distance(transform.position, nearestAgent.transform.position))
                        nearestAgent = nearbyAgents[i];
                }
            }
            return nearestAgent != null;
        }

        public void Attack(int damage)
        {
            CurrentHP -= damage;
            _hpSlider.value = (float)CurrentHP / (float)MaxHP;
            if (CurrentHP <= 0)
            {
                this.IsDead = true;
                gameObject.SetActive(false);
            }
        }

        public bool IsDead { private set; get; } = false;

        public int CurrentHP { private set; get; } = MaxHP;

        public static int MaxHP => 5;

        public Vector3 Pos { private set; get; } = Vector3.zero;

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, _searchRadius);
        }
    }
}
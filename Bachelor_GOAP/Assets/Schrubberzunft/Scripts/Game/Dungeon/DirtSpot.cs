﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Schrubber.Game.Dungeon
{
    /// <summary>
    /// This is a dirtspot that needs to be cleaned
    /// </summary>
    public class DirtSpot : MonoBehaviour
    {
        private Vector3 _position = Vector3.zero;
        private DungeonRoom _parentRoom = null;

        private void Awake()
        {
            _position = transform.position;
        }

        public void Init(DungeonRoom parentRoom)
        {
            _parentRoom = parentRoom;
        }

        /// <summary>
        /// Clean this spot
        /// </summary>
        public void Clean()
        {
            _parentRoom.RemoveSpot(this);
            Destroy(this.gameObject);
        }

        /// <summary>
        /// There is a schrubber that wants to clean this spot. If so, don't assign another schrubber for this spot
        /// </summary>
        public object ClaimedBy { get; set; } = null;

        public bool Claimed => ClaimedBy != null;

        public Vector3 Pos => _position;
    }
}
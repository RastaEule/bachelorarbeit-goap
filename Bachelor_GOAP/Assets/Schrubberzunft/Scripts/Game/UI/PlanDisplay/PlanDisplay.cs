﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GOAP.Core;

namespace Schrubber.Game
{
    public class PlanDisplay : MonoBehaviour
    {
        [SerializeField]
        private Text _textCurrentGoal = null;
        [SerializeField]
        private Transform _actionParent = null;
        [SerializeField]
        private GameObject _planActionEntryPrefab = null;

        [SerializeField]
        private float _updateInterval = 0.5f;
        private bool _isUpdating = false;
        private float _lastUpdateTime = 0;

        private GOAP_PlanRunner _targetPlanRunner = null;

        void Update()
        {
            if (_isUpdating &&
                Time.time - _lastUpdateTime >= _updateInterval)
            {
                GOAP_Plan plan = _targetPlanRunner.Plan;
                if (plan != null)
                {
                    _textCurrentGoal.text = plan.Goal.ToString();

                    foreach (Transform child in _actionParent)
                        Destroy(child.gameObject);

                    List<string> planStrings = new List<string>();
                    planStrings.Add(CleanActionName(_targetPlanRunner.CurrentActionName));
                    foreach (IGOAP_Action a in plan.Plan)
                        planStrings.Add(CleanActionName(a.ToString()));

                    float heightPerEntry = 1f / 4f;
                    int entryCount = 0;
                    for (int i = 0; i < planStrings.Count; i++)
                    {
                        GameObject go = Instantiate(_planActionEntryPrefab, _actionParent);
                        Text txt = go.GetComponent<Text>();
                        txt.text = planStrings[i];
                        RectTransform rect = go.GetComponent<RectTransform>();

                        float amax = 1f - (heightPerEntry * entryCount);
                        float amin = amax - heightPerEntry;
                        rect.anchorMin = new Vector2(rect.anchorMin.x, amin);
                        rect.anchorMax = new Vector2(rect.anchorMax.x, amax);
                        rect.sizeDelta = Vector2.zero;

                        entryCount++;
                    }
                }
                _lastUpdateTime = Time.time;
            }
        }

        private static string CleanActionName(string actionName)
        {
            if (actionName.StartsWith("GOAP_Action", System.StringComparison.InvariantCultureIgnoreCase))
            {
                int preLength = "goap_Action_".Length;
                return actionName.Substring(preLength, actionName.Length - preLength);
            }
            return actionName;
        }

        public void DisplayInformation(GOAP_PlanRunner planRunner)
        {
            this.gameObject.SetActive(true);
            _targetPlanRunner = planRunner;
            _isUpdating = true;

            GetComponentInChildren<ScrollRect>().verticalScrollbar.value = 1;
        }

        public void StopDisplay()
        {
            _isUpdating = false;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Schrubber.Item;

namespace Schrubber.Game
{
    public class UIDisplayBuyHouse : MonoBehaviour
    {
        [SerializeField]
        private Dropdown _dropdownHouseType = null;

        [SerializeField]
        private Button _buttonBuildHouse = null;

        [SerializeField]
        private Text _textCostAmount = null;

        [SerializeField]
        private int _buildingCost = 20;
        BuyHouse _targetBuyhouse = null;

        private void Awake()
        {
            this.UpdateCostDisplay(_buildingCost);
            List<Dropdown.OptionData> optionData = new List<Dropdown.OptionData>();

            foreach (EBuildingTypes buildingType in (EBuildingTypes[])Enum.GetValues(typeof(EBuildingTypes))) // The cast to array is not necessary but makes the code 0.5ns faster
                if (buildingType != EBuildingTypes.None)
                    optionData.Add(new Dropdown.OptionData(buildingType.ToString()));

            _dropdownHouseType.AddOptions(optionData);
            _dropdownHouseType.onValueChanged.AddListener(this.OnDropdownValueChanged);
            _buttonBuildHouse.onClick.AddListener(this.OnBuyButtonClicked);
        }

        private void OnDropdownValueChanged(int arg0)
        {
            // Update price based on other dropdown selection
            this.UpdateCostDisplay(_buildingCost);
            this.CheckPlayerMoney();
        }

        private void CheckPlayerMoney()
        {
            _buttonBuildHouse.interactable = GameManager.I.Player.HasEnoughMoney(2);
        }

        private void UpdateCostDisplay(int cost)
        {
            _textCostAmount.text = cost.ToString().PadLeft(2, '0');
        }

        private void OnBuyButtonClicked()
        {
            EBuildingTypes building = GetBuildingTypeFromString(_dropdownHouseType.captionText.text);
            if (building != EBuildingTypes.None &&
                GameManager.I.Player.TryBuy(_buildingCost))
            {
                // Mapmanager: Build this house
                _targetBuyhouse.Buy(building);
                this.StopDisplay();
            }
        }

        public void DisplayInformation(BuyHouse buyHouse)
        {
            _targetBuyhouse = buyHouse;
            this.gameObject.SetActive(true);
            this.CheckPlayerMoney();
        }

        public void StopDisplay()
        {
            this.gameObject.SetActive(false);
        }

        private static EBuildingTypes GetBuildingTypeFromString(string str)
        {
            foreach (EBuildingTypes buildingType in (EBuildingTypes[])Enum.GetValues(typeof(EBuildingTypes)))
                if (buildingType != EBuildingTypes.None &&
                    buildingType.ToString().Equals(str))
                {
                    return buildingType;
                }
            return EBuildingTypes.None;
        }
    }
}
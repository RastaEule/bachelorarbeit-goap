﻿using UnityEngine;
using Schrubber.Item;

namespace Schrubber.Game
{
    public class UIDisplayStorage : MonoBehaviour
    {
        [SerializeField]
        private InventoryList _inventoryDisplay = null;

        public void DisplayInformation(Storage storage)
        {
            this.gameObject.SetActive(true);
            _inventoryDisplay.DisplayInformation(storage.Inventory);
        }

        public void StopDisplay()
        {
            _inventoryDisplay.StopDisplay();
        }
    }
}
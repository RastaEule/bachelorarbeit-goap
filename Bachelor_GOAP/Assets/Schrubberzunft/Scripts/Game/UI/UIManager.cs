﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Schrubber.Item;
using Schrubber.GOAP;
using Schrubber.Game.Dungeon;

namespace Schrubber.Game
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private Text _currentMoneyText = null;

        private UIDisplayAgent _displayAgent = null;
        private UIDisplayBuyHouse _displayBuyhouse = null;
        private UIDisplayDungeonStorage _displayDungeonStorage = null;
        private UIDisplayStorage _displayStorage = null;
        private UIDisplayTownCenter _displayTowncenter = null;

        private void Awake()
        {
            _displayAgent = GetComponentInChildren<UIDisplayAgent>();
            _displayAgent.gameObject.SetActive(false);
            _displayBuyhouse = GetComponentInChildren<UIDisplayBuyHouse>();
            _displayBuyhouse.gameObject.SetActive(false);
            _displayDungeonStorage = GetComponentInChildren<UIDisplayDungeonStorage>();
            _displayDungeonStorage.gameObject.SetActive(false);
            _displayStorage = GetComponentInChildren<UIDisplayStorage>();
            _displayStorage.gameObject.SetActive(false);
            _displayTowncenter = GetComponentInChildren<UIDisplayTownCenter>();
            _displayTowncenter.gameObject.SetActive(false);
        }

        public void UpdateCurrentMoney(int money)
        {
            _currentMoneyText.text = "Money: " + money.ToString().PadLeft(3, '0');
        }

        public void DisplayInformation(Transform t)
        {
            Schrubber_Agent agent = t.GetComponent<Schrubber_Agent>();
            BuyHouse buyHouse = t.GetComponent<BuyHouse>();
            DungeonStorage dungeonStorage = t.GetComponent<DungeonStorage>();
            Storage storage = t.GetComponent<Storage>();
            TownCenter towncenter = t.GetComponent<TownCenter>();

            if (agent != null)
            {
                _displayAgent.DisplayInformation(agent);
            }
            else if (buyHouse != null)
            {
                _displayBuyhouse.DisplayInformation(buyHouse);
            }
            else if (dungeonStorage != null)
            {
                _displayDungeonStorage.DisplayInformation(dungeonStorage);
            }
            else if (storage != null)
            {
                _displayStorage.DisplayInformation(storage);
            }
            else if (towncenter != null)
            {
                _displayTowncenter.DisplayInformation(towncenter);
            }
        }

        public void StopDisplayInformation()
        {
            _displayAgent.StopDisplay();
            _displayAgent.gameObject.SetActive(false);
            _displayBuyhouse.StopDisplay();
            _displayBuyhouse.gameObject.SetActive(false);
            _displayDungeonStorage.StopDisplay();
            _displayDungeonStorage.gameObject.SetActive(false);
            _displayStorage.StopDisplay();
            _displayStorage.gameObject.SetActive(false);
            _displayTowncenter.StopDisplay();
            _displayTowncenter.gameObject.SetActive(false);
        }
    }
}
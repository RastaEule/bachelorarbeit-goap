﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Schrubber.Item;

namespace Schrubber.Game
{
    public class InventoryList : MonoBehaviour
    {
        [SerializeField]
        private GameObject _entryGO = null;

        /// <summary>
        /// The parent transform for all entries
        /// </summary>
        [SerializeField]
        private Transform _entryParent = null;

        [SerializeField]
        private float _updateInterval = 0.5f;
        private bool _isUpdating = false;
        private float _lastUpdateTime = 0;

        private List<InventoryEntry> _entries = null;

        private Inventory _targetInventory = null;

        private void Update()
        {
            if (_isUpdating &&
                Time.time - _lastUpdateTime >= _updateInterval)
            {
                foreach (KeyValuePair<Schrubber.Item.EItem, List<Schrubber.Item.Item>> kvp in _targetInventory.InternalInventory)
                {
                    InventoryEntry e = _entries.First(x => x.ItemType == kvp.Key);
                    if (e != null)
                        e.Count = kvp.Value.Count;
                }

                _lastUpdateTime = Time.time;
            }
        }

        private void Init()
        {
            foreach (Transform child in _entryParent)
                Destroy(child.gameObject);

            _entries = new List<InventoryEntry>();
            float heightPerEntry = 1f / 8f;

            List<KeyValuePair<EItem, List<Item.Item>>> types = new List<KeyValuePair<EItem, List<Item.Item>>>();
            foreach (KeyValuePair<Schrubber.Item.EItem, List<Schrubber.Item.Item>> kvp in _targetInventory.InternalInventory)
                types.Add(kvp);

            for (int i = 0; i < types.Count; i++)
            {
                GameObject go = Instantiate(_entryGO, _entryParent);
                InventoryEntry entry = go.GetComponent<InventoryEntry>();
                if (entry != null)
                {
                    entry.Init(this);
                    entry.ItemType = types[i].Key;
                    entry.Count = types[i].Value.Count;
                    entry.Text = types[i].Key.ToString();

                    float amax = 1f - (heightPerEntry * _entries.Count);
                    float amin = amax - heightPerEntry;
                    entry.Rect.anchorMin = new Vector2(entry.Rect.anchorMin.x, amin);
                    entry.Rect.anchorMax = new Vector2(entry.Rect.anchorMax.x, amax);
                    entry.Rect.sizeDelta = Vector2.zero;
                    _entries.Add(entry);
                }
            }
        }

        public void AddItem(EItem item)
        {
            _targetInventory.AddItem(Utils.GetItemInstance(item));
        }

        public void RemoveItem(EItem item)
        {
            _targetInventory.TakeItem(item);
        }

        public void DisplayInformation(Inventory inventory)
        {
            this.gameObject.SetActive(true);
            _targetInventory = inventory;
            this.Init();
            _isUpdating = true;
        }

        public void StopDisplay()
        {
            _isUpdating = false;
        }
    }
}
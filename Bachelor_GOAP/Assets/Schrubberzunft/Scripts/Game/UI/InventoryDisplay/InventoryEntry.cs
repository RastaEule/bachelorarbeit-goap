﻿using UnityEngine;
using Schrubber.Item;
using UnityEngine.UI;

namespace Schrubber.Game
{
    public class InventoryEntry : MonoBehaviour
    {
        [SerializeField]
        private Button _btnAddItem = null;
        [SerializeField]
        private Button _btnRemoveItem = null;
        [SerializeField]
        private Text _txtItemName = null;
        [SerializeField]
        private Text _txtItemCount = null;

        private RectTransform _rectTransform = null;
        private InventoryList _parentList = null;

        private int _count = 0;

        private void Awake()
        {
            _btnAddItem.onClick.AddListener(this.OnAddItemClicked);
            _btnRemoveItem.onClick.AddListener(this.OnRemoveItemClicked);
        }

        private void OnAddItemClicked()
        {
            _parentList.AddItem(ItemType);
        }

        private void OnRemoveItemClicked()
        {
            _parentList.RemoveItem(ItemType);
        }

        public void Init(InventoryList parentList)
        {
            _parentList = parentList;
        }

        public string Text
        {
            get { return _txtItemName.text; }
            set { _txtItemName.text = value; }
        }

        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                _txtItemCount.text = _count.ToString().PadLeft(2, '0');
            }
        }

        public EItem ItemType { set; get; } = EItem.Nothing;

        public RectTransform Rect
        {
            get
            {
                if (_rectTransform == null)
                    _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }
    }
}
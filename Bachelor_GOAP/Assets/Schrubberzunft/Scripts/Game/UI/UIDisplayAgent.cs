﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GOAP.Core;
using Schrubber.GOAP;

namespace Schrubber.Game
{
    public class UIDisplayAgent : MonoBehaviour
    {
        [SerializeField]
        private InventoryList _inventoryDisplay = null;
        [SerializeField]
        private PlanDisplay _planDisplay = null;

        [SerializeField]
        private Text _textCurrentSquad = null;
        [SerializeField]
        private Slider _sliderHP = null;
        [SerializeField]
        private Slider _sliderOverall = null;
        [SerializeField]
        private Slider _sliderHunger = null;
        [SerializeField]
        private Slider _sliderThirst = null;
        [SerializeField]
        private Slider _sliderHygiene = null;

        [SerializeField]
        private float _updateInterval = 0.5f;
        private bool _isUpdating = false;
        private float _lastUpdateTime = 0;

        private Schrubber_Agent _targetAgent = null;

        private void Update()
        {
            if (_isUpdating &&
                Time.time - _lastUpdateTime >= _updateInterval)
            {
                // HP
                float valueHP = (float)_targetAgent.Blackboard.HealthPoints / (float)Blackboard.MaxHealthPoints;
                _sliderHP.value = valueHP;

                // Current Squad
                string squad = "None";
                switch(_targetAgent.CurrentSquad.SquadType)
                {
                    case EGoalType.Base:
                        squad = "Base";
                        break;
                    case EGoalType.Clean:
                        squad = "Clean";
                        break;
                }
                _textCurrentSquad.text = squad;

                // Personal goal priorities
                float valueHunger = -1f;
                float valueThirst = -1f;
                float valueHygiene = -1f;
                float valueOverall = _targetAgent.GetPersonalUtility();

                List<IGOAP_Goal> goals = _targetAgent.GetGoals();
                foreach(IGOAP_Goal g in goals)
                {
                    if (g is SchrubberGoal)
                    {
                        SchrubberGoal sg = g as SchrubberGoal;
                        if (sg.GoalType == EGoalType.Personal &&
                            sg is GoalIncreasingPriority)
                        {
                            GoalIncreasingPriority gip = sg as GoalIncreasingPriority;

                            if (sg.GetType() == typeof(GoalEatFood))
                                valueHunger = GetPercentage(gip.PriorityMinMax, g.Priority);
                            else if (sg.GetType() == typeof(GoalDrink))
                                valueThirst = GetPercentage(gip.PriorityMinMax, g.Priority);
                            else if (sg.GetType() == typeof(GoalHygiene))
                                valueHygiene = GetPercentage(gip.PriorityMinMax, g.Priority);
                        }
                    }
                    if (valueHunger > 0 && valueThirst > 0 && valueHygiene > 0) // Done
                        break;
                }
                _sliderOverall.value = 1 - valueOverall;
                _sliderHunger.value = valueHunger;
                _sliderThirst.value = valueThirst;
                _sliderHygiene.value = valueHygiene;

                _lastUpdateTime = Time.time;
            }
        }

        private static float GetPercentage(Vector2 minMax, float currentValue)
        {
            return minMax.y - ((currentValue - minMax.x) / (minMax.y - minMax.x));
        }

        public void DisplayInformation(Schrubber_Agent agent)
        {
            _targetAgent = agent;
            this.gameObject.SetActive(true);
            _inventoryDisplay.DisplayInformation(agent.Blackboard.Inventory);
            _planDisplay.DisplayInformation(agent.Planrunner);

            _isUpdating = true;
        }

        public void StopDisplay()
        {
            _isUpdating = false;

            _inventoryDisplay.StopDisplay();
            _planDisplay.StopDisplay();
            this.gameObject.SetActive(false);
        }
    }
}
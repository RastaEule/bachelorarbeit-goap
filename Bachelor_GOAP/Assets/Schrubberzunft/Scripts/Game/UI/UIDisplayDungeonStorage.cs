﻿using UnityEngine;
using Schrubber.Game.Dungeon;

namespace Schrubber.Game
{
    public class UIDisplayDungeonStorage : MonoBehaviour
    {
        [SerializeField]
        private InventoryList _inventoryDisplay = null;

        public void DisplayInformation(DungeonStorage dungeonStorage)
        {
            this.gameObject.SetActive(true);
            _inventoryDisplay.DisplayInformation(dungeonStorage.Inventory);
        }

        public void StopDisplay()
        {
            _inventoryDisplay.StopDisplay();
        }
    }
}
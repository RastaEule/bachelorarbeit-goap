﻿using UnityEngine;
using UnityEngine.UI;

namespace Schrubber.Game
{
    public class UIDisplayTownCenter : MonoBehaviour
    {
        [SerializeField]
        private Button _buttonSpawnAgent = null;

        private TownCenter _targetTownCenter;

        private void Awake()
        {
            _buttonSpawnAgent.onClick.AddListener(this.OnSpawnButtonClicked);
        }

        public void DisplayInformation(TownCenter townCenter)
        {
            this.gameObject.SetActive(true);
            _targetTownCenter = townCenter;
        }

        public void StopDisplay()
        {
        }

        private void OnSpawnButtonClicked()
        {
            _targetTownCenter.SpawnAgent();
        }
    }
}
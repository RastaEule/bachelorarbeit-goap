﻿using UnityEngine;

namespace Schrubber.Game
{
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private int _maxMoney = 999;

        private int _currentMoney = 50;

        private void Awake()
        {
            GameManager.I.UIManager.UpdateCurrentMoney(_currentMoney);
        }

        /// <summary>
        /// Try to buy something with a given cost
        /// </summary>
        public bool TryBuy(int cost)
        {
            if (_currentMoney < cost) // not enough money
                return false;
            else
            {
                _currentMoney -= cost;
                GameManager.I.UIManager.UpdateCurrentMoney(_currentMoney);
                return true;
            }
        }

        public bool HasEnoughMoney(int cost)
        {
            return _currentMoney >= cost;
        }

        public void AddMoney(int amount)
        {
            _currentMoney += amount;
            if (_currentMoney > _maxMoney)
                _currentMoney = _maxMoney;
            GameManager.I.UIManager.UpdateCurrentMoney(_currentMoney);
        }

        public int CurrentMoney => _currentMoney;
    }
}
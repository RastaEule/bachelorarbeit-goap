﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GOAP.Core;

namespace Schrubber.Game
{
    public class RendezvousPositionManager : PositionManager
    {
        private void Awake()
        {
            foreach (Transform t in transform)
                base.AddTargetPos(t);
        }

        private List<RendezVouzPoint> GetNearestRendezvousPoints(Vector3 fromPos)
        {
            List<Transform> transforms = base.GetNearestTransforms(fromPos);
            List<RendezVouzPoint> rendezvousPositions = new List<RendezVouzPoint>();
            foreach (Transform t in transforms)
                rendezvousPositions.Add(t.GetComponent<RendezVouzPoint>());
            return rendezvousPositions;
        }

        public RendezVouzPoint GetRendezvousPoint(Vector3 fromPos, IGOAP_Agent agentToClaim)
        {
            List<RendezVouzPoint> rendezvousPoints = this.GetNearestRendezvousPoints(fromPos);

            RendezVouzPoint alreadyClaimedPoint = rendezvousPoints.FirstOrDefault(x => x.Claimed && x.ClaimedAgent == agentToClaim);
            if (alreadyClaimedPoint == null) // This agent does not claim any point already
            {
                foreach (RendezVouzPoint pos in rendezvousPoints)
                {
                    if (!pos.Claimed) // Return a point that is not yet claimed
                        return pos;
                }
            }
            else // This agent already claims a point. Return this point
                return alreadyClaimedPoint;
            return null;
        }

        public void UnclaimAllRendezvousPoints()
        {
            List<RendezVouzPoint> rendezvousPoints = this.GetNearestRendezvousPoints(Vector3.zero);
            foreach (RendezVouzPoint pos in rendezvousPoints)
            {
                if (pos.Claimed) // Return a point that is not yet claimed
                    pos.Unclaim();
            }
        }
    }
}
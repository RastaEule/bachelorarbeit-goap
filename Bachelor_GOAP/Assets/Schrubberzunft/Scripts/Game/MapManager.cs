﻿using UnityEngine;
using Schrubber.Game;

namespace Schrubber.Map
{
    public class MapManager : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField]
        private GameObject _bathPrefab = null;
        [SerializeField]
        private GameObject _forestPrefab = null;
        [SerializeField]
        private GameObject _fountainPrefab = null;
        [SerializeField]
        private GameObject _kitchenPrefab = null;
        [SerializeField]
        private GameObject _lakePrefab = null;
        [SerializeField]
        private GameObject _quarryPrefab = null;
        [SerializeField]
        private GameObject _workshopPrefab = null;

        [Header("Parent Transforms")]
        [SerializeField]
        private Transform _bathParent = null;
        [SerializeField]
        private Transform _forestParent = null;
        [SerializeField]
        private Transform _fountainParent = null;
        [SerializeField]
        private Transform _kitchenParent = null;
        [SerializeField]
        private Transform _lakeParent = null;
        [SerializeField]
        private Transform _quarryParent = null;
        [SerializeField]
        private Transform _storageParent = null;
        [SerializeField]
        private Transform _workshopParent = null;

        private void Awake()
        {
            // Fill Position managers
            foreach (Transform t in _bathParent)
                BathPositions.I.AddTargetPos(t);
            foreach (Transform t in _forestParent)
                ForestPositions.I.AddTargetPos(t);
            foreach (Transform t in _fountainParent)
                FountainPositions.I.AddTargetPos(t);
            foreach (Transform t in _kitchenParent)
                KitchenPositions.I.AddTargetPos(t);
            foreach (Transform t in _lakeParent)
                LakePositions.I.AddTargetPos(t);
            foreach (Transform t in _quarryParent)
                QuarryPositions.I.AddTargetPos(t);
            foreach (Transform t in _storageParent)
                StoragePositions.I.AddTargetPos(t);
            foreach (Transform t in _workshopParent)
                WorkshopPositions.I.AddTargetPos(t);
        }

        public void InstantiateBuilding(EBuildingTypes type, Vector3 targetPos)
        {
            GameObject resultGO = null;
            switch(type)
            {
                case EBuildingTypes.Bath:
                    resultGO = Instantiate(_bathPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_bathParent);
                    BathPositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Forest:
                    resultGO = Instantiate(_forestPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_forestParent);
                    ForestPositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Fountain:
                    resultGO = Instantiate(_fountainPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_fountainParent);
                    FountainPositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Kitchen:
                    resultGO = Instantiate(_kitchenPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_kitchenParent);
                    KitchenPositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Lake:
                    resultGO = Instantiate(_lakePrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_lakeParent);
                    LakePositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Quarry:
                    resultGO = Instantiate(_quarryPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_quarryParent);
                    QuarryPositions.I.AddTargetPos(resultGO.transform);
                    break;
                case EBuildingTypes.Workshop:
                    resultGO = Instantiate(_workshopPrefab);
                    resultGO.transform.position = targetPos;
                    resultGO.transform.SetParent(_workshopParent);
                    WorkshopPositions.I.AddTargetPos(resultGO.transform);
                    break;
            }
        }
    }
}
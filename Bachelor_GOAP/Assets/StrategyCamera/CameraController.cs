﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LZ_Camera
{
    public class CameraController : MonoBehaviour
    {
        /// <summary>
        /// The rig will follow ground height. This will handle elevation changes. <see cref="_groundMask"/> Needs to be set.
        /// </summary>
        [SerializeField]
        [Tooltip("The rig will follow ground height. This will handle elevation changes. groundMask Needs to be set.")]
        private bool _followGround = true;

        /// <summary>
        /// The camera-rig will always try to stay on the ground to handle elevation changes. To find the ground, it needs to know which layer it is on.
        /// You have to set your ground to the layer specified here. Only has an effect if <see cref="_followGround"/> is true.
        /// </summary>
        [SerializeField]
        [Tooltip("The camera-rig will always try to stay on the ground to handle elevation changes. To find the ground, it needs to know which layer it is on. You have to set your ground to the layer specified here. Only has an effect if followGround is true.")]
        private LayerMask _groundMask;

        /// <summary>
        /// Smoothing of the camera movements. Lower value = more smoothing
        /// </summary>
        [SerializeField]
        [Range(0, 25)]
        [Tooltip("Smoothing of the camera movements. Lower value = more smoothing")]
        private float _smoothFactor = 5f;

        [Header("Movement")]
        /// <summary>
        /// How fast the camera moves around normally
        /// </summary>
        [SerializeField]
        [Tooltip("How fast the camera moves around normally")]
        private float _moveSpeedNormal = 1f;

        /// <summary>
        /// How fast the camera moves around when the Button "CameraFastMove" is pressed. Needs to be setup in input settings.
        /// </summary>
        [SerializeField]
        [Tooltip("How fast the camera moves around when the Button \"CameraFastMove\" is pressed. Needs to be setup in input settings.")]
        private float _moveSpeedFast = 5f;

        [Header("Rotation")]
        [SerializeField]
        private float _rotationSpeed = 0.25f;

        /// <summary>
        /// Minimum and maximum value of the possible rotation around the pitch-rig.
        /// </summary>
        [SerializeField]
        [Tooltip("Minimum and maximum value of the possible rotation around the pitch-rig.")]
        private Vector2 _pitchConstraint = new Vector2(-40, 30);

        [Header("Zoom")]
        [SerializeField]
        private float _zoomSpeed = 20f;

        /// <summary>
        /// The zoom-value that the camera will zoom to, when an object to follow is selected. Should be between _zoomMin and _zoomMax values.
        /// If you don't want any zoom-change when following an object, use the value -1 for this.
        /// </summary>
        [SerializeField]
        [Tooltip("The zoom-value that the camera will zoom to, when an object to follow is selected. Should be between _zoomMin and _zoomMax values. If you don't want any zoom-change when following an object, use the value -1 for this.")]
        private float _zoomFollow = 100f;

        /// <summary>
        /// How far the player can zoom in (Distance from camera rig)
        /// </summary>
        [SerializeField]
        [Tooltip("How far the player can zoom in (Distance from camera rig)")]
        private float _zoomMin = 50f;

        /// <summary>
        /// How far the player can zoom out (Distance to camera rig)
        /// </summary>
        [SerializeField]
        [Tooltip("How far the player can zoom out (Distance to camera rig)")]
        private float _zoomMax = 5000f;

        [Header("Misc")]

        /// <summary>
        /// How far above the rig the script searches for ground
        /// </summary>
        [SerializeField]
        [Tooltip("How far above the rig the script searches for ground")]
        private float _groundCheckDistance = 1000f;

        [Header("Input")]
        /// <summary>
        /// Which axis to use for zooming. Needs to be setup in Project Settings -> Input
        /// </summary>
        [SerializeField]
        [Tooltip("Which axis to use for zooming. Needs to be setup in Project Settings -> Input")]
        private string _inputAxis_Zoom = "Mouse ScrollWheel";

        /// <summary>
        /// Which button to use for moving the camera around. Needs to be setup in Project Settings -> Input
        /// </summary>
        [SerializeField]
        [Tooltip("Which button to use for moving the camera around. Needs to be setup in Project Settings -> Input")]
        private string _inputButton_Move = "Fire2";

        /// <summary>
        /// Which button to use for rotating the camera around. Needs to be setup in Project Settings -> Input
        /// </summary>
        [SerializeField]
        [Tooltip("Which button to use for rotating the camera around. Needs to be setup in Project Settings -> Input")]
        private string _inputButton_Rotate = "Fire3";

        /// <summary>
        /// Which button to use to stop following an object. Needs to be setup in Project Settings -> Input
        /// </summary>
        [SerializeField]
        [Tooltip("Which button to use to stop following an object. Needs to be setup in Project Settings -> Input")]
        private string _inputButton_CancelFollow = "Cancel";

        /// <summary>
        /// Which button to use for highspeed-control. While this button is pressed, movement is faster. Needs to be setup in Project Settings -> Input
        /// </summary>
        [SerializeField]
        [Tooltip("Which button to use for highspeed-control. While this button is pressed, movement is faster. Needs to be setup in Project Settings -> Input")]
        private string _inputButton_Fast = "CameraFastMove";

        /// <summary>
        /// Is this camera currently following another object
        /// </summary>
        private bool _isFollowing = false;

        /// <summary>
        /// The object to follow
        /// </summary>
        private Transform _followObj = null;

        /// <summary>
        /// Is called when CameraController stops folling the <see cref="_followObj"/>
        /// </summary>
        private System.Action _onUnfollow = null;

        /// <summary>
        /// Is the current application focused? You can't zoom without focus
        /// </summary>
        private bool _applicationFocus = true;

        // References to rigs
        private Transform _yaw = null;
        private Transform _pitch = null;
        private Camera _camera = null;

        // Stuff for interpolating:
        private Vector3 _newPos = Vector3.zero;
        private Vector3 _mouseDragStart = Vector3.zero;
        private Vector3 _rotateStart = Vector3.zero;
        private Quaternion _newYaw = Quaternion.identity;
        private Quaternion _newPitch = Quaternion.identity;
        private float _newZoomValue = 100f;

        private void Awake()
        {
            _yaw = transform.Find("yaw");
            _pitch = _yaw.Find("pitch");
            _camera = _pitch.GetComponentInChildren<Camera>();

            // Set initial values
            _newPos = transform.position;
            _newYaw = _yaw.localRotation;
            _newPitch = _pitch.localRotation;

            if (_camera.orthographic)
                _newZoomValue = _camera.orthographicSize;
            else // Perspective camera
                _newZoomValue = Vector3.Distance(transform.position, _camera.transform.position);
        }

        private void Update()
        {
            if (Input.GetButtonDown(_inputButton_CancelFollow))
                this.StopFollowObject();

            // ---- Set new values ----
            this.HandleRotation();

            if (_isFollowing)
                _newPos = _followObj.position;
            this.HandleMovement();

            this.HandleZoom();
        }

        private void FixedUpdate()
        {
            // ---- Interpolate stuff ----
            float step = Time.smoothDeltaTime * _smoothFactor;
            // Movement
            transform.position = Vector3.Lerp(transform.position, _newPos, step);

            // Rotation
            _yaw.localRotation = Quaternion.Lerp(_yaw.localRotation, _newYaw, step);
            _pitch.localRotation = Quaternion.Lerp(_pitch.localRotation, _newPitch, step);

            // Zoom
            if (_camera.orthographic) // Ortographic camera only changes orthographicSize value
                _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _newZoomValue, step);
            else // Perspective camera changes camera pos in relation to rig position
                _camera.transform.position = Vector3.Lerp(_camera.transform.position, this.GetCameraZoomPosition(_newZoomValue), step);
        }

        private void HandleMovement()
        {
            if (_isFollowing)
            {
                if (Input.GetButton(_inputButton_Move)) // Player wants to move again -> stop following the current object
                    this.StopFollowObject();
                else
                    return; // Don't handle movement
            }

            // Handle mouse drag
            if (Input.GetButtonDown(_inputButton_Move))
            {
                _mouseDragStart = Input.mousePosition;

                Plane plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                float entry = 0;
                if (plane.Raycast(ray, out entry))
                    _mouseDragStart = ray.GetPoint(entry);
            }
            if (Input.GetButton(_inputButton_Move))
            {
                Plane plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                float entry = 0;
                if (plane.Raycast(ray, out entry))
                {
                    Vector3 dragCurrent = ray.GetPoint(entry);
                    float speed = _moveSpeedNormal;
                    if (Input.GetButton(_inputButton_Fast))
                        speed = _moveSpeedFast;

                    _newPos = transform.position + (_mouseDragStart - dragCurrent) * speed;
                }
            }

            // Handle elevation change
            // Cast a ray from the sky to the rigpoint
            if (_followGround)
            {
                RaycastHit hit;
                Vector3 origin = new Vector3(transform.position.x, transform.position.y + _groundCheckDistance, transform.position.z);
                if (Physics.Raycast(origin, Vector3.down, out hit, _groundCheckDistance * 2, _groundMask.value))
                    _newPos = new Vector3(_newPos.x, hit.point.y, _newPos.z);
            }
        }

        private void HandleRotation()
        {
            if (Input.GetButtonDown(_inputButton_Rotate))
            {
                _rotateStart = Input.mousePosition;
            }
            if (Input.GetButton(_inputButton_Rotate))
            {
                Vector3 rotateCurrent = Input.mousePosition;
                // Horizontal and vertical mouse movements on screen
                float h = _rotateStart.x - rotateCurrent.x;
                float v = _rotateStart.y - rotateCurrent.y;
                _rotateStart = rotateCurrent;

                _newYaw *= Quaternion.Euler(0, -h * _rotationSpeed, 0);
                 _newPitch *= Quaternion.Euler(v * _rotationSpeed, 0, 0);

                _newPitch = ClampRotationX(_newPitch, _pitchConstraint);
            }
        }

        private void HandleZoom()
        {
            if (_applicationFocus)
            {
                float input = Input.GetAxis(_inputAxis_Zoom) * _zoomSpeed * 100; // Multiply with 100 so the number in inspector is smaller
                if (input != 0)
                {
                    _newZoomValue -= input;

                    // Clamp
                    if (_newZoomValue < _zoomMin)
                        _newZoomValue = _zoomMin;
                    if (_newZoomValue > _zoomMax)
                        _newZoomValue = _zoomMax;
                }
            }
        }

        /// <summary>
        /// Returns the world-space position of the camera for a given zoom-value
        /// </summary>
        /// <param name="zoomvalue"></param>
        /// <returns></returns>
        private Vector3 GetCameraZoomPosition(float zoomvalue)
        {
            Vector3 dir = (transform.position - _camera.transform.position).normalized;
            Vector3 camPos = transform.position - dir * zoomvalue;
            return camPos;
        }

        /// <summary>
        /// Clamp the rotation of a quaternion around the x-Axis
        /// </summary>
        /// <param name="q">The quaternion to clamp</param>
        /// <param name="minMax">The minimum and maximum values</param>
        private static Quaternion ClampRotationX(Quaternion q, Vector2 minMax)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
            angleX = Mathf.Clamp(angleX, minMax.x, minMax.y);
            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);
            q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);

            float angleZ = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.z);
            q.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleZ);

            return q;
        }

        /// <summary>
        /// Follow a moving object around
        /// <param name="onUnFollow">Is called when the cameracontroller stops following</param>
        /// </summary>
        public void FollowObject(Transform obj, System.Action onUnfollow)
        {
            // If last followObject exists, this CameraController stops to follow it
            _onUnfollow?.Invoke();

            if (obj != null)
            {
                _followObj = obj;
                _onUnfollow = onUnfollow;
                _isFollowing = true;

                if (_zoomFollow > 0)
                    _newZoomValue = _zoomFollow;
            }
        }

        /// <summary>
        /// Stop following any objects
        /// </summary>
        public void StopFollowObject()
        {
            _followObj = null;
            _isFollowing = false;
            _onUnfollow();
        }

        private void OnDrawGizmosSelected()
        {
            if (_camera != null)
                Debug.DrawLine(transform.position, _camera.transform.position);
        }

        private void OnApplicationFocus(bool focus)
        {
            _applicationFocus = focus;
        }
    }
}
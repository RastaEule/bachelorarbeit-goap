﻿namespace Schrubber.GOAP
{
    public class GoalGetToolAxe : GoalIncreasingPriority
    {
        protected override void Awake()
        {
            base.Awake();
            base._name = "GetToolAxe";
            base._goalType = EGoalType.Base;

            base._goalState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Axe), true);
        }

        public override void OnPlanFinished()
        {
            base.OnPlanFinished();
            base._priority = 0f;
            // Reset this world state, so the goal can be repeated
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Axe), false);
        }

        public override void OnPlanFailed()
        {
            base.OnPlanFailed();
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Axe), false);
        }

        public override void Blacklist(bool byPlanner)
        {
            base.Blacklist(byPlanner);
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.ItemStored(SchrubberSymbols.Tool_Axe), false);
        }
    }
}
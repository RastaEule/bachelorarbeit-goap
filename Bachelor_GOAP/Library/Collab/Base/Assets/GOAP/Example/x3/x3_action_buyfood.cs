﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Unity;
using GOAP.Core;

namespace GOAP.Example
{
    public class x3_action_buyfood : GOAP_Action
    {
        [SerializeField]
        private List<Transform> _shops = null;
        private List<Vector3> _shopPositions = null;

        private Vector3 _targetPos = Vector3.zero;

        protected override void Awake()
        {
            base.Awake();
            base._effects.SetSymbol("food_available", true);
            _targetPos = _shops[0].position;

            _shopPositions = new List<Vector3>(_shops.Count);
            foreach (Transform t in _shops)
                _shopPositions.Add(t.position);
        }

        public override void GetPrecalculations(GOAP_State plannedState)
        {
            // Get closest shop
            _targetPos = _shopPositions[0];

            object atposobj;
            if (plannedState.State.TryGetValue("atPos", out atposobj))
            {
                Vector3 atpos = (Vector3)atposobj;
                if (atpos != null)
                {
                    for (int i = 1; i < _shopPositions.Count; i++)
                    {
                        if (Vector3.Distance(atpos, _shopPositions[i]) < Vector3.Distance(_targetPos, _shopPositions[i]))
                            _targetPos = _shopPositions[i];
                    }
                }
            }

            plannedState.SetSymbol("targetPos", _targetPos);
        }

        public override GOAP_State GetProceduralEffects(GOAP_State plannedState)
        {
            object targetPos = null;
            if (plannedState.State.ContainsKey("targetPos") && plannedState.State.TryGetValue("targetPos", out targetPos))
            {
                GOAP_State eff = base.GetProceduralEffects(plannedState);
                eff.SetSymbol("atPos", targetPos);
                return eff;
            }
            return base.GetProceduralEffects(plannedState);
        }

        public override float GetCost(GOAP_State plannedState)
        {
            object atposobj;
            object targetposobj;
            if (plannedState.State.TryGetValue("atPos", out atposobj) && plannedState.State.TryGetValue("targetPos", out targetposobj))
            {
                Vector3 atPos = (Vector3)atposobj;
                Vector3 targetPos = (Vector3)targetposobj;
                if (atPos != null && targetposobj != null)
                {
                    return base.GetCost(plannedState) + Vector3.Distance(atPos, targetPos);
                }
            }
            return base.GetCost(plannedState);
        }

        public override void Run(Action onFinished, Action onFailed, Core.GOAP_State worldState)
        {
            onFinished();
        }
    }
}

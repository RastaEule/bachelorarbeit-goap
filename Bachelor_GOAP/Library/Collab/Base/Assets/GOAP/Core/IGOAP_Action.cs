﻿using System;

namespace GOAP.Core
{
    public interface IGOAP_Action
    {
        /// <summary>
        /// Get dynamic preconditions of this action. They can vary at runtime based on other dependencies.
        /// </summary>
        /// <param name="plannedState">The currently planned worldstate. You can make this affect your preconditions. readonly!</param>
        GOAP_State GetDynamicPreconditions(GOAP_State plannedState);

        /// <summary>
        /// Get dynamic effects of this action. They can vary at runtime based on other dependencies.
        /// </summary>
        /// <param name="plannedState">The currently planned worldstate. You can make this affect your effects. readonly!</param>
        GOAP_State GetDynamicEffects(GOAP_State plannedState);

        /// <summary>
        /// Get static preconditions of this action. They cannot change.
        /// </summary>
        GOAP_State GetStaticPreconditions();

        /// <summary>
        /// Get static effects of this action. They cannot change.
        /// </summary>
        GOAP_State GetStaticEffects();

        /// <summary>
        /// Set symbols to the planned worldstate that can be used by other calculations. These symbols may contain information, that you can use in your other actions or calculations.
        /// Call order: GetPrecalculations -> GetEffects -> GetPreconditions
        /// Warning: They are directly applied to the Agents worldState when planning is finished. So you should not overwrite any critical stuff in here, just additional information.
        /// </summary>
        /// <param name="plannedState">The planned worldstate at this point of time.</param>
        void GetPrecalculations(GOAP_State plannedState);

        /// <summary>
        /// Cost of this action. Based on the worldstate before this action and the worldstate after this action
        /// </summary>
        /// <param name="plannedState">The currently planned worldstate. You can make this affect your cost. readonly!</param>
        float GetCost(GOAP_State plannedState);

        /// <summary>
        /// Is this action possible at all
        /// </summary>
        /// <param name="plannedState">The currently planned worldstate. You can make this affect your procedural precondition. readonly!</param>
        bool IsPossible(GOAP_State plannedState);

        /// <summary>
        /// Perform this action
        /// </summary>
        /// <param name="onFinished">Call this when the action has finished executing</param>
        /// <param name="onFailed">Call this when the execution of this action failed. This will invalidate the current plan.</param>
        /// <param name="worldState">You can make changes to the agents worldstate while performing the action if you want</param>
        void Run(Action onFinished, Action onFailed, GOAP_State worldState);
    }
}
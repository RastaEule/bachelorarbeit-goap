﻿using System;
using GOAP.Logging;

namespace GOAP.Core
{
    /// <summary>
    /// This class manages the execution of a plan
    /// </summary>
    public class GOAP_PlanRunner
    {
        private IGOAP_Agent _agent;
        private GOAP_Plan _currentPlan = null;
        private IGOAP_Action _currentAction = null;

        /// <summary>
        /// Plan execution should be cancelled
        /// </summary>
        private bool _planCancelRequest = false;

        public GOAP_PlanRunner(IGOAP_Agent agent)
        {
            _agent = agent;
        }

        public void StartPlanExecution(GOAP_Plan plan)
        {
            if (plan == null || plan.Plan.Count <= 0)
            {
                this.PlanExecutionFinished(); // No plan to execute
            }
            else
            {
                _currentPlan = plan;
                // Execute the first action
                this.OnActionFinished();
            }
        }

        /// <summary>
        /// Stop the execution of the current plan and request a new plan.
        /// </summary>
        public void StopPlanExecution()
        {
            if (_currentPlan == null) // No plan available, just generate a new plan
                this.PlanExecutionFinished();
            else
                _planCancelRequest = true; // Generate a new plan after the next action is completed
        }

        /// <summary>
        /// This plan is finished. Tell Agent to formulate a new plan.
        /// </summary>
        private void PlanExecutionFinished()
        {
            _planCancelRequest = false; // Reset cancel request
            _currentAction = null; // Reset current action

            _agent.GeneratePlan();
        }

        /// <summary>
        /// Current method finished. Execute the next method in the plan.
        /// </summary>
        private void OnActionFinished()
        {
            if (_currentAction != null)
            {
                // This action was finished
                // Apply effects to players WorldState
                _agent.CurrentState = _agent.CurrentState.Copy() + _currentAction.GetStaticEffects() + _currentAction.GetDynamicEffects(_agent.CurrentState);
            }

            if (_currentPlan == null)
            {
                GOAP_Logger.I.Log("Unexpected error in " + _agent.ToString() +": currentPlan in PlanRunner is null", EMessageType.PlanExecution, EMessageSeverity.Error);
                return;
            }

            if (_currentPlan.Plan.Count == 0 || _planCancelRequest)
            {
                if (_currentPlan.Plan.Count == 0) // Plan was executed correctly
                {
                    _currentPlan.Goal.OnPlanFinished(); // Only call this, if the plan was executed correctly
                }

                // Stop execution if there is no action to execute left or there was a request to stop the current plan execution
                this.PlanExecutionFinished();
            }
            else
            {
                // Execute the next action
                _currentAction = _currentPlan.Plan.Dequeue();
                _currentAction.GetPrecalculations(_agent.CurrentState);
                GOAP_Logger.I.Log("Performing " + _currentAction.ToString(), _agent, EMessageType.PlanExecution, EMessageSeverity.Info);

                // Check if this actions preconditions are still met
                if (_currentAction.IsPossible(_agent.CurrentState) &&
                    _agent.CurrentState.IsGoal(_currentAction.GetStaticPreconditions() + _currentAction.GetDynamicPreconditions(_agent.CurrentState)))
                {
                    _currentAction.Run(OnActionFinished, OnActionFailed, _agent.CurrentState);
                }
                else
                {
                    GOAP_Logger.I.Log(string.Format("Plan execution cannot continue: Preconditions of {0} aren't fulfilled anymore", _currentAction.ToString()), _agent, EMessageType.PlanExecution,EMessageSeverity.Warning);

                    _currentPlan.Goal.Blacklist(false);
                    this.PlanExecutionFinished();
                }
            }
        }

        /// <summary>
        /// Current method failed to execute. The whole plan can't be executed anymore. So tell agent to formulate a new plan.
        /// </summary>
        private void OnActionFailed()
        {
            if (_currentAction == null)
            {
                GOAP_Logger.I.Log("Unexpected error: PlanRunner currentAction is null", EMessageType.PlanExecution, EMessageSeverity.Error);
                return;
            }

            GOAP_Logger.I.Log(string.Format("Plan execution cannot continue: {0} failed", _currentAction.ToString()), _agent, EMessageType.PlanExecution, EMessageSeverity.Warning);

            _currentPlan.Goal.OnPlanFailed();

            _currentPlan.Goal.Blacklist(false);
            _currentPlan = null;
            this.PlanExecutionFinished();
        }

        public GOAP_Plan Plan => _currentPlan;

        public string CurrentActionName => _currentAction == null ? "null" : _currentAction.ToString();
    }
}
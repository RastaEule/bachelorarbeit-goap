﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GOAP.Core;
using System.Collections;

namespace GOAP.Unity
{
    public class GOAP_Goal : MonoBehaviour, IGOAP_Goal
    {
        [SerializeField]
        [Tooltip("The name of this goal")]
        protected string _name = "DefaultGoal";
        /// <summary>
        /// Is this goal possible at all
        /// </summary>
        [SerializeField]
        [Tooltip("Is this goal possible at all")]
        protected bool _isPossible = true;
        /// <summary>
        /// The priority of this goal
        /// </summary>
        [SerializeField][Tooltip("The priority of this goal")]
        protected float _priority = 0.5f;
        /// <summary>
        /// How long to blacklist this goal if no valid plan for it could be found
        /// </summary>
        [SerializeField][Tooltip("How long to blacklist this goal if no valid plan for it could be found")]
        protected float _blacklistTime = 18f;

        /// <summary>
        /// If the worldstate symbols are equal to this state, the goal is considered to be completed
        /// </summary>
        protected GOAP_State _goalState = new GOAP_State();

        /// <summary>
        /// The agent of this action
        /// </summary>
        protected IGOAP_Agent _agent = null;

        private bool _blacklisted = false;

        #region Unity

        protected virtual void Awake()
        {
            _goalState = new GOAP_State();
            _agent = GetComponent<IGOAP_Agent>();
        }

        #endregion

        #region IGOAP_Goal Members

        public float Priority { get { return _priority; } set { _priority = value; } }

        public virtual bool IsPossible
        {
            get { return _isPossible; }
            set { _isPossible = value; }
        }

        public virtual GOAP_State GetGoalState()
        {
            return _goalState;
        }

        public virtual void OnPlanFinished()
        {
        }

        public virtual void OnPlanFailed()
        {
        }

        public virtual void Blacklist()
        {
            StopAllCoroutines();
            StartCoroutine(this.BlacklistgoalSeconds(_blacklistTime));
        }

        public bool IsBlackListed => _blacklisted;

        #endregion

        /// <summary>
        /// Blacklist this goal for x seconds
        /// </summary>
        private IEnumerator BlacklistgoalSeconds(float seconds)
        {
            _blacklisted = true;
            yield return new WaitForSecondsRealtime(seconds);
            _blacklisted = false;
        }

        public override string ToString()
        {
            return "Goap_Goal " + _name;
        }
    }
}

﻿using UnityEngine;

namespace Schrubber.Game
{
    public class TreasureChest : MonoBehaviour
    {
        private int _currentOpenCalls = 0;

        private void Awake()
        {
            Pos = transform.position;
        }

        public void Open()
        {
            _currentOpenCalls++;

            if (_currentOpenCalls >= NeededAgents)
            {
                IsOpen = true;
                this.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// How many agents are needed to open this chest
        /// </summary>
        public int NeededAgents => 2;

        public Vector3 Pos { private set; get; } = Vector3.zero;

        public bool IsOpen { private set; get; } = false;
    }
}
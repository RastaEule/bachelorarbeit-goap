﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Schrubber
{
    public class PositionManager : MonoBehaviour
    {
        [SerializeField]
        protected List<Transform> _targetPositions = null;

        public virtual Vector3 GetNearestPos(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return Vector3.zero;

            Vector3 closest = _targetPositions[0].position;
            for (int i = 1; i < _targetPositions.Count; i++)
            {
                if (Vector3.Distance(fromPos, _targetPositions[i].position) < Vector3.Distance(fromPos, closest))
                    closest = _targetPositions[i].position;
            }
            return closest;
        }

        public Transform GetNearestTransform(Vector3 fromPos)
        {
            if (_targetPositions == null || _targetPositions.Count == 0)
                return null;

            Transform closest = _targetPositions[0];
            for (int i = 1; i < _targetPositions.Count; i++)
            {
                if (Vector3.Distance(fromPos, _targetPositions[i].position) < Vector3.Distance(fromPos, closest.position))
                    closest = _targetPositions[i];
            }
            return closest;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Schrubber.GOAP;
using Schrubber.Game;
using Schrubber.Game.Dungeon;

namespace Schrubber.Hierarchy
{
    public class CleanSquad : MonoBehaviour, ISquad
    {
        /// <summary>
        /// How many agents should be in this squad
        /// </summary>
        [SerializeField]
        [Tooltip("How many agents should be in this squad")]
        private int _targetAgentCount = 5;

        private Commander _commander;
        private bool _isActive = false;

        /// <summary>
        /// Was Rendezvous already succesfully executed
        /// </summary>
        private bool _rendezvousFinished = false;

        /// <summary>
        /// All agents in this squad
        /// </summary>
        private List<ISquadMember> _members;

        /// <summary>
        /// Information about all current orders
        /// </summary>
        private OrderStatus _orderState;

        private DirtMonster _targetDirtMonster = null;

        #region ISquad Members

        public void Init(Commander commander)
        {
            _commander = commander;
        }

        public void StartSquad(List<ISquadMember> members)
        {
            _orderState = new OrderStatus();
            _members = members;
            _members[0].Blackboard.IsDungeonStorageRefillAgent = true; // The first agent has to refill the dungeonStorage
            _rendezvousFinished = false;
            foreach (ISquadMember a in _members)
            {
                a.SetSquad(this);
                a.Blackboard.CanRefillAtDungeonStorage = false;
            }

            _isActive = true;
            this.StartSquadBehavior(EOrderType.Rendezvous);
        }

        public void StopSquad()
        {
            _isActive = false;
            _rendezvousFinished = false;

            while (_members.Count > 0)
            {
                GameManager.I.DungeonManager.UnAssignAgent(_members[0]);
                this.SquadMemberLeft(_members[0]);
            }

            _commander.StopSquad(this.SquadType);
            _members = new List<ISquadMember>();
        }

        public void SquadMemberLeft(ISquadMember member)
        {
            member.Blackboard.IsDungeonStorageRefillAgent = false;
            GameManager.I.DungeonManager.UnAssignAgent(member);
            _orderState.RemoveSquadMember(member);
            _commander.ReassignAgentToBaseSquad(member);
            if (_members.Contains(member))
                _members.Remove(member);

            if (_isActive && this.SquadMembers.Count <= (float)this.TargetAgentCount / 2f)
                this.StopSquad();
        }

        public void AddAgent(ISquadMember member)
        {
            member.SetSquad(this);
            if (!_members.Contains(member))
                _members.Add(member);

            member.Blackboard.CanRefillAtDungeonStorage = _rendezvousFinished;
        }

        public void MemberNotification(ISquadMember member, ENotificationType type, object target)
        {
            if (!IsActive)
                return;

            switch(type)
            {
                case ENotificationType.AgentDead:
                    {
                        _commander.RemoveAgent((Schrubber_Agent)member);
                        break;
                    }
                case ENotificationType.SquadChangeBase:
                    {
                        if (this.SquadMembers.Count - 1 <= (float)this.TargetAgentCount / 2f)
                            this.StopSquad();
                        else
                        {
                            this.SquadMemberLeft(member);
                        }
                        break;
                    }
                case ENotificationType.TargetedByDirtMonster:
                    {
                        _targetDirtMonster = target as DirtMonster;
                        this.StartSquadBehavior(EOrderType.AttackEnemy);
                        break;
                    }
                case ENotificationType.TreasureChestSpotted:
                    {
                        TreasureChest chest = target as TreasureChest;
                        if (!_orderState.Exists(EOrderType.OpenTreasureChest) && // this order is currently not active
                            _members.Count >= chest.NeededAgents) // There are enough squad members in total
                        {
                            List<ISquadMember> closestMembers = _members.OrderBy(x => Vector3.Distance(chest.Pos, x.Blackboard.Pos)).ToList();
                            List<ISquadMember> chestAgents = new List<ISquadMember>();
                            chestAgents.Add(closestMembers[0]);
                            chestAgents.Add(closestMembers[1]);
                            GameManager.I.DungeonManager.UnAssignAgent(closestMembers[0]);
                            GameManager.I.DungeonManager.UnAssignAgent(closestMembers[1]);
                            _orderState.StartNewOrder(EOrderType.OpenTreasureChest, chestAgents);

                            foreach (ISquadMember a in chestAgents)
                                a.SetOrder(new Order(EOrderType.OpenTreasureChest, chest, this.OnOrderFinished, this.OnOrderFailed));
                        }
                        break;
                    }
            }
        }

        public List<ISquadMember> SquadMembers => _members;

        public EGoalType SquadType => EGoalType.Clean;

        public Transform Transform => transform;

        public bool IsActive => _isActive;

        public int TargetAgentCount
        {
            get { return _targetAgentCount; }
            set { _targetAgentCount = value; }
        }

        #endregion

        private void StartSquadBehavior(EOrderType type)
        {
            if (!IsActive)
                return;

            switch (type)
            {
                case EOrderType.AttackEnemy:
                    _orderState.StartNewOrder(type, _members);
                    foreach (ISquadMember a in _members)
                        a.SetOrder(new Order(EOrderType.AttackEnemy, _targetDirtMonster, this.OnOrderFinished, this.OnOrderFailed));
                    break;
                case EOrderType.Rendezvous:
                    {
                        _orderState.StartNewOrder(type, _members);
                        foreach (ISquadMember a in _members)
                            a.SetOrder(new Order(type, this.OnOrderFinished, this.OnOrderFailed));
                        break;
                    }
                case EOrderType.CleanRoom:
                    {
                        _orderState.StartNewOrder(type, _members);
                        GameManager.I.DungeonManager.ClearAssignments();
                        foreach(ISquadMember sm in _members)
                        {
                            DungeonRoom room = GameManager.I.DungeonManager.AssignAgentToRoom(sm);
                            sm.SetOrder(new Order(EOrderType.CleanRoom, room, OnOrderFinished, OnOrderFailed));
                        }
                        break;
                    }
            }
        }

        private void SquadBehaviorFinished(EOrderType type)
        {
            if (!IsActive)
                return;

            _orderState.StopOrder(type);
            switch (type)
            {
                case EOrderType.AttackEnemy:
                    this.StartSquadBehavior(EOrderType.CleanRoom);
                    break;
                case EOrderType.CleanRoom:
                    this.StopSquad();
                    break;
                case EOrderType.OpenTreasureChest:
                    this.StartSquadBehavior(EOrderType.CleanRoom);
                    break;
                case EOrderType.Rendezvous:
                    GameManager.I.DungeonManager.RendezvousPositionManager.UnclaimAllRendezvousPoints();
                    _rendezvousFinished = true;
                    foreach(ISquadMember sm in _members)
                        sm.Blackboard.CanRefillAtDungeonStorage = true;

                    this.StartSquadBehavior(EOrderType.CleanRoom);
                    break;
            }
        }

        private void OnOrderFinished(ISquadMember member, EOrderType orderType)
        {
            if (!IsActive)
                return;

            _orderState.AgentOrderFinished(orderType, member);

             if (orderType == EOrderType.CleanRoom)
            {
                if (GameManager.I.DungeonManager.IsClean)
                {
                    this.StopSquad();
                }
                else
                {
                    this.CleanRoomSingleAgent(member);
                }
            }
            else if (_orderState.IsFinished(orderType))
                this.SquadBehaviorFinished(orderType);
            else if (orderType == EOrderType.AttackEnemy)
            {
                this.CleanRoomSingleAgent(member);
            }
        }

        private void OnOrderFailed(ISquadMember member, EOrderType orderType)
        {
            if (!IsActive)
                return;

            switch (orderType)
            {
                case EOrderType.AttackEnemy:
                    _orderState.RemoveSquadMember(member);
                    this.CleanRoomSingleAgent(member);
                    break;
                case EOrderType.Rendezvous:
                    GameManager.I.DungeonManager.RendezvousPositionManager.UnclaimAllRendezvousPoints();
                    this.StopSquad();
                    break;
                default:
                    this.MemberNotification(member, ENotificationType.SquadChangeBase, null);
                    break;
            }
        }

        private void CleanRoomSingleAgent(ISquadMember member)
        {
            GameManager.I.DungeonManager.UnAssignAgent(member);
            DungeonRoom room = GameManager.I.DungeonManager.AssignAgentToRoom(member);
            _orderState.AddSquadMember(member, EOrderType.CleanRoom);
            member.SetOrder(new Order(EOrderType.CleanRoom, room, OnOrderFinished, OnOrderFailed));
        }
    }
}
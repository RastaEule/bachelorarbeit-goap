﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Schrubber.GOAP;
using Schrubber.Item;
using Schrubber.Game;
using Schrubber.Game.Dungeon;

namespace Schrubber.Hierarchy
{
    public class Commander : MonoBehaviour
    {
        /// <summary>
        /// How well an agents personal threshold has to be in order to be considered for a schrubberSquad
        /// </summary>
        [SerializeField][Tooltip("How well an agents personal threshold has to be in order to be considered for a schrubberSquad")]
        private float _schrubberSquadThreshold = 0.4f;

        /// <summary>
        /// Check if a new SchrubberSquad can be created each x seconds
        /// </summary>
        [SerializeField][Tooltip("Check if a new SchrubberSquad can be created each x seconds")]
        private float _cleanSquadCheckCreateInterval = 2f;

        /// <summary>
        /// Wait this many seconds after the last cleanSquad has stopped, until the commander tries to create a new cleanSquad
        /// </summary>
        [SerializeField][Tooltip("Wait this many seconds after the last cleanSquad has stopped, until the commander tries to create a new cleanSquad")]
        private float _cleanSquadCreateCooldown = 20f;
        private float _lastCleanSquadCheckCreate = 0;
        private float _lastCleanSquadStopTime = 0;

        private List<ISquadMember> _agents;
        private ISquad _cleanSquad;
        private ISquad _baseSquad;

        private void Awake()
        {
            List<Schrubber_Agent> agents = new List<Schrubber_Agent>(GetComponentsInChildren<Schrubber_Agent>());

            _agents = new List<ISquadMember>();
            for (int i = 0; i < agents.Count; i++)
            {
                agents[i].ID = i;
                _agents.Add(agents[i]);
                agents[i].Init(false);
            }

            _cleanSquad = GetComponentInChildren<CleanSquad>();
            _baseSquad = GetComponentInChildren<BaseSquad>();

            _cleanSquad.Init(this);
            _baseSquad.Init(this);
        }

        private void Start()
        {
            List<ISquadMember> mbrs = new List<ISquadMember>();
            foreach (ISquadMember member in _agents)
                mbrs.Add(member);

            _baseSquad.StartSquad(mbrs);
        }

        private void Update()
        {
            if (Time.time - _lastCleanSquadCheckCreate > _cleanSquadCheckCreateInterval &&
                (Time.time < _cleanSquadCreateCooldown || Time.time - _lastCleanSquadStopTime > _cleanSquadCreateCooldown))
            {
                this.CheckSchrubberSquadCreate();
                _lastCleanSquadCheckCreate = Time.time;
            }
        }

        private void CheckSchrubberSquadCreate()
        {
            if (!_cleanSquad.IsActive &&
                !GameManager.I.DungeonManager.IsClean)
            {
                this.CreateSchrubberSquad();
            }
        }

        /// <summary>
        /// Creates and starts a new CleanSquad.
        /// </summary>
        /// <returns>true, if successful</returns>
        private bool CreateSchrubberSquad()
        {
            if (_cleanSquad.IsActive) // Don't check if the squad is already active
                return false;

            // Get the agents with the most fulfilled personal utility
            _agents = _agents.OrderBy(x => x.GetPersonalUtility()).ToList();
            List<ISquadMember> agentsToAdd = new List<ISquadMember>();

            // How many agents are needed for the cleanSquad
            int totalneededAgents = GameManager.I.DungeonManager.TotalNeededAgents;
            if (totalneededAgents <= 0) // Dungeon is already clean
                return false;
            else if (totalneededAgents < _cleanSquad.TargetAgentCount) // Less agents than usual are needed
                _cleanSquad.TargetAgentCount = totalneededAgents;
            if (_agents.Count < _cleanSquad.TargetAgentCount) // Do even enough agents exist in total
                return false;

            int i = 0;
            while (agentsToAdd.Count < _cleanSquad.TargetAgentCount && i < _agents.Count)
            {
                Schrubber_Agent a = _agents[i] as Schrubber_Agent;
                if (a.CurrentSquad.SquadType != EGoalType.Clean && // Only if agent isn't already in schrubbersquad
                    a.GetPersonalUtility() < _schrubberSquadThreshold && // And his utility is good enough
                    a.Blackboard.HealthPoints > ((float)Blackboard.MaxHealthPoints / 2f)) // And has enough health
                    agentsToAdd.Add(_agents[i]);
                i++;
            }

            if (agentsToAdd.Count == _cleanSquad.TargetAgentCount &&
                this.CleanSquadEnoughItemsInStorage(agentsToAdd))
            {
                foreach (ISquadMember a in agentsToAdd)
                    _baseSquad.SquadMemberLeft(a);

                _cleanSquad.StartSquad(agentsToAdd);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Are there enough specific items in the storage for the squad to be created
        /// </summary>
        private bool CleanSquadEnoughItemsInStorage(List<ISquadMember> membersToAdd)
        {
            Recipe neededResources = new Recipe();

            // Check brooms
            int neededBrooms = membersToAdd.Count;
            foreach (ISquadMember sm in membersToAdd) // Check each member
                if (sm.Blackboard.Inventory.PeekItems(EItem.Tool_Broom, out List<Item.Item> brooms)) // If has a broom
                    neededBrooms--;

            neededResources.AddItem(new KeyValuePair<EItem, int>(EItem.Tool_Broom, neededBrooms));
            neededResources += DungeonStorage.I.GetCurrentlyNeededItems();

            Storage s = StoragePositions.I.GetNearestStorage(Vector3.zero);
            if (s.Inventory.HasEnoughResources(neededResources))
                return true;
            return false;
        }

        public void ReassignAgentToBaseSquad(ISquadMember a)
        {
            _baseSquad.AddAgent(a);
        }

        /// <summary>
        /// A completely new agent just entered the scene. Initialize and manage this agent
        /// </summary>
        public void AddAgent(Schrubber_Agent a)
        {
            a.ID = _agents.Count;
            a.Init(true);
            _agents.Add(a);
            _baseSquad.AddAgent(a);
        }

        public void RemoveAgent(Schrubber_Agent a)
        {
            a.CurrentSquad?.SquadMemberLeft(a);
            _agents.Remove(a);
            if (a.transform != null &&
                a.transform.parent?.gameObject != null)
                Destroy(a.transform.parent.gameObject);
        }

        public void StopSquad(EGoalType type)
        {
            switch(type)
            {
                case EGoalType.Clean:
                    _lastCleanSquadStopTime = Time.time;
                    break;
            }
        }
    }
}
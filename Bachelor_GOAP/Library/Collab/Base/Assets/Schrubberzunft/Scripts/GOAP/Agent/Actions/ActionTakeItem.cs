﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;
using Schrubber.Item;

namespace Schrubber.GOAP
{
    public class ActionTakeItem : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Inventory _targetInventory = null;
        private EItem _targetItem = EItem.Nothing;

        #region IGOAP_Action Members

        protected override void Awake()
        {
            base.Awake();
            base._name = "TakeItem";
        }

        public override bool CheckProceduralPrecondition(GOAP_State plannedState)
        {
            this.UpdateTargetItem(plannedState);
            this.GetTargetPosition(plannedState);
            return _targetInventory.ItemExists(_targetItem);
        }


        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        public override GOAP_State GetEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetEffects(plannedState);

            this.UpdateTargetItem(plannedState);
            if (_targetItem != EItem.Nothing)
                effects.SetSymbol(SchrubberSymbols.HasItem(Utils.GetItemName(_targetItem)), true);
            return effects;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            if (worldState.State.TryGetValue(SchrubberSymbols.NearestStorage, out object targetobj))
            {
                Storage storage = (Storage)targetobj;
                _targetInventory = storage.Inventory;
                targetPos = storage.Position;
            }
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            StartCoroutine(Wait(onFinished, onFailed, worldState));
        }

        #endregion

        private IEnumerator Wait(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            yield return new WaitForSecondsRealtime(_actionDuration);

            this.UpdateTargetItem(worldState);
            this.GetTargetPosition(worldState);
            Item.Item targetItem;
            if (_targetInventory.TakeItem(_targetItem, out targetItem))
            {
                base._blackboard.Inventory.AddItem(targetItem);
                onFinished();
            }
            else
                onFailed();
        }

        private void UpdateTargetItem(GOAP_State worldState)
        {
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.TargetItem, out targetobj))
                _targetItem = (EItem)targetobj;
        }
    }
}
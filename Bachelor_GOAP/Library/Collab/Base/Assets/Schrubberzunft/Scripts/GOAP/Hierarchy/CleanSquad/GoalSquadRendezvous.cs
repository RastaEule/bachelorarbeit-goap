﻿using GOAP.Unity;
using Schrubber.GOAP;

namespace Schrubber.Hierarchy
{
    public class GoalSquadRendezvous : GOAP_Goal, ICleanSquadGoal
    {
        private CleanSquad_Blackboard _blackboard = null;

        protected override void Awake()
        {
            base.Awake();
            base._name = "SquadRendezvous";

            base._goalState.SetSymbol(SchrubberSymbols.SquadRendezvous, true);
        }

        void ICleanSquadGoal.Init(CleanSquad_Blackboard blackboard)
        {
            _blackboard = blackboard;
        }

        public override void OnPlanFinished()
        {
            // Reset this world state
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.SquadRendezvous, false);

            _blackboard.SetSquadBehavior(typeof(GoalCleanDungeon));

#warning Experiment1
            foreach (Schrubber_Agent a in _blackboard.Agents)
                a.IsActive = false;
        }

        public override void OnPlanFailed()
        {
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.SquadRendezvous, false);
        }

        public override void Blacklist()
        {
            base._agent.CurrentState.SetSymbol(SchrubberSymbols.SquadRendezvous, false);
            base.Blacklist();
        }
    }
}
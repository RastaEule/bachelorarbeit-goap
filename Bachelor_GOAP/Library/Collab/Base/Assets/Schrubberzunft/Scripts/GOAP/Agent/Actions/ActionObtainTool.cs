﻿using System;
using System.Collections;
using UnityEngine;
using GOAP.Core;

namespace Schrubber.GOAP
{
    public class ActionObtainTool : BaseAction_Move
    {
        [SerializeField]
        private float _actionDuration = 0.5f;

        private Action _onFinished = null;
        private Action _onFailed = null;

        private Item.EItem _targetTool = Item.EItem.Nothing;

        #region IGOAP_Action Members

        public override GOAP_State GetEffects(GOAP_State plannedState)
        {
            GOAP_State effects = base.GetEffects(plannedState);

            GOAP_State possibleTools = new GOAP_State(); // First remove information about precious tool
            possibleTools.SetSymbol("hasTool_" + SchrubberSymbols.Tool_Axe, false);
            possibleTools.SetSymbol("hasTool_" + SchrubberSymbols.Tool_FishingRod, false);
            effects -= possibleTools;

            // Then add information about the new tool
            object targetobj;
            if (plannedState.State.TryGetValue("targetTool", out targetobj))
            {
                string targetString = (string)targetobj;

                if (targetString.Equals(SchrubberSymbols.Tool_Axe) ||
                    targetString.Equals(SchrubberSymbols.Tool_FishingRod))
                    effects.SetSymbol("hasTool_" + targetString, true);
            }
            return effects;
        }

        public override float GetCost(GOAP_State plannedState)
        {
            return base.GetCost(plannedState) + _actionDuration;
        }

        #endregion

        #region BaseAction_Move Members

        protected override Vector3 GetTargetPosition(GOAP_State worldState)
        {
            // Get target position from sensor
            Vector3 targetPos = Vector3.zero;
            object targetobj;
            if (worldState.State.TryGetValue(SchrubberSymbols.PositionStorage, out targetobj))
                targetPos = (Vector3)targetobj;
            return targetPos;
        }

        protected override void Execute(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            _onFinished = onFinished;
            _onFailed = onFailed;

            // Check which tool to obtain
            object targetobj;
            if (worldState.State.TryGetValue("targetTool", out targetobj))
            {
                string targetString = (string)targetobj;

                if (targetString.Equals(SchrubberSymbols.Tool_Axe))
                    _targetTool = Item.EItem.Tool_Axe;
                else if (targetString.Equals(SchrubberSymbols.Tool_FishingRod))
                    _targetTool = Item.EItem.Tool_FishingRod;
            }

            StartCoroutine(Wait());
        }

        #endregion

        private IEnumerator Wait()
        {
            yield return new WaitForSecondsRealtime(_actionDuration);
            base._blackboard.Inventory.AddResource(_targetTool, 1);
            _onFinished();
        }
    }
}
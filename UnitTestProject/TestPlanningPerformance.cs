﻿using GOAP.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using UnitTestProject.TestClasses;
using UnitTestProject.TestClasses.Test2ComplexPlan;

namespace UnitTestProject
{
    /// <summary>
    /// This class is not actually used for unit testing.
    /// However, I use this to test the runtime of planning
    /// </summary>
    [TestClass]
    public class TestPlanningPerformance
    {
        private TestLogger _logger;
        private TestAgent _agent;

        [TestInitialize]
        public void Init()
        {
            _logger = new TestLogger();

            _agent = new TestAgent(_logger, this.Test1OnPlanningDone, this.Test1OnPlanningFailed);
            _agent.CurrentState.SetSymbol("stage2_fin", false);
            _agent.CurrentState.SetSymbol("stage1_L_fin", false);
            _agent.CurrentState.SetSymbol("stage1_R_fin", false);
            _agent.CurrentState.SetSymbol("stage1_X_fin", false);
            _agent.CurrentState.SetSymbol("stage0_R_fin", false);

            List<IGOAP_Action> actions = new List<IGOAP_Action>();
            actions.Add(new Test2Action_Stage0R());
            actions.Add(new Test2Action_Stage1L());
            actions.Add(new Test2Action_Stage1R());
            actions.Add(new Test2Action_Stage1X());
            actions.Add(new Test2Action_Stage2L());
            actions.Add(new Test2Action_Stage2R());

            List<IGOAP_Goal> goals = new List<IGOAP_Goal>();
            goals.Add(new Test2Goal_Stage2Fin());

            _agent.SetActions(actions);
            _agent.SetGoals(goals);
        }

        [TestMethod]
        public void Test1ComplexPlanPerformance()
        {
            int testRuns = 50;
            List<PerformanceResults> results = new List<PerformanceResults>();

            results.Add(this.PerformPlanning(1, testRuns));
            results.Add(this.PerformPlanning(10, testRuns));
            results.Add(this.PerformPlanning(50, testRuns));
            results.Add(this.PerformPlanning(100, testRuns));
            results.Add(this.PerformPlanning(1000, testRuns));
            results.Add(this.PerformPlanning(10000, testRuns));

            this.WriteToExcel(results);
            
            Assert.IsTrue(true);
        }

        private PerformanceResults PerformPlanning(int iterations, int testRuns)
        {
            PerformanceResults result = new PerformanceResults();
            List<long> individualresults = new List<long>();
            Stopwatch sw_iteration = new Stopwatch();
            for (int i = 0; i < testRuns; i++)
            {
                sw_iteration.Restart();
                for (int j = 0; j < iterations; j++)
                    _agent.GeneratePlan();

                sw_iteration.Stop();
                individualresults.Add(sw_iteration.ElapsedMilliseconds);
            }
            result.Iterations = iterations;
            result.RunResults = individualresults;

            return result;
        }

        private void Test1OnPlanningDone(GOAP_PlanResults result)
        {
        }

        private void Test1OnPlanningFailed(GOAP_PlanResults result)
        {
            Assert.IsTrue(false);
        }

        public void WriteToExcel(List<PerformanceResults> results)
        {
            results.OrderBy(x => x.Iterations);

            using (StreamWriter writer = new StreamWriter("D:\\BachelorarbeitGOAP\\bachelorarbeitgoap\\Programmierung\\PerformanceResultsNoH.txt"))
            {
                // Line 1
                writer.Write("Iterations;");
                foreach (var pr in results)
                    writer.Write(pr.Iterations + ";");
                writer.Write("\r\n");

                // All individual runs
                for (int i = 0; i < results[0].RunResults.Count; i++) // All runs
                {
                    writer.Write("Run " + i + ", elapsed Time in ms;");
                    foreach (var pr in results)
                        writer.Write(pr.RunResults[i] + ";");
                    writer.Write("\r\n");
                }

                writer.WriteLine();
                // Duration in total
                writer.Write("Duration total in ms;");
                foreach (var pr in results)
                    writer.Write(pr.TotalElapsedTime + ";");
                writer.Write("\r\n");

                // Average run duration
                writer.Write("Average run duration in ms;");
                foreach (var pr in results)
                    writer.Write(pr.AverageRunElapsedTime + ";");
                writer.Write("\r\n");

                // Average Astar duration
                writer.Write("Average astar duration in ms;");
                foreach (var pr in results)
                    writer.Write(pr.AverageSingleAstarElapsedTime + ";");
                writer.Write("\r\n");
            }
        }
    }

    public class PerformanceResults
    {
        public PerformanceResults()
        {

        }

        /// <summary>
        /// How many iterations for each testrun
        /// </summary>
        public int Iterations { get; set; }

        /// <summary>
        /// Elapsed ms of each testrun
        /// </summary>
        public List<long> RunResults { get; set; }

        public long TotalElapsedTime
        {
            get
            {
                long estTime = 0;
                foreach (long l in RunResults)
                    estTime += l;
                return estTime;
            }
        }

        public double AverageRunElapsedTime => TotalElapsedTime / RunResults.Count;

        public double AverageSingleAstarElapsedTime => AverageRunElapsedTime / Iterations;
    }
}
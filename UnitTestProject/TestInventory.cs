﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using GOAP.Core;
using Schrubber;
using Schrubber.Item;

namespace UnitTestProject
{
    [TestClass]
    public class TestInventory
    {
        [TestMethod]
        public void TestInventory_HasEnoughResources()
        {
            Inventory i1 = new Inventory();
            i1.AddItem(new Resource_Wood());
            i1.AddItem(new Resource_Wood());
            Assert.IsFalse(i1.HasEnoughResources(RecipeDatabase.GetRecipe(EItem.Tool_Axe)));

            i1.AddItem(new Resource_Stone());
            Assert.IsTrue(i1.HasEnoughResources(RecipeDatabase.GetRecipe(EItem.Tool_Axe)));

            i1.TakeItem(EItem.Resource_Wood);
            Assert.IsFalse(i1.HasEnoughResources(RecipeDatabase.GetRecipe(EItem.Tool_Axe)));
        }

        [TestMethod]
        public void TestInventory_HasEnoughResources2()
        {
            List<KeyValuePair<EItem, int>> neededItems;

            Inventory i1 = new Inventory();
            i1.AddItem(new Resource_Wood());
            i1.AddItem(new Resource_Wood());
            Assert.IsFalse(i1.HasEnoughResources(RecipeDatabase.GetRecipe(EItem.Tool_Axe), out neededItems));
            Assert.IsTrue(neededItems.Count == 1);
            Assert.IsTrue(neededItems[0].Key == EItem.Resource_Stone);
            Assert.IsTrue(neededItems[0].Value == 1);

            i1.TakeItem(EItem.Resource_Wood);
            i1.TakeItem(EItem.Resource_Wood);
            Assert.IsFalse(i1.HasEnoughResources(RecipeDatabase.GetRecipe(EItem.Tool_Axe), out neededItems));
            Assert.IsTrue(neededItems.Count == 2);
            Assert.IsTrue(neededItems[0].Key == EItem.Resource_Wood);
            Assert.IsTrue(neededItems[0].Value == 2);
            Assert.IsTrue(neededItems[1].Key == EItem.Resource_Stone);
            Assert.IsTrue(neededItems[1].Value == 1);
        }

        [TestMethod]
        public void TestInventory_HasEnoughResourcesCombined()
        {
            Inventory i1 = new Inventory();
            i1.AddItem(new Resource_Wood());
            i1.AddItem(new Resource_Wood());

            Inventory i2 = new Inventory();
            i2.AddItem(new Resource_Stone());
            List<KeyValuePair<EItem, int>> itemsToTake;
            Assert.IsTrue(i1.HasEnoughResourcesCombined(i2, RecipeDatabase.GetRecipe(EItem.Tool_Axe), out itemsToTake));
            Assert.IsTrue(itemsToTake.Count == 1);
            Assert.IsTrue(itemsToTake[0].Key == EItem.Resource_Wood);
            Assert.IsTrue(itemsToTake[0].Value == 2);
            i1.TakeItems(itemsToTake);

            Assert.IsTrue(i1.InternalInventory[EItem.Resource_Wood].Count == 0);
        }
    }
}
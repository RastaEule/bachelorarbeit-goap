using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using GOAP.Core;

namespace UnitTestProject
{
    [TestClass]
    public class TestGOAP_State
    {
        [TestMethod]
        public void TestState_Equals()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", false);
            s1.SetSymbol("Symbol2", true);
            s1.SetSymbol("Symbol3", false);
            s1.SetSymbol("Symbol4", true);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", true);
            s2.SetSymbol("Symbol3", false);
            s2.SetSymbol("Symbol4", true);
            Assert.IsTrue(s1.Equals(s2));

            GOAP_State s3 = new GOAP_State();
            s3.SetSymbol("Symbol1", false);
            s3.SetSymbol("Symbol2", true);
            s3.SetSymbol("Symbol3", false);
            s3.SetSymbol("Symbol4", true);
            s3.SetSymbol("Symbol5", true);
            Assert.IsFalse(s1.Equals(s3));

            GOAP_State s4 = new GOAP_State();
            s4.SetSymbol("Symbol1", false);
            s4.SetSymbol("Symbol2", true);
            s4.SetSymbol("Symbol3", false);
            Assert.IsFalse(s1.Equals(s4));

            GOAP_State s5 = new GOAP_State();
            Assert.IsFalse(s1.Equals(s5));

            GOAP_State s6 = new GOAP_State();
            Assert.IsTrue(s5.Equals(s6));
        }

        [TestMethod]
        public void TestState_IsGoal()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", false);
            s1.SetSymbol("Symbol2", true);
            s1.SetSymbol("Symbol3", false);
            s1.SetSymbol("Symbol4", true);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", true);
            s2.SetSymbol("Symbol3", false);
            Assert.IsTrue(s1.IsGoal(s2));
            Assert.IsFalse(s2.IsGoal(s1));

            GOAP_State s3 = new GOAP_State();
            s3.SetSymbol("Symbol1", false);
            s3.SetSymbol("Symbol4", true);
            Assert.IsTrue(s1.IsGoal(s3));
            Assert.IsFalse(s2.Equals(s3));


            GOAP_State s4 = new GOAP_State();
            s4.SetSymbol("s1", true);

            GOAP_State s5 = new GOAP_State();
            s5.SetSymbol("s1", true);
            s5.SetSymbol("s2", true);
            Assert.IsFalse(s4.IsGoal(s5));
        }

        [TestMethod]
        public void TestState_Copy()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", false);
            s1.SetSymbol("Symbol2", true);
            s1.SetSymbol("Symbol3", false);
            s1.SetSymbol("Symbol4", true);

            GOAP_State s1_control = new GOAP_State(); // same as s1
            s1_control.SetSymbol("Symbol1", false);
            s1_control.SetSymbol("Symbol2", true);
            s1_control.SetSymbol("Symbol3", false);
            s1_control.SetSymbol("Symbol4", true);

            GOAP_State s2_control = new GOAP_State(); // same as s1, but with change from s2
            s2_control.SetSymbol("Symbol1", true);
            s2_control.SetSymbol("Symbol2", true);
            s2_control.SetSymbol("Symbol3", false);
            s2_control.SetSymbol("Symbol4", true);

            GOAP_State s2 = s1.Copy();
            s2.SetSymbol("Symbol1", true);

            // Change something after copy -> check if this value is also different in s1
            // If so, it is not a deep copy.
            Assert.IsTrue(s1.Equals(s1_control));
            Assert.IsTrue(s2.Equals(s2_control));
        }

        [TestMethod]
        public void TestState_MatchingSymbols()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", true);
            s1.SetSymbol("Symbol2", true);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", false);

            GOAP_State s3 = new GOAP_State();
            s3.SetSymbol("Symbol1", true);
            s3.SetSymbol("Symbol2", false);

            GOAP_State s4 = new GOAP_State();
            s4.SetSymbol("Symbol1", false);
            s4.SetSymbol("Symbol2", true);

            Assert.IsFalse(s1.HasMatchingSymbols(s2));
            Assert.IsTrue(s1.HasMatchingSymbols(s3));
            Assert.IsTrue(s1.HasMatchingSymbols(s4));
        }

        [TestMethod]
        public void TestState_MismatchingSymbols()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", true);
            s1.SetSymbol("Symbol2", true);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", true);

            GOAP_State s3 = new GOAP_State();
            s3.SetSymbol("Symbol1", false);

            GOAP_State mismatches;
            Assert.IsTrue(s1.HasMismatchingSymbols(s2, out mismatches));
            Assert.IsTrue(mismatches.Equals(s3));


            GOAP_State s4 = new GOAP_State();
            s4.SetSymbol("Symbol1", false);
            s4.SetSymbol("Symbol2", false);
            s4.SetSymbol("Symbol3", false);
            Assert.IsTrue(s1.HasMismatchingSymbols(s4, out mismatches));
            Assert.IsTrue(mismatches.Equals(s4));
        }
        
        [TestMethod]
        public void TestState_Addition()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", true);
            s1.SetSymbol("Symbol2", true);
            s1.SetSymbol("Symbol3", false);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", true);
            s2.SetSymbol("Symbol4", true);

            GOAP_State s_result1 = new GOAP_State();
            s_result1.SetSymbol("Symbol1", false);
            s_result1.SetSymbol("Symbol2", true);
            s_result1.SetSymbol("Symbol3", false);
            s_result1.SetSymbol("Symbol4", true);

            Assert.IsTrue((s1 + s2).Equals(s_result1));
        }

        [TestMethod]
        public void TestState_Subtraction()
        {
            GOAP_State s1 = new GOAP_State();
            s1.SetSymbol("Symbol1", true);
            s1.SetSymbol("Symbol2", true);
            s1.SetSymbol("Symbol3", false);

            GOAP_State s2 = new GOAP_State();
            s2.SetSymbol("Symbol1", false);
            s2.SetSymbol("Symbol2", true);
            s2.SetSymbol("Symbol4", true);

            GOAP_State s_result1 = new GOAP_State();
            s_result1.SetSymbol("Symbol3", false);

            GOAP_State s_result2 = new GOAP_State();
            s_result2.SetSymbol("Symbol4", true);

            Assert.IsTrue((s1 - s2).Equals(s_result1));
            Assert.IsTrue((s2 - s1).Equals(s_result2));
        }
    }
}
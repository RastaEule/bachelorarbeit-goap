﻿using GOAP.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitTestProject.TestClasses;
using UnitTestProject.TestClasses.Test2ComplexPlan;
using UnitTestProject.TestClasses.Test3PlanFailProcPrec;

namespace UnitTestProject
{
    [TestClass]
    public class TestPlanning
    {
        private TestLogger _logger;
        private TestAgent _agent;


        [TestInitialize]
        public void Init()
        {
            _logger = new TestLogger();
        }

        [TestMethod]
        public void Test1SimplePlan()
        {
            _agent = new TestAgent(_logger, this.Test1OnPlanningDone, this.Test1OnPlanningFailed);
            _agent.CurrentState.SetSymbol("level1Done", false);

            List<IGOAP_Action> actions = new List<IGOAP_Action>();
            actions.Add(new Test1ActionLevel1());

            List<IGOAP_Goal> goals = new List<IGOAP_Goal>();
            goals.Add(new Test1Goal_Level1());

            _agent.SetActions(actions);
            _agent.SetGoals(goals);
            _agent.GeneratePlan();
        }

        private void Test1OnPlanningDone(GOAP_PlanResults result)
        {
            Assert.IsTrue(result.Plan.Plan.Count == 1);
            Assert.IsTrue(result.Plan.Goal is Test1Goal_Level1);

            IGOAP_Action a1 = result.Plan.Plan.Dequeue();
            Assert.IsTrue(a1 is Test1ActionLevel1);

            Assert.IsTrue(_agent.CurrentState.State.Count == 2);
            Assert.IsTrue(_agent.CurrentState.State.TryGetValue("testPrecalculations1", out object obj));
            Assert.IsTrue(obj is bool);
            Assert.IsTrue((bool)obj);
        }

        private void Test1OnPlanningFailed(GOAP_PlanResults result)
        {
            Assert.IsTrue(false);
        }

        [TestMethod]
        public void Test2ComplexPlan()
        {
            _agent = new TestAgent(_logger, this.Test2OnPlanningDone, this.Test2OnPlanningFailed);
            _agent.CurrentState.SetSymbol("stage2_fin", false);
            _agent.CurrentState.SetSymbol("stage1_L_fin", false);
            _agent.CurrentState.SetSymbol("stage1_R_fin", false);
            _agent.CurrentState.SetSymbol("stage1_X_fin", false);
            _agent.CurrentState.SetSymbol("stage0_R_fin", false);

            List<IGOAP_Action> actions = new List<IGOAP_Action>();
            actions.Add(new Test2Action_Stage0R());
            actions.Add(new Test2Action_Stage1L());
            actions.Add(new Test2Action_Stage1R());
            actions.Add(new Test2Action_Stage1X());
            actions.Add(new Test2Action_Stage2L());
            actions.Add(new Test2Action_Stage2R());

            List<IGOAP_Goal> goals = new List<IGOAP_Goal>();
            goals.Add(new Test2Goal_Stage2Fin());

            _agent.SetActions(actions);
            _agent.SetGoals(goals);
            _agent.GeneratePlan();
        }

        private void Test2OnPlanningDone(GOAP_PlanResults result)
        {
            Assert.IsTrue(result.Plan.Plan.Count == 4);
            Assert.IsTrue(result.Plan.Goal is Test2Goal_Stage2Fin);

            IGOAP_Action a = result.Plan.Plan.Dequeue();
            Assert.IsTrue(a is Test2Action_Stage0R);
            a = result.Plan.Plan.Dequeue();
            Assert.IsTrue(a is Test2Action_Stage1R);
            a = result.Plan.Plan.Dequeue();
            Assert.IsTrue(a is Test2Action_Stage1X);
            a = result.Plan.Plan.Dequeue();
            Assert.IsTrue(a is Test2Action_Stage2R);
        }

        private void Test2OnPlanningFailed(GOAP_PlanResults result)
        {
            Assert.IsTrue(false);
        }

        [TestMethod]
        public void Test3PlanFailProcPrec()
        {
            _agent = new TestAgent(_logger, this.Test3OnPlanningDone, this.Test3OnPlanningFailed);
            _agent.CurrentState.SetSymbol("dosomething", false);
            _agent.CurrentState.SetSymbol("done1", false);
            _agent.CurrentState.SetSymbol("done2", false);

            List<IGOAP_Action> actions = new List<IGOAP_Action>();
            actions.Add(new Test3Action_Do1());
            actions.Add(new Test3Action_Do2());

            List<IGOAP_Goal> goals = new List<IGOAP_Goal>();
            goals.Add(new Test3GoalNotPossible());
            goals.Add(new Test3GoalPossible());

            _agent.SetActions(actions);
            _agent.SetGoals(goals);
            _agent.GeneratePlan();
        }

        private void Test3OnPlanningDone(GOAP_PlanResults result)
        {
            Assert.IsTrue(false);
        }

        private void Test3OnPlanningFailed(GOAP_PlanResults result)
        {
            Assert.IsNull(result.Plan);
            Assert.IsTrue(result.FailedGoals.Count == 1);
            Assert.IsTrue(result.FailedGoals[0] is Test3GoalPossible);
        }
    }
}
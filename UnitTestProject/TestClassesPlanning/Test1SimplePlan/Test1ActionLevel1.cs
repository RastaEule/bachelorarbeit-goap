﻿using GOAP.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject.TestClasses
{
    public class Test1ActionLevel1 : IGOAP_Action
    {
        public Test1ActionLevel1()
        {

        }

        #region IGOAP_Action Members

        public bool IsPossible(GOAP_State plannedState)
        {
            return true;
        }

        public float GetCost(GOAP_State plannedState)
        {
            return 1f;
        }

        public GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public void GetPrecalculations(GOAP_State plannedState)
        {
            plannedState.SetSymbol("testPrecalculations1", true);
        }

        public GOAP_State GetStaticEffects()
        {
            GOAP_State effects = new GOAP_State();
            effects.SetSymbol("level1Done", true);
            return effects;
        }

        public GOAP_State GetStaticPreconditions()
        {
            return new GOAP_State();
        }

        public void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }

        #endregion
    }
}

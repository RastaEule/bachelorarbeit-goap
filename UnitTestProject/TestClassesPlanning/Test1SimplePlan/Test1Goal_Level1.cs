﻿using GOAP.Core;

namespace UnitTestProject.TestClasses
{
    public class Test1Goal_Level1 : IGOAP_Goal
    {
        public Test1Goal_Level1()
        {

        }

        private bool _blacklisted = false;

        #region IGOAP_Goal Members

        public void Blacklist(bool byPlanner)
        {
            _blacklisted = true;
        }

        public GOAP_State GetGoalState()
        {
            GOAP_State goalstate = new GOAP_State();
            goalstate.SetSymbol("level1Done", true);
            return goalstate;
        }

        public void OnPlanFailed()
        {
            // Do nothing
        }

        public void OnPlanFinished()
        {
            // Do nothing
        }

        public float Priority { get; set; } = 0.5f;

        public bool IsPossible { get; set; } = true;

        public bool IsBlackListed => _blacklisted;

        #endregion
    }
}
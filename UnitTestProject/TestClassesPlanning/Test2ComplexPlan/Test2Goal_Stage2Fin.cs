﻿using GOAP.Core;

namespace UnitTestProject.TestClasses.Test2ComplexPlan
{
    public class Test2Goal_Stage2Fin : IGOAP_Goal
    {
        public Test2Goal_Stage2Fin()
        {

        }

        private bool _blacklisted = false;

        #region IGOAP_Goal Members

        public void Blacklist(bool byPlanner)
        {
            _blacklisted = true;
        }

        public GOAP_State GetGoalState()
        {
            GOAP_State goalstate = new GOAP_State();
            goalstate.SetSymbol("stage2_fin", true);
            return goalstate;
        }

        public void OnPlanFailed()
        {
            // Do nothing
        }

        public void OnPlanFinished()
        {
            // Do nothing
        }

        public float Priority { get; set; } = 0.5f;

        public bool IsPossible { get; set; } = true;

        public bool IsBlackListed => _blacklisted;

        #endregion
    }
}

﻿using GOAP.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject.TestClasses.Test2ComplexPlan
{
    public class Test2Action_Stage1R : IGOAP_Action
    {
        public Test2Action_Stage1R()
        {

        }

        #region IGOAP_Action Members

        public float GetCost(GOAP_State plannedState)
        {
            return 2f;
        }

        public GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public void GetPrecalculations(GOAP_State plannedState)
        {
        }

        public GOAP_State GetStaticPreconditions()
        {
            GOAP_State preconditions = new GOAP_State();
            preconditions.SetSymbol("stage0_R_fin", true);
            return preconditions;
        }

        public GOAP_State GetStaticEffects()
        {
            GOAP_State effects = new GOAP_State();
            effects.SetSymbol("stage1_R_fin", true);
            return effects;
        }

        public bool IsPossible(GOAP_State plannedState)
        {
            return true;
        }

        public void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }

        #endregion
    }
}

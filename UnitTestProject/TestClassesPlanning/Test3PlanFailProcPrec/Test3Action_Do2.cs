﻿using GOAP.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject.TestClasses.Test3PlanFailProcPrec
{
    public class Test3Action_Do2 : IGOAP_Action
    {
        public Test3Action_Do2()
        {

        }

        #region IGOAP_Action Members

        public bool IsPossible(GOAP_State plannedState)
        {
            return false;
        }

        public float GetCost(GOAP_State plannedState)
        {
            return 2f;
        }

        public GOAP_State GetDynamicEffects(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public GOAP_State GetDynamicPreconditions(GOAP_State plannedState)
        {
            return new GOAP_State();
        }

        public void GetPrecalculations(GOAP_State plannedState)
        {
        }

        public GOAP_State GetStaticPreconditions()
        {
            return new GOAP_State();
        }

        public GOAP_State GetStaticEffects()
        {
            GOAP_State effects = new GOAP_State();
            effects.SetSymbol("done2", true);
            return effects;
        }

        public void Run(Action onFinished, Action onFailed, GOAP_State worldState)
        {
            onFinished();
        }

        #endregion
    }
}

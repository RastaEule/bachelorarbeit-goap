﻿using GOAP.Core;

namespace UnitTestProject.TestClasses.Test3PlanFailProcPrec
{
    public class Test3GoalNotPossible : IGOAP_Goal
    {
        public Test3GoalNotPossible()
        {

        }

        private bool _blacklisted = false;

        #region IGOAP_Goal Members

        public void Blacklist(bool byPlanner)
        {
            _blacklisted = true;
        }

        public GOAP_State GetGoalState()
        {
            GOAP_State goalstate = new GOAP_State();
            goalstate.SetSymbol("dosomething", true);
            return goalstate;
        }

        public void OnPlanFailed()
        {
            // Do nothing
        }

        public void OnPlanFinished()
        {
            // Do nothing
        }

        public float Priority { get; set; } = 1;

        public bool IsPossible { get; set; } = false;

        public bool IsBlackListed => _blacklisted;

        #endregion
    }
}

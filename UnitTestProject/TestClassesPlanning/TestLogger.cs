﻿using GOAP.Core;
using GOAP.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject.TestClasses
{
    public class TestLogger : IGOAP_Logger
    {
        public TestLogger()
        {
        }

        public void Log(string message, IGOAP_Agent agent, EMessageType type, EMessageSeverity severity)
        {
            string pre = "---- ";
            switch (severity)
            {
                case EMessageSeverity.Error:
                    pre = "GOAP_ERROR: ";
                    break;
                case EMessageSeverity.Warning:
                    pre = "GOAP_WARNING: ";
                    break;
                case EMessageSeverity.Info:
                    pre = "GOAP_INFO: ";
                    break;
            }
            System.Diagnostics.Debug.WriteLine(pre + message);
        }

        public void Log(string message, EMessageType type, EMessageSeverity severity)
        {
            this.Log(message, null, type, severity);
        }

        public void Log(string message)
        {
            this.Log(message, null, EMessageType.Info, EMessageSeverity.Info);
        }

        public EMessageType MessageFlags { get; set; } = EMessageType.Info | EMessageType.PlanExecution | EMessageType.Planning;
    }
}

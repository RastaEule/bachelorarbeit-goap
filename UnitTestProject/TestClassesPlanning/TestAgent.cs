﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GOAP.Core;
using GOAP.Logging;

namespace UnitTestProject.TestClasses
{
    public class TestAgent : IGOAP_Agent
    {
        private IGOAP_Logger _logger;
        private Action<GOAP_PlanResults> _onPlanningDone;
        private Action<GOAP_PlanResults> _onPlanningFailed;
        private GOAP_State _currentState;
        private List<IGOAP_Action> _actions;
        private List<IGOAP_Goal> _goals;

        public TestAgent(IGOAP_Logger logger, Action<GOAP_PlanResults> onPlanningDone, Action<GOAP_PlanResults> onPlanningFailed)
        {
            _logger = logger;
            _onPlanningDone = onPlanningDone;
            _onPlanningFailed = onPlanningFailed;
            CurrentState = new GOAP_State();
        }

        #region IGOAP_Agent Members

        public GOAP_State CurrentState
        {
            get { return _currentState; }
            set { _currentState = value; }
        }

        public GOAP_PlanRunner Planrunner => throw new NotImplementedException();

        public void GeneratePlan()
        {
            GOAP_Planner planner = new GOAP_Planner(_logger);
            planner.Plan(this, _onPlanningDone, _onPlanningFailed);
        }

        public void SortGoals()
        {
            _goals = _goals.OrderByDescending(x => x.Priority).ToList();
        }

        public List<IGOAP_Action> GetActions()
        {
            return _actions;
        }

        public List<IGOAP_Goal> GetGoals()
        {
            return _goals;
        }

        public void StartPlanExecution(GOAP_Plan plan)
        {
            throw new NotImplementedException();
        }

        public bool ReplanAfterGoalsBlacklisted { get; set; } = false;

        #endregion

        public void SetActions(List<IGOAP_Action> actions)
        {
            _actions = actions;
        }

        public void SetGoals(List<IGOAP_Goal> goals)
        {
            _goals = goals;
        }
    }
}